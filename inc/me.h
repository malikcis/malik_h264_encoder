/*****************************************************************************  
 *  
 *  H264 AVC CODEC  
 *  
 *  Copyright(C) 2020 llcc <malik.cisse@abateck.com>  
 *  
 *  This program is free software ; you can redistribute it and/or modify  
 *  it under the terms of the GNU General Public License as published by  
 *  the Free Software Foundation ; either version 2 of the License, or  
 *  (at your option) any later version.  
 *  
 *  This program is distributed in the hope that it will be useful,  
 *  but WITHOUT ANY WARRANTY ; without even the implied warranty of  
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the  
 *  GNU General Public License for more details.  
 *  
 *  You should have received a copy of the GNU General Public License  
 *  along with this program ; if not, write to the Free Software  
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA  
 *  
 ****************************************************************************/ 
#ifndef ME_H_
#define ME_H_

#include "global.h"

#define CLIP_PIXEL(x)   ((UInt8)(((x)<0)?(0):(((x)>255)?(255):(x))))   /* x clipped to 0 - 255 */
//#define ABS(x)      (((x) >= 0 ) ? (x) : -(x))    /** Absolute value of x */

void mb_motion_estimation(pBlockInfo_t pBlockInfo, pEncFrame_t pEncFrame);
void get_best_match(pBlockInfo_t pBlockInfo, pEncFrame_t pEncFrame);
void get_ref_luma_block(pBlockInfo_t pBlockInfo, pEncFrame_t pEncFrame, Int16 mv_x, Int16 mv_y, UInt8 **pp_ref_lum);

void bilinear_interpolation(
	pBlockInfo_t pBlockInfo,
    UInt8 **in, 
    UInt8 **out, 
    UInt32  rows, 
    UInt32  cols, 
    UInt8 int_ver, 
    UInt8 int_hor);

void bicubic_interpolation(
	pBlockInfo_t pBlockInfo,
    UInt8 **in, 
    UInt8 **out, 
    UInt32  rows, 
    UInt32  cols, 
    UInt8 int_ver, 
    UInt8 int_hor);

void get_mb_sad(UInt8 pp_currentData[16][16], UInt8 **pp_ref_lum, UInt32 *mb_sad_curr);
void mv_prediction(pBlockInfo_t pBlockInfo);
void frame_motion_estimation(pEncFrame_t pEncFrame);
void adjust_motion_vector(pBlockInfo_t pBlockInfo, vector_t *g_point);

#endif //ME_H_

