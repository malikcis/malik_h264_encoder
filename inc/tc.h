/*****************************************************************************  
 *  
 *  H264 AVC CODEC  
 *  
 *  Copyright(C) 2020 llcc <malik.cisse@abateck.com>  
 *  
 *  This program is free software ; you can redistribute it and/or modify  
 *  it under the terms of the GNU General Public License as published by  
 *  the Free Software Foundation ; either version 2 of the License, or  
 *  (at your option) any later version.  
 *  
 *  This program is distributed in the hope that it will be useful,  
 *  but WITHOUT ANY WARRANTY ; without even the implied warranty of  
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the  
 *  GNU General Public License for more details.  
 *  
 *  You should have received a copy of the GNU General Public License  
 *  along with this program ; if not, write to the Free Software  
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA  
 *  
 ****************************************************************************/ 
#ifndef TC_H_
#define TC_H_

#include "global.h"

void intraPred(pBlockInfo_t pBlockInfo);
void iIntraPred(pBlockInfo_t pBlockInfo);
void get_4x4_blocks(pBlockInfo_t pBlockInfo);
void write_4x4_blocks(pBlockInfo_t pBlockInfo);
void getIntraPredictorY(pBlockInfo_t pBlockInfo);
void getIntraPredictorU(pBlockInfo_t pBlockInfo);
void getIntraPredictorV(pBlockInfo_t pBlockInfo);

void dct4x4_c(Int16* data);
void dct4x4dc_c(Int16* data);
void dct2x2dc_c(Int16* data);
void idct4x4_c(Int16* data);
void idct4x4dc_c(Int16* data);
void idct2x2dc_c(Int16* data);

void quant4x4_c(Int16* data, const Int32 Qp, Int32 is_intra);
void quant4x4dc_c(Int16* data, const Int32 Qp);
void quant2x2dc_c(Int16* data, const Int32 Qp, Int32 is_intra);
void iquant4x4_c(Int16* data, const Int32 Qp);
void iquant4x4dc_c(Int16* data, const Int32 Qp);
void iquant2x2dc_c(Int16* data, const Int32 Qp);

void mb_texture_codec(pBlockInfo_t pBlockInfo);
void blocksTxtEncoding(pBlockInfo_t pBlockInfo);
void blocksTxtDecoding(pBlockInfo_t pBlockInfo);
void blocksIntrapred(pBlockInfo_t pBlockInfo);
void iBlocksIntrapred(pBlockInfo_t pBlockInfo);
void get_residual(pBlockInfo_t pBlockInfo, int do_dbg_print);
void get_chroma_skip(pBlockInfo_t pBlockInfo);
void motion_compensation(pBlockInfo_t pBlockInfo);
void intra_clipping(pBlockInfo_t pBlockInfo);

//debug
void get_4x4_blocks_dbg(pBlockInfo_t pBlockInfo);
void print_yuv_in_hw_order(pBlockInfo_t pBlockInfo);
void print_reconstructed(pBlockInfo_t pBlockInfo);

#endif //TC_H_

