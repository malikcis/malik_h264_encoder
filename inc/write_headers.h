/*****************************************************************************  
 *  
 *  H264 AVC CODEC  
 *  
 *  Copyright(C) 2020 llcc <malik.cisse@abateck.com>  
 *  
 *  This program is free software ; you can redistribute it and/or modify  
 *  it under the terms of the GNU General Public License as published by  
 *  the Free Software Foundation ; either version 2 of the License, or  
 *  (at your option) any later version.  
 *  
 *  This program is distributed in the hope that it will be useful,  
 *  but WITHOUT ANY WARRANTY ; without even the implied warranty of  
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the  
 *  GNU General Public License for more details.  
 *  
 *  You should have received a copy of the GNU General Public License  
 *  along with this program ; if not, write to the Free Software  
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA  
 *  
 ****************************************************************************/ 
#ifndef WRITE_HEADERS_H_
#define WRITE_HEADERS_H_
#include "global.h"

//define the start codes
#define SEQ_SC     0x0000010F   //sequence start code
#define NAL_SC     0x00000001   //nal start code
#define ENTRY_SC   0x0000010E   //entry point start code
#define FRM_SC     0x0000010D   //frame start code
#define EOF_SEQ_SC 0x0000010A   //end of sequence start code
#define PIC_INIT_QP_MINUS26 0 // hard coded to zero, QP lives in the slice header. This saves some bits in PSP. See JM

enum
{
    NAL_SLICE_NOPART = 1,
    NAL_SLICE_PARTA,
    NAL_SLICE_PARTB,
    NAL_SLICE_PARTC,
    NAL_SLICE_IDR,
    NAL_SEI,
    NAL_SEQ_SET,
    NAL_PIC_SET,
    NAL_ACD,
    NAL_END_SEQ,
    NAL_END_STREAM,
    NAL_FILTER,
    NAL_CUSTOM_SET = 26
};

// Slice type
enum
{
    SLICE_P = 0,
    SLICE_B,
    SLICE_I,
    SLICE_SP,
    SLICE_SI,
};

enum profile_e
{
    PROFILE_BASELINE = 66,
    PROFILE_MAIN     = 77,
    PROFILE_EXTENTED = 88,
    PROFILE_HIGH    = 100,
    PROFILE_HIGH10  = 110,
    PROFILE_HIGH422 = 122,
    PROFILE_HIGH444 = 144
};

//functions 
void writeSequenceHeader(pEncFrame_t pEncFrame);
void writePictureHeader(pEncFrame_t pEncFrame);
void writeSliceHeader(pEncFrame_t pEncFrame);
void nal_unit_write(int nal_ref_idc, int nal_unit_type);

#endif /* WRITE_HEADERS_H_ */

