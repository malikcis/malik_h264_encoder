/*****************************************************************************  
 *  
 *  H264 AVC CODEC  
 *  
 *  Copyright(C) 2020 llcc <malik.cisse@abateck.com>  
 *  
 *  This program is free software ; you can redistribute it and/or modify  
 *  it under the terms of the GNU General Public License as published by  
 *  the Free Software Foundation ; either version 2 of the License, or  
 *  (at your option) any later version.  
 *  
 *  This program is distributed in the hope that it will be useful,  
 *  but WITHOUT ANY WARRANTY ; without even the implied warranty of  
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the  
 *  GNU General Public License for more details.  
 *  
 *  You should have received a copy of the GNU General Public License  
 *  along with this program ; if not, write to the Free Software  
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA  
 *  
 ****************************************************************************/ 
#ifndef RATE_CONTROL_H_
#define RATE_CONTROL_H_

#include "global.h"
#include "rate_control.h"

typedef enum _esc3CSizeVlcTab_t
{
	conservative = 0x00,
	efficient    = 0x01
} esc3CSizeVlcTab_t, *pEsc3CSizeVlcTab_t;

void rdOpt(pEncFrame_t pEncFrame);
void rdOpt_get_pqindex(pEncFrame_t pEncFrame);
void rdOpt_init(pEncFrame_t pEncFrame);
void rdOpt_get_tables(pEncFrame_t pEncFrame);

#endif //RATE_CONTROL_H_
