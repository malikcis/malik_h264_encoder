/*****************************************************************************  
 *  
 *  H264 AVC CODEC  
 *  
 *  Copyright(C) 2020 llcc <malik.cisse@abateck.com>  
 *  
 *  This program is free software ; you can redistribute it and/or modify  
 *  it under the terms of the GNU General Public License as published by  
 *  the Free Software Foundation ; either version 2 of the License, or  
 *  (at your option) any later version.  
 *  
 *  This program is distributed in the hope that it will be useful,  
 *  but WITHOUT ANY WARRANTY ; without even the implied warranty of  
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the  
 *  GNU General Public License for more details.  
 *  
 *  You should have received a copy of the GNU General Public License  
 *  along with this program ; if not, write to the Free Software  
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA  
 *  
 ****************************************************************************/ 
#ifndef GLOBAL_H_
#define GLOBAL_H_

#include <stdlib.h>
#include <stdio.h>
#include <string.h>

//debug stuff
#define MB_ENC               0
#define AC_VLC_TABLE_ERROR   1
#define BLOCK_DEBUG          0
#define BSG_DEBUG            0
#define DQUANT               0 //enable DQUANT synthax elements but not real mquant
#define ENABLE_P_FRAME_INTRA 1
#define DISABLE_NOT_CODED    0
#define FORCE_NOT_CODED      0
#define RANDOM_MVS           0 //random MVs
#define FULL_SEARCH          0 //1: full search, 0: 
#define READ_FROM_STD_IN     0 //force read from std input
#define VHDL_DEBUG		     1

//#define FPRINTF_MC fprintf
#define FPRINTF_MC //

#if 0//BSG_DEBUG
FILE *fileDebug;
#endif

#if GLOBAL_DEBUG1
FILE *idct_input;
FILE *idct_out;
FILE *quant_coeffs;
FILE *scanned_coeffs;
FILE *dc;
FILE *raw_cbpcy;
FILE *mv_pred;
FILE *hp_mv_xy;
FILE *qp_mv_xy;
FILE *cmv_x_o;
FILE *cmv_y_o;
FILE *quant_coeffs_dc_predicted;
FILE *curr_yc;
FILE *res;
FILE *writeback;
FILE *lt100config;
FILE *fp_mv_xy;
FILE *pq_index;  
FILE *pic_bytes;
//h264
FILE *lvl_vlc;
#endif

#ifndef MAX
#define  MAX(a,b)       (((a) > (b)) ? (a) : (b))
#endif

#ifndef MIN
#define  MIN(a,b)       (((a) < (b)) ? (a) : (b))
#endif

#define MEDIAN(x,y,z)   ((x)+(y)+(z)-MAX(MAX(x,y),z)-MIN(MIN(x,y),z))

#define MAX_BITS_COEFF_MB 20000//0x3fff  

#define INTRA_SAD_THR 65535 //no intra

//#define SKIP_SAD_THR  300 //normal mode
#define SKIP_SAD_THR  0 //no skip MB decided in ME

#define SKIP_FR_THR  0  //frame skip threshold (normal == 3). 0 = no skip
#define SC_FR_THR    250 //scene cut threshold
#define NOISE_FR_THR 10000
#define MV_TRESHOLD  6//for hp mvs

//types
typedef long long       Int64;
typedef signed   char   Int8;   //  8 bit   signed integer
typedef signed   short  Int16;  // 16 bit   signed integer
typedef signed   long   Int32;  // 32 bit   signed integer
typedef unsigned char   UInt8;  //  8 bit unsigned integer
typedef unsigned short  UInt16; // 16 bit unsigned integer
typedef unsigned long   UInt32; // 32 bit unsigned integer
typedef float           Float;  // 32 bit floating point
typedef unsigned int    Bool;   // Boolean (True/False)
typedef char            Char;   // character, character array ptr
typedef int             Int;    // machine-natural integer
typedef unsigned int    UInt;   // machine-natural unsigned integer
typedef char           *String; // Null terminated 8 bit char str

typedef Int8   *pInt8;            //  8 bit   signed integer
typedef Int16  *pInt16;           // 16 bit   signed integer
typedef Int32  *pInt32;           // 32 bit   signed integer
typedef UInt8  *pUInt8;           //  8 bit unsigned integer
typedef UInt16 *pUInt16;          // 16 bit unsigned integer
typedef UInt32 *pUInt32;          // 32 bit unsigned integer
typedef void    Void, *pVoid;     // Void (typeless)
typedef Float  *pFloat;           // 32 bit floating point
typedef double  Double, *pDouble; // 32/64 bit floating point
typedef Bool   *pBool;            // Boolean (True/False)
typedef Char   *pChar;            // character, character array ptr
typedef Int    *pInt;             // machine-natural integer
typedef UInt   *pUInt;            // machine-natural unsigned integer
typedef String *pString;          // Null terminated 8 bit char str,

#define FALSE 0     // boolean false value
#define TRUE  1     // boolean true value
#define DWORD unsigned int

typedef struct _dbg_files_t {
	FILE *level_vlc;
	FILE *level_len;
	FILE *run_before;       
	FILE *run_before_len;
	FILE *tzeros; 
	FILE *t1s; 
	FILE *ctoken; 
	FILE *ctoken_len; 
	FILE *bsg_data; 
	FILE *residual; 
	FILE *tc_din; 	
	FILE *reconstructed; 
	FILE *cbpcy; 
	FILE *h264_cfg;	
	FILE *h264_cfg_qp;	
	FILE *h264_cfg_fr_type;	
	FILE *quant2x2dc;
	FILE *quant4x4dc;
	FILE *quant4x4ac;
	FILE *hp_mvx;
	FILE *hp_mvy;
	FILE *res;
	FILE *res_a;
	FILE *res_b;
	FILE *hp_mv_xy;
	FILE *qp_mv_xy;
	FILE *fp_mv_xy;
	FILE *writeback;
	FILE *writeback_ab;
	FILE *writeback_hex;
	FILE *mv_pred;
	FILE *mb_act;
	FILE *mb_cand;
	char mb_cand_buf[3][16*128+4];
}dbg_files_t, *pDbg_files_t;

//malik: TI. For reordering into 4x4 block scanning order according to h.264 book p125 fig 5.16       
static const Int8 luma_index[] = 
{
    0, 1, 4, 5,
    2, 3, 6, 7,
    8, 9, 12, 13,
    10, 11, 14, 15
};

typedef struct _hybrid_mode_t {
	Bool used_flag;
	Bool value;
}hybrid_mode_t, *pHybrid_mode_t;

typedef struct _pos_t{
	Int32 hor;
	Int32 ver;
}pos_t, *pPos_t;

typedef struct _blockSize_t{
	UInt32 rows;
	UInt32 cols;
}blockSize_t, *pBlockSize_t;

typedef enum _mbType_t
{
	INTRA,
	INTER,
	SKIP
}mbType_t, *pMbType_t;

typedef struct _vector_t {
	Int16 hor;
	Int16 ver;
	mbType_t mbType;
} vector_t, *pVector_t;

typedef enum _acLumIntraVlcTab_t
{
	lowMotion  = 0x00,
	highMotion = 0x01,
	midRate    = 0x02,
	highRate   = 0x03
} acLumIntraVlcTab_t, *pAcLumIntraVlcTab_t;
typedef enum _dcDiffVlcTab_t
{
	dcDiffLowMotion      = 0x00,
	dcDiffHighMotion     = 0x01
} dcDiffVlcTab_t, *pDcDiffVlcTab_t;

enum SCAN_METHOD 
{
	INTER_SCAN8X8, 
	INTRA_SCAN8X8
};

typedef struct _frameData_t
{
    UInt8 PQIndex;             /* Picture quantiser PQUANT */ 
    UInt8 dcDiffTab;           /* Intra block DC VLC table */ 
    //UInt8 acLumaIntraTab;      /* Luma Intra block AC VLC table */
    //UInt8 acChromaInterTab;    /* Chroma and Inter block AC VLC table */
	UInt8 acTab;               /* AC VLC table */
    UInt8 levelCsizeTab;       /* Escape code 3 table */
    UInt8 mvDiffTab;           /* Motion vector VLC table */
    UInt8 cbpcyTab;            /* Coded block pattern VLC table */
    UInt8 isNonUniformQ;       /* 0:uniform  1: non-uniform quantiser */  
    UInt8 isBIFrame;           /* 0: normal B-Frame  1: BI frame */
    UInt8 mbModeTab;           /* MB mode table */		
}frameData_t, *pFrameData_t;

typedef struct _block8x8Buff_t 
{
	//3D data [6][8][8]
	Int32 dctInp[6][8][8];        // DCT input data (residual in inter frames and current data in intra)
	Int16 dct4x4inp[6*4][16];	  // 6 blocks, 4subblocks, 16 pixel/subblock DCT input
	Int16 dct4x4out[6*4][16];	  // 6 blocks, 4subblocks, 16 pixel/subblock DCT input
	Int16 dct4x4outDcY[16];	      // luma DC
	Int16 dct2x2outDcU[4];	      // chroma DC
	Int16 dct2x2outDcV[4];	      // chroma DC
	Int32 dctOut[6][8][8];        // DCT coefficients after Quantization and postprocessing
	Int32 idctOut[6][8][8];       
	Int32 idct_input[6][8][8];        // DCT coefficients
	Int32 iDctInp[6][8][8];       // IDCT input data
	Int16 tcReconst[6][8][8];     // reconstructed after IDCT and Q^-1 
	UInt8 ***ppp_currentData;     // current pixel data 
	//2D data
	Int32 scanArray[6][64];       // zig-zag scan array of DCT coeffs
	UInt8 **pp_ref_lum;
	UInt8 **pp_ref_chr_u_gross;
	UInt8 **pp_ref_chr_v_gross;
	UInt8 **pp_ref_chr_u_gross_d;
	UInt8 **pp_ref_chr_v_gross_d;
	UInt8 **pp_ref_chr_u;
	UInt8 **pp_ref_chr_v; 
	UInt8 **pp_ref_lum_19x19;	  
	UInt8 **pp_ref_lum_32x24;	
	UInt8 **pp_ref_lum_17x17;
} block8x8Buff_t, *pBlock8x8Buff_t;

typedef struct _lineMemory_t
{
	Int32 dcCoeff[6]; //12bits/coeff
	UInt8 cbpcy;
	Int16 intraPR[16]; //intra 16x16 predictors right border
	Int16 intraPB[16]; //intra 16x16 predictors bottom border
	Int16 intraPRu[8]; //chroma u 8x8 predictors right border
	Int16 intraPBu[8]; //chroma u 8x8 predictors bottom border
	Int16 intraPRv[8]; //chroma v 8x8 predictors right border
	Int16 intraPBv[8]; //chroma v 8x8 predictors bottom border
	Int16 nnz[16 + 4 + 4];
}lineMemory_t, *pLineMemory_t;

typedef struct _mvMemory_t
{
	vector_t mv;                  //storage of MV's of a frame
	vector_t fp_mv;
	vector_t pred_mv;             //storage of MV's predictor of a frame
	hybrid_mode_t hybrid_mode;
	UInt8 bestMatch[6][8][8];     // ME best match data
	UInt32 skip;
}mvMemory_t, *pMvMemory_t;

//input interface
typedef struct _inputInterface_t
{
	UInt8 use_sl_epl;//use seq layer and entrypoint layer befor key frames
	UInt8 gop_length;
	UInt8 framerate_ind;
	UInt8 framerate_nr;
	UInt8 framerate_dr;
	UInt16 framerate_exp;
    UInt32  frame_height;
    UInt32  frame_width;
    UInt8  profile_and_level;
    UInt8  frame_rate;
	UInt8 brcIFrOffset;
	UInt8 PQIndexMin;
	UInt8 PQIndexMax;
	UInt8 PQIndexConst;
    UInt8  search_algorithm;
	UInt8  isQuarterSample; 
	UInt8  isDeblockFilterOn;   /* 0: Deblock filter off  1: Deblock filter on  */
	UInt8  isImplicitQ;         /* 0: explict Q   1: Implicit Quantizer signalling */
	UInt8  multiRes;            /* 0:off 1:h/2  2:v/2  3: h/2 v/2 */
	UInt8  isFastUV;
	UInt8  mvRange;
	frameData_t interH264Data;
	frameData_t intraH264Data;
	UInt32 numbFrames; //number of frames to be encoded
	Char   in_file_name[256];   
	Char   test_name[256];
	Bool   use_bstuffing;
	Bool   useHighProfile;
	Bool   disableFilter;
	Bool   usePsnr;
	UInt8  SkipMbYOffset;
	UInt8  SkipMbYSlope; 
	UInt8  SkipMbCOffset;
	UInt8  SkipMbCSlope; 
} inputInterface_t, *pInputInterface_t;

typedef enum _encodingType_t
{
	I  = 0,
	P  = 1,
	S  = 3//skip
} encodingType_t, *pEncodingType_t;

typedef struct _streamBuffer_t
{
	Void *pPictStream; //the buffer for encoded bitstream of one picture
	Void *pSequenceHeader;
	Void *pPictHeader;
	Void *pSliceHeader;
	//void *pPictureHeader;
}streamBuffer_t, *pStreamBuffer_t;

typedef struct _streamBufferInfo_t
{
	UInt32 pictStreamSize;   //size in Bytes
	UInt32 seqHeaderSize;    //size in Bytes
	UInt32 pictHdrSize;//size in Bytes
	UInt32 sliceHdrSize;//size in Bytes
	//UInt32 pictHeaderSize;   //size in Bytes
	Bool wrtSeqHdrFlag;
	Bool wrtPictureHdr;
}streamBufferInfo_t, *pStreamBufferInfo_t;

typedef struct _encFrame_t //frame metadata
{
	streamBufferInfo_t streamBufferInfo;
	streamBuffer_t streamBuffer;
	UInt8 *pInFrameData; //pointer to raw input stream content
	UInt8 *pOutFrameData; //pointer to raw output stream content of current
	UInt8 *pRefFrameData; //pointer to raw reconstructed frame
	UInt32 lastFrameBytes;
	UInt32 frameCntr;
	UInt32 h264_frame_num;
	pLineMemory_t pLineMemory1;//2 line memories to avoid complicated controling
	pLineMemory_t pLineMemory2;
	pMvMemory_t pMvMemory;
	pInputInterface_t pInputData;//stream level input data
	encodingType_t frameType;
	UInt8 PQIndex;
	UInt8 PQIndexInit;
	UInt8 dcDiffTab;           /* Intra block DC VLC table */ 
    //UInt8 acLumaIntraTab;      /* Luma Intra block AC VLC table */
    //UInt8 acChromaInterTab;    /* Chroma and Inter block AC VLC table */
	UInt8 acTab;               /* AC VLC table */
    UInt8 levelCsizeTab;       /* Escape code 3 table */
    UInt8 mvDiffTab;           /* Motion vector VLC table */
    UInt8 cbpcyTab;            /* Coded block pattern VLC table */
    UInt8 isNonUniformQ;       /* 0:uniform  1: non-uniform quantiser */  
	UInt8 mbModeTab;           /* MB mode table */
    UInt8 isBIFrame;           /* 0: normal B-Frame  1: BI frame */
	UInt32 vbvBufferSize;
	UInt32 stdRmax;
	UInt8 roundCtrl;
	Int32 bcrBuffer;
	UInt32 framerate;
	UInt32 sumMvX;
	UInt32 sumMvY;
	Bool isFirstFrame;
	Bool frame_is_noisy;
	Bool frame_is_skip;
	Bool frame_is_sc;
	UInt8 mvRange;
	FILE *fileMVectors_r;
	FILE *fileMVectors_w;
	UInt32 idr_pic_id;
	Int32 log2_max_pic_order_cnt_lsb_minus4;
	Int32 log2_max_frame_num_minus4;
	Int32 pic_order_cnt_lsb;
	UInt32 poc;
	UInt32 pic_order_cnt_type;
	dbg_files_t dbg_files;
}encFrame_t, *pEncFrame_t;

typedef struct _blockInfo_t
{
	UInt8  in_shift;
	UInt8 cbpcyTab;
	Bool isQuarterSample ;
	UInt8 mvDiffTab;
	UInt32 frame_mb_width;
	UInt32 frame_mb_height;
	Int16 mvX;
	Int16 mvY;
	UInt32 mbX;    // current MB x position
	UInt32 mbY;    // current MB y position
	UInt8  blockIdx;  // no. of current block in macro block (0..3=luma, 4, 5= chroma)
	Bool acPredFlag; //ac prediction flag
	block8x8Buff_t block8x8Buff;
	Int32 nnz[16 + 4 + 4];
	mbType_t mbType;
	Int32 mquantY;
	Int32 mquantC;
	Bool level_shift;
	UInt8 default_dc_pred;
	UInt8 isNonUniformQ;       /* 0:uniform  1: non-uniform quantiser */  
	pLineMemory_t pPrevLineMem;
	pLineMemory_t pCurrLineMem;
	UInt32 mbNumb;
	UInt16 DCStepSize;
	Int32 dcPredictionDir[6];
	Int32 dcPredictor[6];
	UInt32 sliceMbNumb;//number of MB in line
	UInt8 dcDiffTab;             /* Intra block DC VLC table */ 
    //UInt8 acLumaIntraTab;      /* Luma Intra block AC VLC table */
    //UInt8 acChromaInterTab;    /* Chroma and Inter block AC VLC table */
	UInt8 acTab;                 /* Chroma and Inter block AC VLC table */
    UInt8 levelCsizeTab;         /* Escape code 3 table */
	Bool esc3_control;
	Bool last_mb;
	UInt8 cbp_y;
	UInt8 cbp_c;
    UInt8 mode_i16x16;
	UInt32 max_bits_per_mb;
	encodingType_t frameType;
	UInt8 mvRange;
	UInt8 roundCtrl;
	UInt8 isFastUV;
	pMvMemory_t pMvMemory; 
	UInt32 skip_sad_thr;
	UInt32 intra_sad_thr;
	UInt32 frameCntr;
	Int32 intra16x16DCPred;
	Int32 intra8x8ChromaPred_u[8];
	Int32 intra8x8ChromaPred_v[8];
	UInt8 chromaPredDir;
	UInt32 skip_run;
	UInt32 skip;
	pEncFrame_t pEncFrame;//only for debug
}blockInfo_t, *pBlockInfo_t;

Void writeStreamToFile(pEncFrame_t pEncFrame, FILE *fileStream);
double calcPSNR(unsigned char *in, unsigned char *orig, int n);

#endif /* GLOBAL_H_ */
