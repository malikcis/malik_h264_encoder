/*****************************************************************************  
 *  
 *  H264 AVC CODEC  
 *  
 *  Copyright(C) 2020 llcc <malik.cisse@abateck.com>  
 *  
 *  This program is free software ; you can redistribute it and/or modify  
 *  it under the terms of the GNU General Public License as published by  
 *  the Free Software Foundation ; either version 2 of the License, or  
 *  (at your option) any later version.  
 *  
 *  This program is distributed in the hope that it will be useful,  
 *  but WITHOUT ANY WARRANTY ; without even the implied warranty of  
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the  
 *  GNU General Public License for more details.  
 *  
 *  You should have received a copy of the GNU General Public License  
 *  along with this program ; if not, write to the Free Software  
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA  
 *  
 ****************************************************************************/ 
#ifndef BITSTREAM_H_
#define BITSTREAM_H_
#include "global.h"

void    bitstream_Init(void *buffer);
void  	bitstream_PutBits (UInt32 n, UInt32 val);
UInt32 	bitstream_Close (void);
UInt32 	bitstream_Close_32bits (UInt32 seq_hdr_len, UInt32 epl_hdr_len);
UInt32 	bitstream_Close_16bits (void);
UInt32 	bitstream_Stuffing (UInt32 seq_len);
UInt32	bitstream_GetLength(void);
void    start_code_emul_prevention(void);
void	set_scep(Bool scep);
void	eg_write_ue(int code_num, int *len, int *data);
void    eg_write_se(int code_num, int *len, int *data);

#endif /* _MOM_BITSTREAM_I_H_ */

