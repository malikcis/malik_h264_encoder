/*****************************************************************************  
 *  
 *  H264 AVC CODEC  
 *  
 *  Copyright(C) 2020 llcc <malik.cisse@abateck.com>  
 *  
 *  This program is free software ; you can redistribute it and/or modify  
 *  it under the terms of the GNU General Public License as published by  
 *  the Free Software Foundation ; either version 2 of the License, or  
 *  (at your option) any later version.  
 *  
 *  This program is distributed in the hope that it will be useful,  
 *  but WITHOUT ANY WARRANTY ; without even the implied warranty of  
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the  
 *  GNU General Public License for more details.  
 *  
 *  You should have received a copy of the GNU General Public License  
 *  along with this program ; if not, write to the Free Software  
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA  
 *  
 ****************************************************************************/ 
#ifndef MEM_CONTROL_H_
#define MEM_CONTROL_H_

#include "global.h"

void mbWriteRefToMem(pBlockInfo_t pBlockInfo, pEncFrame_t pEncFrame);
void writeLumBlock(pBlockInfo_t pBlockInfo, pEncFrame_t pEncFrame);
void writeChrBlock(pBlockInfo_t pBlockInfo, pEncFrame_t pEncFrame);
void writeCurrMemForNnz(pBlockInfo_t pBlockInfo);
void writeCurrMemForIntraPred(pBlockInfo_t pBlockInfo);

void mbMemRead(UInt32 frame_width, 
			   UInt32 frame_height, 
			   UInt32 row, 
			   UInt32 col, 
			   UInt8 *pPicture_data,
			   UInt8 ***mb_data);

void me_get_padded_lum_block(UInt32 frame_width, 
						UInt32 frame_height, 
						pos_t pos,
						blockSize_t blockSize,
						UInt8 *pPicture_data,
						UInt8 **mb_data);

void me_get_padded_chr_blocks(UInt32 frame_width, 
						UInt32 frame_height, 
						pos_t pos,
						blockSize_t blockSize,
						UInt8 *pPicture_data,
						UInt8 **data_u,
						UInt8 **data_v);

void get_mem2D(UInt8 ***array2D, Int32 rows, Int32 columns);
void get_mem3D(UInt8 ****array3D, Int32 idx, Int32 rows, Int32 columns);
void free_mem2D(UInt8 **array2D);
void free_mem3D(UInt8 ***array2D, Int32 idx);


#endif /* MEM_CONTROL_H_ */

