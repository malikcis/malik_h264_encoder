# malik_h264_encoder

This is an H.264 open source encoder project written in plain ansi-C. It is intended for video compressionists such as students or engineers who want to dig into the details or understand the standard.
Usual H.264 encoder implementations are optimized for speed whereas malik_h264_encoder is optimized for readability and exploration. no difficult or fancy C code is used such as pointers pointer (at least only very few :-)).
The code is much more compact compared to the JM joint model.

## About the encoder

An extensive documentation of the encoder is provided on my [Video Expert Blog](https://videoexpert.tech/h-264-encoder/)  where I invite you to discuss issues or ideas. As a freelancer consultant I am also open to work on related topics.

The code can also be used as a bit true model to develop IPs for FPGAs, ASICs as well as GPU.

H.264 or AVC constrained baseline profile is implemented which is the simplest form of the standard thus supporting the smallest feature set. Main features implemented here are CAVLC, 4x4 DTC , P-frames only and a simplified motion estimation.

## Demo

To demonstrate the encoder, a precompiled binary file "h264_encoder.exe" is placed in "test" directory. Just launch "h264_encoder.exe" and a H.264 elementary stream video gets recorded to test_1280x720.264.
The program uses "test_1280x720_cmodel.yuv" raw YUV 4:2:0 planar input video and compresses it using configuration parameters in "options.cfg". Only 3 frames are used in order to avoid heavy YUV raw input files.
The resulting video can be watched using VLC.

## Building

Building the malik_h264_encoder requires the Microsoft Visual Studio 2010 or later environment.
No dependency to external library is needed and 100% of the code is written from scratch in plain C.

Get the source code:

```shell
git clone git@gitlab.com:malikcis/malik_h264_encoder.git
```

## Resources

This is a self contained project and no outside resources are needed.

### Documentation

The documentation for the malik_h264_encoder is available on my [Video Expert Blog](https://videoexpert.tech/h-264-encoder/)


### Reporting Issues

You can report issues on [here](https://videoexpert.tech/h-264-encoder/)  Or get in touch with me directly using my email address: malik.cisse@abateck.com

## Licensing

This program is free software. You can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation; either version 2 of the License, or (at your option) any later version. 
T264 (developed in 2004, 2005), an early fork of the x264 encoder has been used as a source of inspiration. especially the bitstream generator is partly inspired by this code.
T264 project is dead and is no longer officially available.

## About Me

I am Malik Cisse,

As an established and dedicated engineer with a passion for embedded software development, I Have progressed through a series of roles over a comprehensive years of experience in domains involving video compression and Image/Video processing of all sort.
I am a freelancer embedded software developer since 2018 an open to work on projects in this area.

My skills (among others):
- C
- C++
- Script Languages
- Python
- Yocto Linux

You will find more information about me here:
- [About me](https://videoexpert.tech/about/)
- [LinkedIn](https://www.linkedin.com/in/malik-cisse-6150672/)

