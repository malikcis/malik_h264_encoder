/*****************************************************************************  
 *  
 *  H264 AVC CODEC  
 *  
 *  Copyright(C) 2020 llcc <malik.cisse@abateck.com>  
 *  
 *  This program is free software ; you can redistribute it and/or modify  
 *  it under the terms of the GNU General Public License as published by  
 *  the Free Software Foundation ; either version 2 of the License, or  
 *  (at your option) any later version.  
 *  
 *  This program is distributed in the hope that it will be useful,  
 *  but WITHOUT ANY WARRANTY ; without even the implied warranty of  
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the  
 *  GNU General Public License for more details.  
 *  
 *  You should have received a copy of the GNU General Public License  
 *  along with this program ; if not, write to the Free Software  
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA  
 *  
 ****************************************************************************/ 
#include "bsg.h"
#include "bitstream.h"

#define BLOCK_INDEX_CHROMA_DC   (-1)
#define BLOCK_INDEX_LUMA_DC     (-2)

#define GLOBAL_DEBUG 0

//debug only
#define DBG_PRINT_LEVEL_VLC      0  
#define DBG_PRINT_LEVEL_LEN      0 
#define DBG_CTOKEN               0
#define DBG_CTOKEN_LEN           0
#define DBG_T1S                  0
#define DBG_TOTAL_ZEROS          0
#define DBG_RUN_BEFORE           0
#define DBG_CBPCY                0

static const UInt8 inter_cbp_to_golomb[48]=
{
  0,  2,  3,  7,  4,  8, 17, 13,  5, 18,  9, 14, 10, 15, 16, 11,
  1, 32, 33, 36, 34, 37, 44, 40, 35, 45, 38, 41, 39, 42, 43, 19,
  6, 24, 25, 20, 26, 21, 46, 28, 27, 47, 22, 29, 23, 30, 31, 12
};

#define T264_MIN(a,b) ( (a)<(b) ? (a) : (b) )
#define ABS(x) ((x) > 0 ? (x) : -(x))

static __inline void eg_write_vlc(vlc_t v)
{
	bitstream_PutBits(v.i_size, v.i_bits & ((UInt32)~0 >> (UInt32)(32 - v.i_size)));
}

//t264enc.c 1426 -> cavlc.c 338
void T264_macroblock_write_cavlc(pBlockInfo_t pBlockInfo)//bitstream_generation
{
	Int32 offset;
	Int32 i;
	int eg_len, eg_data;	

#if GLOBAL_DEBUG
	if(pBlockInfo->frameCntr == 0)
	{
		FPRINTF_MC(pBlockInfo->pEncFrame->dbg_files.mv_pred, "#Fr Nr: %d, MB(%d, %d), pred_x, pred_y, x, y\n",pBlockInfo->frameCntr, pBlockInfo->mbX, pBlockInfo->mbY);		
		FPRINTF_MC(pBlockInfo->pEncFrame->dbg_files.mv_pred, "?\t%d\t%d\t%d\t%d\n",pBlockInfo->pMvMemory[pBlockInfo->mbNumb].pred_mv.hor, pBlockInfo->pMvMemory[pBlockInfo->mbNumb].pred_mv.ver, pBlockInfo->pMvMemory[pBlockInfo->mbNumb].mv.hor, pBlockInfo->pMvMemory[pBlockInfo->mbNumb].mv.ver);						
		
	}
	else//P
	{
		if(pBlockInfo->mbType == INTRA)
		{
			FPRINTF_MC(pBlockInfo->pEncFrame->dbg_files.mv_pred, "#Fr Nr: %d, MB(%d, %d), pred_x, pred_y, x, y\n",pBlockInfo->frameCntr, pBlockInfo->mbX, pBlockInfo->mbY);		
			FPRINTF_MC(pBlockInfo->pEncFrame->dbg_files.mv_pred, "?\t%d\t%d\t%d\t%d\n",pBlockInfo->pMvMemory[pBlockInfo->mbNumb].pred_mv.hor/2, pBlockInfo->pMvMemory[pBlockInfo->mbNumb].pred_mv.ver/2, pBlockInfo->pMvMemory[pBlockInfo->mbNumb].mv.hor/2, pBlockInfo->pMvMemory[pBlockInfo->mbNumb].mv.ver/2);									
		}
		else
		{
			FPRINTF_MC(pBlockInfo->pEncFrame->dbg_files.mv_pred, "#Fr Nr: %d, MB(%d, %d), pred_x, pred_y, x, y\n",pBlockInfo->frameCntr, pBlockInfo->mbX, pBlockInfo->mbY);		
			FPRINTF_MC(pBlockInfo->pEncFrame->dbg_files.mv_pred, "$\t%d\t%d\t%d\t%d\n",pBlockInfo->pMvMemory[pBlockInfo->mbNumb].pred_mv.hor/2, pBlockInfo->pMvMemory[pBlockInfo->mbNumb].pred_mv.ver/2, pBlockInfo->pMvMemory[pBlockInfo->mbNumb].mv.hor/2, pBlockInfo->pMvMemory[pBlockInfo->mbNumb].mv.ver/2);						
		}
	}
#endif 	

	//do intra mode decision elsewhere later
	pBlockInfo->mode_i16x16 = 2; //DC	

	//T264_macroblock_write_cavlc(T264_t *t)
	if (pBlockInfo->frameType == I)
    {
        offset = 0;
    }
    else//P-frames
    {
        offset = 5;
    }

	if(pBlockInfo->frameType != I)
    {        	
		// skip run			
		//eg_write_ue(pBlockInfo->skip_run, &eg_len, &eg_data);
		//bitstream_PutBits(eg_len, eg_data);		
		if(pBlockInfo->skip && !pBlockInfo->last_mb)	
		{
			return;
		}
		else
		{		
			// skip run			
			eg_write_ue(pBlockInfo->skip_run, &eg_len, &eg_data);
			bitstream_PutBits(eg_len, eg_data);		
			pBlockInfo->skip_run = 0;
			if(pBlockInfo->skip && pBlockInfo->last_mb)
			{
				bitstream_PutBits(1, 1);//stop bit
				return;
			}
		}
    }

	if (pBlockInfo->mbType == INTRA)
    {
		int mb_qp_delta = 0;
		
        // mb_type
		int mb_type = offset + 1 + pBlockInfo->mode_i16x16 + pBlockInfo->cbp_c * 4 + (pBlockInfo->cbp_y == 0 ? 0 : 12);
		eg_write_ue(mb_type, &eg_len, &eg_data);
        bitstream_PutBits(eg_len, eg_data);

		// intra chroma pred mode
		eg_write_ue(pBlockInfo->chromaPredDir, &eg_len, &eg_data);
        bitstream_PutBits(eg_len, eg_data);

        // delta qp		
        eg_write_se(mb_qp_delta, &eg_len, &eg_data);
		bitstream_PutBits(eg_len, eg_data);		

        // dc luma
        block_residual_write_cavlc(pBlockInfo, BLOCK_INDEX_LUMA_DC, pBlockInfo->block8x8Buff.dct4x4outDcY, 16);

        //if (pBlockInfo->cbp_y != 0)
        //{
            for(i = 0 ; i < 16 ; i ++)
            {							
				block_residual_write_cavlc(pBlockInfo, i, &(pBlockInfo->block8x8Buff.dct4x4out[i][1]), 15);//LUMA AC         
            }
        //}
    }
    else//P-MB
    {
        vector_t delta_vector;

		//transmit mb_type
		eg_write_ue(MB_16x16, &eg_len, &eg_data);
        bitstream_PutBits(eg_len, eg_data);
        
        //vec = t->mb.vec[0][0];
		//T264_predict_mv(t, 0, 0, 4, &vec);		
		delta_vector.hor = pBlockInfo->pMvMemory[pBlockInfo->mbNumb].mv.hor - pBlockInfo->pMvMemory[pBlockInfo->mbNumb].pred_mv.hor;   //qp    
		delta_vector.ver = pBlockInfo->pMvMemory[pBlockInfo->mbNumb].mv.ver - pBlockInfo->pMvMemory[pBlockInfo->mbNumb].pred_mv.ver;   //qp
        
		//transmit MV x: mvd_l0
		//eg_write_se(t->bs, t->mb.vec[0][0].x - vec.x);
		eg_write_se(delta_vector.hor, &eg_len, &eg_data);
		bitstream_PutBits(eg_len, eg_data);

		//transmit MV y: mvd_l0
		//eg_write_se(t->bs, t->mb.vec[0][0].y - vec.y);
		eg_write_se(delta_vector.ver, &eg_len, &eg_data);
		bitstream_PutBits(eg_len, eg_data);                       
       
        //transmit coded_block_pattern: cbp. see tab 9-4 p.202      
        eg_write_ue(inter_cbp_to_golomb[(pBlockInfo->cbp_c << 4)| pBlockInfo->cbp_y], &eg_len, &eg_data);
        bitstream_PutBits(eg_len, eg_data);

        if (pBlockInfo->cbp_y > 0 || pBlockInfo->cbp_c > 0)
        {
			//transmit delta qp
			int mb_qp_delta = 0;// 0 = no change on qp 
			eg_write_se(mb_qp_delta, &eg_len, &eg_data);
			bitstream_PutBits(eg_len, eg_data);
		}
        for (i = 0; i < 16 ; i ++)
        {
            //if(pBlockInfo->cbp_y & (1 << ( i / 4 )))
            //{                  
				block_residual_write_cavlc(pBlockInfo, i, pBlockInfo->block8x8Buff.dct4x4out[i], 16);//LUMA AC
            //}
        }
        //}
    }

	//chroma
	//if (pBlockInfo->cbp_c != 0)
    //{
        block_residual_write_cavlc(pBlockInfo, BLOCK_INDEX_CHROMA_DC, pBlockInfo->block8x8Buff.dct2x2outDcU, 4);//CHROMA DC
        block_residual_write_cavlc(pBlockInfo, BLOCK_INDEX_CHROMA_DC, pBlockInfo->block8x8Buff.dct2x2outDcV, 4);//CHROMA DC
        //if (pBlockInfo->cbp_c & 0x2)
        //{
            for(i = 0 ; i < 8 ; i ++)
            {
                block_residual_write_cavlc(pBlockInfo, 16 + i, &(pBlockInfo->block8x8Buff.dct4x4out[i+16][1]), 15);//CHROMA AC
            }
        //}
    //}	

	
	if(pBlockInfo->last_mb)
	{
		bitstream_PutBits(1, 1);//stop bit
	}
	
}

/****************************************************************************
 * block_residual_write_cavlc
 ****************************************************************************/
void block_residual_write_cavlc( pBlockInfo_t pBlockInfo, Int32 i_idx, Int16 *coeffs, Int32 i_count )
{
    int level[16], run[16];
    int i_total, i_trailing;
    int i_total_zero;
    int i_last;
    unsigned int i_sign;
	int use_ctoken;
    int i;
    int i_zero_left;
    int i_suffix_length;

    /* first find i_last */
    i_last = i_count - 1;
    while( i_last >= 0 && coeffs[i_last] == 0 )
    {
        i_last--;
    }

    i_sign = 0;
    i_total = 0;
    i_trailing = 0;
    i_total_zero = 0;

	//If there are coefficients in that 4x4 block: i_last >= 0
    if( i_last >= 0 )
    {
        int b_trailing = 1;
        int idx = 0;

        /* level and run and total */ 
        while( i_last >= 0 )
        {
            level[idx] = coeffs[i_last--];

            run[idx] = 0;
            while( i_last >= 0 && coeffs[i_last] == 0 )
            {
                run[idx]++;
                i_last--;
            }

            i_total++;
            i_total_zero += run[idx];

            if( b_trailing && ABS( level[idx] ) == 1 && i_trailing < 3 )
            {
                i_sign <<= 1;
                if( level[idx] < 0 )
                {
                    i_sign |= 0x01;
                }

                i_trailing++;
            }
            else
            {
                b_trailing = 0;
            }

            idx++;
        }
    }

	use_ctoken = FALSE;
	if (pBlockInfo->mbType == INTRA)
	{

		// dc luma
		if(i_idx == BLOCK_INDEX_LUMA_DC)
		{
			use_ctoken = TRUE;
		}
		else if (pBlockInfo->cbp_y != 0)
		{
			if(i_idx >= 0 && i_idx < 16)
			{				
				use_ctoken = TRUE;
			}
		}
	}
	else//P-MB
	{
		if (pBlockInfo->cbp_y > 0 || pBlockInfo->cbp_c > 0)
		{
			if(i_idx >= 0 && i_idx < 16)
			{
				if(pBlockInfo->cbp_y & (1 << ( i_idx / 4 )))
				{                  
					use_ctoken = TRUE;
				}
			}
		}
	}

	//chroma
	if (pBlockInfo->cbp_c != 0)
	{
		if(i_idx == BLOCK_INDEX_CHROMA_DC)
		{
			use_ctoken = TRUE;
		}
		else if (pBlockInfo->cbp_c & 0x2)
		{
			if(i_idx > 15 && i_idx < 24)
			{				
				use_ctoken = TRUE;
			}
		}
	}


	/* total/trailingOnes represented by Coeff_token (5 tables)*/
	if( i_idx == BLOCK_INDEX_CHROMA_DC )
	{
		if(use_ctoken)
			eg_write_vlc(x264_coeff_token[4][i_total*4+i_trailing]);
		//printf("nr1: len: %d, data: %d\n", x264_coeff_token[4][i_total*4+i_trailing].i_size, x264_coeff_token[4][i_total*4+i_trailing].i_bits);
#if DBG_CTOKEN
		//printf("%d\n", x264_coeff_token[4][i_total*4+i_trailing].i_bits);
		FPRINTF_MC(pBlockInfo->pEncFrame->dbg_files.ctoken, "%d\n", x264_coeff_token[4][i_total*4+i_trailing].i_bits);
#endif
#if DBG_CTOKEN_LEN
		//printf("%d\n", x264_coeff_token[4][i_total*4+i_trailing].i_size);
		FPRINTF_MC(pBlockInfo->pEncFrame->dbg_files.ctoken_len, "%d\n", x264_coeff_token[4][i_total*4+i_trailing].i_size);
#endif
	}
	else
	{        
		static const int ct_index[17] = {0,0,1,1,2,2,2,2,3,3,3,3,3,3,3,3,3 };//see page 207 std: 0<=nC<2, 2<=nC<4 etc...
		int nC = 0;

		if( i_idx == BLOCK_INDEX_LUMA_DC )
		{
			// predict nC = (nA + nB) / 2;
			nC = T264_mb_predict_non_zero_code(pBlockInfo, 0);
		}
		else
		{
			// predict nC = (nA + nB) / 2;
			nC = T264_mb_predict_non_zero_code(pBlockInfo, i_idx);
		}
		if(use_ctoken)
			eg_write_vlc(x264_coeff_token[ct_index[nC]][i_total*4+i_trailing]);
		//printf("nr2: len: %d, data: %d\n", x264_coeff_token[ct_index[nC]][i_total*4+i_trailing].i_size, x264_coeff_token[ct_index[nC]][i_total*4+i_trailing].i_bits);
#if DBG_CTOKEN
		//printf("%d\n", x264_coeff_token[ct_index[nC]][i_total*4+i_trailing].i_bits);
		FPRINTF_MC(pBlockInfo->pEncFrame->dbg_files.ctoken, "%d\n", x264_coeff_token[ct_index[nC]][i_total*4+i_trailing].i_bits);
#endif
#if DBG_CTOKEN_LEN
		//printf("%d\n", x264_coeff_token[ct_index[nC]][i_total*4+i_trailing].i_size);
		FPRINTF_MC(pBlockInfo->pEncFrame->dbg_files.ctoken_len, "%d\n", x264_coeff_token[ct_index[nC]][i_total*4+i_trailing].i_size);
#endif
	}


    if( i_total <= 0 )
    {
        return;
    }

	//encoding trailing '1's (max = 3)
    if( i_trailing > 0 )
    {
		//eg_write_direct(h->bs, i_sign & ((UInt32)~0 >> (UInt32)(32 - i_trailing)), i_trailing);
		bitstream_PutBits(i_trailing, i_sign & ((UInt32)~0 >> (UInt32)(32 - i_trailing)));
#if DBG_T1S
		//printf("%d\n", i_sign & ((UInt32)~0 >> (UInt32)(32 - i_trailing)));
		FPRINTF_MC(pBlockInfo->pEncFrame->dbg_files.t1s, "%d\n", i_sign & ((UInt32)~0 >> (UInt32)(32 - i_trailing)));
#endif
    }

	//encoding remaining levels of non-zero coefficients (prefix + suffix)
	i_suffix_length = i_total > 10 && i_trailing < 3 ? 1 : 0;

	
    for( i = i_trailing; i < i_total; i++ )
    {
        int i_level_code;

        /* calculate level code */
        if( level[i] < 0 )
        {
            i_level_code = -2*level[i] - 1;
        }
        else /* if( level[i] > 0 ) */
        {
            i_level_code = 2 * level[i] - 2;
        }
        if( i == i_trailing && i_trailing < 3 )
        {
            i_level_code -=2; /* as level[i] can't be 1 for the first one if i_trailing < 3 */
        }
				
        if( ( i_level_code >> i_suffix_length ) < 14 )//HW case 1
        {            
			//printf("nr3: len: %d, data: %d\n", x264_level_prefix[i_level_code >> i_suffix_length].i_size, x264_level_prefix[i_level_code >> i_suffix_length].i_bits);
            if( i_suffix_length > 0 )
            {
				int prefix, suffix, vlc, len;
				len    = x264_level_prefix[i_level_code >> i_suffix_length].i_size + i_suffix_length;
				prefix = x264_level_prefix[i_level_code >> i_suffix_length].i_bits;
				suffix = i_level_code & ((UInt32)~0 >> (UInt32)(32 - i_suffix_length));
				vlc    = (prefix << i_suffix_length) | suffix;
#if DBG_PRINT_LEVEL_VLC
				FPRINTF_MC(pBlockInfo->pEncFrame->dbg_files.level_vlc, "%d\n",vlc);
				FPRINTF_MC(pBlockInfo->pEncFrame->dbg_files.level_len, "%d\n",len);
#endif

				eg_write_vlc(x264_level_prefix[i_level_code >> i_suffix_length]);
				bitstream_PutBits(i_suffix_length, i_level_code & ((UInt32)~0 >> (UInt32)(32 - i_suffix_length)));				
            }
			else
			{
				int prefix, vlc, len;
				len    = x264_level_prefix[i_level_code >> i_suffix_length].i_size;
				prefix = x264_level_prefix[i_level_code >> i_suffix_length].i_bits;				
				vlc    = prefix;
#if DBG_PRINT_LEVEL_VLC
				FPRINTF_MC(pBlockInfo->pEncFrame->dbg_files.level_vlc, "%d\n",vlc);
				FPRINTF_MC(pBlockInfo->pEncFrame->dbg_files.level_len, "%d\n",len);
#endif
#if DBG_PRINT_LEVEL_LEN
				printf("%d\n", len);
#endif

				eg_write_vlc(x264_level_prefix[i_level_code >> i_suffix_length]);
			}
        }
        else if( i_suffix_length == 0 && i_level_code < 30 )//HW case 2
        {		
			int prefix, suffix, vlc, len;
			len    = x264_level_prefix[14].i_size + 4;
			prefix = x264_level_prefix[14].i_bits;
			suffix = (i_level_code - 14) & ((UInt32)~0 >> (UInt32)(32 - 4));
			vlc    = (prefix << 4) | suffix;
#if DBG_PRINT_LEVEL_VLC
			FPRINTF_MC(pBlockInfo->pEncFrame->dbg_files.level_vlc, "%d\n",vlc);
			FPRINTF_MC(pBlockInfo->pEncFrame->dbg_files.level_len, "%d\n",len);
#endif
#if DBG_PRINT_LEVEL_LEN
				printf("%d\n", len);
#endif

            eg_write_vlc(x264_level_prefix[14]);
			//printf("nr4: len: %d, data: %d\n", x264_level_prefix[14].i_size, x264_level_prefix[14].i_bits);
			bitstream_PutBits(4, (i_level_code - 14) & ((UInt32)~0 >> (UInt32)(32 - 4)));
        }
        else if( i_suffix_length > 0 && ( i_level_code >> i_suffix_length ) == 14 )//HW case 3
        {
			int prefix, suffix, vlc, len;
			len    = x264_level_prefix[14].i_size + i_suffix_length;
			prefix = x264_level_prefix[14].i_bits;
			suffix = i_level_code & ((UInt32)~0 >> (UInt32)(32 - i_suffix_length));
			vlc    = (prefix << i_suffix_length) | suffix;
#if DBG_PRINT_LEVEL_VLC
			FPRINTF_MC(pBlockInfo->pEncFrame->dbg_files.level_vlc, "%d\n",vlc);
			FPRINTF_MC(pBlockInfo->pEncFrame->dbg_files.level_len, "%d\n",len);
#endif
#if DBG_PRINT_LEVEL_LEN
			printf("%d\n", len);
#endif

            eg_write_vlc(x264_level_prefix[14]);
			//printf("nr5: len: %d, data: %d\n", x264_level_prefix[14].i_size, x264_level_prefix[14].i_bits);
			bitstream_PutBits(i_suffix_length, i_level_code & ((UInt32)~0 >> (UInt32)(32 - i_suffix_length)));
        }
        else//HW case 4
        {
			int prefix, suffix, vlc, len;		

            eg_write_vlc(x264_level_prefix[15]);
			//printf("nr6: len: %d, data: %d\n", x264_level_prefix[15].i_size, x264_level_prefix[15].i_bits);
            i_level_code -= 15 << i_suffix_length;
            if( i_suffix_length == 0 )
            {
                i_level_code -= 15;
            }

			//If the prefix size exceeds 15, High Profile is required. 
            if( i_level_code >= ( 1 << 12 ) || i_level_code < 0 )
            {
				//see x.264
                FPRINTF_MC( stderr, "OVERFLOW levelcode=%d\n", i_level_code );
				/* clip level, preserving sign */
				i_level_code = (1<<12) - 2 + (i_level_code & 1);
            }

			len    = x264_level_prefix[15].i_size + 12;
			prefix = x264_level_prefix[15].i_bits;
			suffix = i_level_code & ((UInt32)~0 >> (UInt32)(32 - 12));
			vlc    = (prefix << 12) | suffix;
#if DBG_PRINT_LEVEL_VLC
			FPRINTF_MC(pBlockInfo->pEncFrame->dbg_files.level_vlc, "%d\n",vlc);
			FPRINTF_MC(pBlockInfo->pEncFrame->dbg_files.level_len, "%d\n",len);
#endif	
#if DBG_PRINT_LEVEL_LEN
			printf("%d\n", len);
#endif

			bitstream_PutBits(12, i_level_code & ((UInt32)~0 >> (UInt32)(32 - 12)));
        }

        if( i_suffix_length == 0 )
        {
            i_suffix_length++;
        }
        if( ABS( level[i] ) > ( 3 << ( i_suffix_length - 1 ) ) && i_suffix_length < 6 )
        {
            i_suffix_length++;
        }
    }

	//encode total zeros [i_total-1][i_total_zero]. tab.9-7, P211
    if( i_total < i_count )//there are zeros. 
    {
        if( i_idx == BLOCK_INDEX_CHROMA_DC )
        {
            eg_write_vlc(x264_total_zeros_dc[i_total-1][i_total_zero]);
			//printf("nr7: len: %d, data: %d\n", x264_total_zeros_dc[i_total-1][i_total_zero].i_size, x264_total_zeros_dc[i_total-1][i_total_zero].i_bits);
#if DBG_TOTAL_ZEROS
			FPRINTF_MC(pBlockInfo->pEncFrame->dbg_files.tzeros, "%d\n", x264_total_zeros_dc[i_total-1][i_total_zero].i_bits);
#endif
        }
        else
        {
            eg_write_vlc(x264_total_zeros[i_total-1][i_total_zero]);
			//printf("nr8: len: %d, data: %d\n", x264_total_zeros[i_total-1][i_total_zero].i_size, x264_total_zeros[i_total-1][i_total_zero].i_bits);
#if DBG_TOTAL_ZEROS
			FPRINTF_MC(pBlockInfo->pEncFrame->dbg_files.tzeros, "%d\n", x264_total_zeros[i_total-1][i_total_zero].i_bits);
#endif
        }
    }

	//encode each run of zeros
    for( i = 0, i_zero_left = i_total_zero; i < i_total - 1; i++ )
    {
        int i_zl;

        if( i_zero_left <= 0 )
        {
            break;
        }

        i_zl = T264_MIN( i_zero_left - 1, 6 );

		eg_write_vlc(x264_run_before[i_zl][run[i]]);
		//printf("nr9: len: %d, data: %d\n", x264_run_before[i_zl][run[i]].i_size, x264_run_before[i_zl][run[i]].i_bits);
#if DBG_RUN_BEFORE	
		FPRINTF_MC(pBlockInfo->pEncFrame->dbg_files.run_before, "%d\n",x264_run_before[i_zl][run[i]].i_bits);
		FPRINTF_MC(pBlockInfo->pEncFrame->dbg_files.run_before_len, "%d\n",x264_run_before[i_zl][run[i]].i_size);
#endif

        i_zero_left -= run[i];
    }
}

//calculate non-zero counts for an array v[i_count]
Int32 array_non_zero_count(Int16 *v, Int32 i_count)
{
    Int32 i;
    Int32 i_nz;

    for( i = 0, i_nz = 0; i < i_count; i++ )
    {
        if( v[i] )
        {
            i_nz++;
        }
    }
    return i_nz;
}

void h264_cbpcy_calc(pBlockInfo_t pBlockInfo)
{  
	Int32 i;
	Int32 dc_nz0, dc_nz1, cbp_dc;
	cbp_dc = 0;

    if (pBlockInfo->mbType == INTRA)
    {
        pBlockInfo->cbp_y = 0;
        for(i = 0; i < 16 ; i ++)
        {
            //Int32 x, y;
            const Int32 nz = array_non_zero_count(&(pBlockInfo->block8x8Buff.dct4x4out[i][1]), 15);           
			pBlockInfo->nnz[i] = nz;
            if( nz > 0 )
            {
                //pBlockInfo->cbp_y = 0x0f;
				pBlockInfo->cbp_y |= 1 << (i / 4);//one can take line above. It does no mater
            }
        }
    }	
    else
    {
        pBlockInfo->cbp_y = 0;
        for(i = 0; i < 16; i ++)
        {            
            const Int32 nz = array_non_zero_count(pBlockInfo->block8x8Buff.dct4x4out[i], 16);
			pBlockInfo->nnz[i] = nz;
            if( nz > 0 )
            {
                pBlockInfo->cbp_y |= 1 << (i / 4);
            }
        }
    }

    /* Calculate the chroma patern */
    pBlockInfo->cbp_c = 0;
    for(i = 0; i < 8; i ++)
    {
        const int nz = array_non_zero_count(&(pBlockInfo->block8x8Buff.dct4x4out[i+16][1]), 15);
        pBlockInfo->nnz[i + 16] = nz;        
        if( nz > 0 )
        {
            pBlockInfo->cbp_c = 0x02;    /* dc+ac */
        }
    }
	dc_nz0 = array_non_zero_count(pBlockInfo->block8x8Buff.dct2x2outDcU, 4) > 0;
	dc_nz1 = array_non_zero_count(pBlockInfo->block8x8Buff.dct2x2outDcV, 4) > 0;
    if(pBlockInfo->cbp_c == 0x00 && (dc_nz0 || dc_nz1))
    {
        pBlockInfo->cbp_c = 0x01;    /* dc only */
    }
	
#if DBG_CBPCY
	FPRINTF_MC(pBlockInfo->pEncFrame->dbg_files.cbpcy, "%d\n",(pBlockInfo->cbp_c << 4) | pBlockInfo->cbp_y);
	//FPRINTF_MC(pBlockInfo->pEncFrame->dbg_files.cbpc, "%d\n",pBlockInfo->cbp_c);
#endif
   
}

//luma block scanning
//0  1   4  5
//2  3   6  7
// 
//8  9   12 13
//10 11  14 15

//Chroma block scanning u
//16 17
//18 19

//Chroma block scanning v
//20 21
//22 23
Int16 T264_mb_predict_non_zero_code(pBlockInfo_t pBlockInfo, int idx)
{	
	Int16 nC;
	
	switch(idx)
	{		
		case 0:				
			if(pBlockInfo->mbX == 0 && pBlockInfo->mbY == 0)//top-left boarder
				nC = 0;
			else if(pBlockInfo->mbX == 0)//left boarder
				nC = pBlockInfo->pPrevLineMem[pBlockInfo->sliceMbNumb].nnz[10];//nC=nB
			else if(pBlockInfo->mbY == 0)//top boarder
				nC = pBlockInfo->pCurrLineMem[pBlockInfo->sliceMbNumb-1].nnz[5];//nC=nA
			else
			{
				nC = (pBlockInfo->pCurrLineMem[pBlockInfo->sliceMbNumb-1].nnz[5] + //nA
					pBlockInfo->pPrevLineMem[pBlockInfo->sliceMbNumb].nnz[10] +	   //nB				
					1) >> 1;
			}
		break;
		case 1:						
			if(pBlockInfo->mbY == 0)//top boarder
				nC = pBlockInfo->pCurrLineMem[pBlockInfo->sliceMbNumb].nnz[0];//nC=nA
			else
			{
				nC = (pBlockInfo->pCurrLineMem[pBlockInfo->sliceMbNumb].nnz[0] + //nA
					pBlockInfo->pPrevLineMem[pBlockInfo->sliceMbNumb].nnz[11] +	 //nB				 
					1) >> 1;
			}
		break;
		case 2:						
			if(pBlockInfo->mbX == 0)//left boarder
				nC = pBlockInfo->pCurrLineMem[pBlockInfo->sliceMbNumb].nnz[0];//nC=nB
			else
			{
				nC = (pBlockInfo->pCurrLineMem[pBlockInfo->sliceMbNumb-1].nnz[7] + //nA
					pBlockInfo->pCurrLineMem[pBlockInfo->sliceMbNumb].nnz[0] +     //nB
					1) >> 1;
			}
		break;
		case 3:										
			nC = (pBlockInfo->pCurrLineMem[pBlockInfo->sliceMbNumb].nnz[2] + //nA
				pBlockInfo->pCurrLineMem[pBlockInfo->sliceMbNumb].nnz[1] +	 //nB			 
				1) >> 1;				
		break;
		case 4:						
			if(pBlockInfo->mbY == 0)//top boarder
				nC = pBlockInfo->pCurrLineMem[pBlockInfo->sliceMbNumb].nnz[1];//nC=nA
			else
			{
				nC = (pBlockInfo->pCurrLineMem[pBlockInfo->sliceMbNumb].nnz[1] + //nA
					pBlockInfo->pPrevLineMem[pBlockInfo->sliceMbNumb].nnz[14] +  //nB
					1) >> 1;
			}
		break;
		case 5:						
			if(pBlockInfo->mbY == 0)//top boarder
				nC = pBlockInfo->pCurrLineMem[pBlockInfo->sliceMbNumb].nnz[4];//nC=nA
			else
			{
				nC = (pBlockInfo->pCurrLineMem[pBlockInfo->sliceMbNumb].nnz[4] + //nA
					pBlockInfo->pPrevLineMem[pBlockInfo->sliceMbNumb].nnz[15] +  //nB
					1) >> 1;
			}
		break;
		case 6:										
			nC = (pBlockInfo->pCurrLineMem[pBlockInfo->sliceMbNumb].nnz[3] + //nA
				pBlockInfo->pCurrLineMem[pBlockInfo->sliceMbNumb].nnz[4] +	 //nB			 
				1) >> 1;				
		break;
		case 7:										
			nC = (pBlockInfo->pCurrLineMem[pBlockInfo->sliceMbNumb].nnz[6] + //nA
				pBlockInfo->pCurrLineMem[pBlockInfo->sliceMbNumb].nnz[5] +	 //nB			 
				1) >> 1;				
		break;
		case 8:						
			if(pBlockInfo->mbX == 0)//left boarder
				nC = pBlockInfo->pCurrLineMem[pBlockInfo->sliceMbNumb].nnz[2];//nC=nB
			else
			{
				nC = (pBlockInfo->pCurrLineMem[pBlockInfo->sliceMbNumb-1].nnz[13] + //nA
					pBlockInfo->pCurrLineMem[pBlockInfo->sliceMbNumb].nnz[2] +     //nB
					1) >> 1;
			}
		break;
		case 9:										
			nC = (pBlockInfo->pCurrLineMem[pBlockInfo->sliceMbNumb].nnz[8] + //nA
				pBlockInfo->pCurrLineMem[pBlockInfo->sliceMbNumb].nnz[3] +	 //nB			 
				1) >> 1;				
		break;
		case 10:						
			if(pBlockInfo->mbX == 0)//left boarder
				nC = pBlockInfo->pCurrLineMem[pBlockInfo->sliceMbNumb].nnz[8];//nC=nB
			else
			{
				nC = (pBlockInfo->pCurrLineMem[pBlockInfo->sliceMbNumb-1].nnz[15] + //nA
					pBlockInfo->pCurrLineMem[pBlockInfo->sliceMbNumb].nnz[8] +     //nB
					1) >> 1;
			}
		break;
		case 11:										
			nC = (pBlockInfo->pCurrLineMem[pBlockInfo->sliceMbNumb].nnz[10] + //nA
				pBlockInfo->pCurrLineMem[pBlockInfo->sliceMbNumb].nnz[9] +	 //nB			 
				1) >> 1;				
		break;
		case 12:										
			nC = (pBlockInfo->pCurrLineMem[pBlockInfo->sliceMbNumb].nnz[9] + //nA
				pBlockInfo->pCurrLineMem[pBlockInfo->sliceMbNumb].nnz[6] +	 //nB			 
				1) >> 1;				
		break;
		case 13:										
			nC = (pBlockInfo->pCurrLineMem[pBlockInfo->sliceMbNumb].nnz[12] + //nA
				pBlockInfo->pCurrLineMem[pBlockInfo->sliceMbNumb].nnz[7] +	 //nB			 
				1) >> 1;				
		break;
		case 14:										
			nC = (pBlockInfo->pCurrLineMem[pBlockInfo->sliceMbNumb].nnz[11] + //nA
				pBlockInfo->pCurrLineMem[pBlockInfo->sliceMbNumb].nnz[12] +	 //nB			 
				1) >> 1;				
		break;
		case 15:										
			nC = (pBlockInfo->pCurrLineMem[pBlockInfo->sliceMbNumb].nnz[14] + //nA
				pBlockInfo->pCurrLineMem[pBlockInfo->sliceMbNumb].nnz[13] +	 //nB			 
				1) >> 1;				
		break;
		//#################################### chroma ################################
		case 16:				
			if(pBlockInfo->mbX == 0 && pBlockInfo->mbY == 0)//top-left boarder
				nC = 0;
			else if(pBlockInfo->mbX == 0)//left boarder
				nC = pBlockInfo->pPrevLineMem[pBlockInfo->sliceMbNumb].nnz[18];//nC=nB
			else if(pBlockInfo->mbY == 0)//top boarder
				nC = pBlockInfo->pCurrLineMem[pBlockInfo->sliceMbNumb-1].nnz[17];//nC=nA
			else
			{
				nC = (pBlockInfo->pCurrLineMem[pBlockInfo->sliceMbNumb-1].nnz[17] + //nA
					pBlockInfo->pPrevLineMem[pBlockInfo->sliceMbNumb].nnz[18] +	    //nB				
					1) >> 1;
			}
		break;
		case 17:				
			if(pBlockInfo->mbY == 0)//top boarder
				nC = pBlockInfo->pCurrLineMem[pBlockInfo->sliceMbNumb].nnz[16];//nC=nA
			else
			{
				nC = (pBlockInfo->pCurrLineMem[pBlockInfo->sliceMbNumb].nnz[16] + //nA
					pBlockInfo->pPrevLineMem[pBlockInfo->sliceMbNumb].nnz[19] +	  //nB				
					1) >> 1;
			}
		break;
		case 18:				
			if(pBlockInfo->mbX == 0)//left boarder
				nC = pBlockInfo->pCurrLineMem[pBlockInfo->sliceMbNumb].nnz[16];//nC=nB			
			else
			{
				nC = (pBlockInfo->pCurrLineMem[pBlockInfo->sliceMbNumb-1].nnz[19] + //nA
					pBlockInfo->pCurrLineMem[pBlockInfo->sliceMbNumb].nnz[16] +	    //nB				
					1) >> 1;
			}
		break;
		case 19:										
			nC = (pBlockInfo->pCurrLineMem[pBlockInfo->sliceMbNumb].nnz[18] + //nA
				pBlockInfo->pCurrLineMem[pBlockInfo->sliceMbNumb].nnz[17] +	 //nB			 
				1) >> 1;				
		break;
		case 20:				
			if(pBlockInfo->mbX == 0 && pBlockInfo->mbY == 0)//top-left boarder
				nC = 0;
			else if(pBlockInfo->mbX == 0)//left boarder
				nC = pBlockInfo->pPrevLineMem[pBlockInfo->sliceMbNumb].nnz[22];//nC=nB
			else if(pBlockInfo->mbY == 0)//top boarder
				nC = pBlockInfo->pCurrLineMem[pBlockInfo->sliceMbNumb-1].nnz[21];//nC=nA
			else
			{
				nC = (pBlockInfo->pCurrLineMem[pBlockInfo->sliceMbNumb-1].nnz[21] + //nA
					pBlockInfo->pPrevLineMem[pBlockInfo->sliceMbNumb].nnz[22] +	    //nB				
					1) >> 1;
			}
		break;
		case 21:				
			if(pBlockInfo->mbY == 0)//top boarder
				nC = pBlockInfo->pCurrLineMem[pBlockInfo->sliceMbNumb].nnz[20];//nC=nA
			else
			{
				nC = (pBlockInfo->pCurrLineMem[pBlockInfo->sliceMbNumb].nnz[20] + //nA
					pBlockInfo->pPrevLineMem[pBlockInfo->sliceMbNumb].nnz[23] +	  //nB				
					1) >> 1;
			}
		break;
		case 22:				
			if(pBlockInfo->mbX == 0)//left boarder
				nC = pBlockInfo->pCurrLineMem[pBlockInfo->sliceMbNumb].nnz[20];//nC=nB			
			else
			{
				nC = (pBlockInfo->pCurrLineMem[pBlockInfo->sliceMbNumb-1].nnz[23] + //nA
					pBlockInfo->pCurrLineMem[pBlockInfo->sliceMbNumb].nnz[20] +	    //nB				
					1) >> 1;
			}
		break;
		case 23:										
			nC = (pBlockInfo->pCurrLineMem[pBlockInfo->sliceMbNumb].nnz[22] + //nA
				pBlockInfo->pCurrLineMem[pBlockInfo->sliceMbNumb].nnz[21] +	 //nB			 
				1) >> 1;				
		break;
	}
    
	//return pred_blk & 0x7f;
	return nC;
}

