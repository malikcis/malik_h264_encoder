/*****************************************************************************  
 *  
 *  H264 AVC CODEC  
 *  
 *  Copyright(C) 2020 llcc <malik.cisse@abateck.com>  
 *  
 *  This program is free software ; you can redistribute it and/or modify  
 *  it under the terms of the GNU General Public License as published by  
 *  the Free Software Foundation ; either version 2 of the License, or  
 *  (at your option) any later version.  
 *  
 *  This program is distributed in the hope that it will be useful,  
 *  but WITHOUT ANY WARRANTY ; without even the implied warranty of  
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the  
 *  GNU General Public License for more details.  
 *  
 *  You should have received a copy of the GNU General Public License  
 *  along with this program ; if not, write to the Free Software  
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA  
 *  
 ****************************************************************************/ 
#include "encode_frame.h"
#include "bsg.h"
#include "tc.h"
#include "mem_control.h"
#include "bitstream.h"

#ifndef MAX
#define  MAX(a,b)       (((a) > (b)) ? (a) : (b))
#endif

#ifndef MIN
#define  MIN(a,b)       (((a) < (b)) ? (a) : (b))
#endif

const Int32 chroma_qp[] =
{
    0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 
    11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 
    21, 22, 23, 24, 25, 26, 27, 28, 29,
    29, 30, 31, 32, 32, 33, 34, 34, 35, 35,
    36, 36, 37, 37, 37, 38, 38, 38, 39, 39, 39, 39
};

static UInt32 mbLine;

typedef struct _struct1_
{
	unsigned short profile_and_level    : 3;
    unsigned short usr_sr_epl           : 1; 
    unsigned short smooth               : 1;
    unsigned short raw_yuv_format       : 1; 
    unsigned short enc_yuv_format       : 1;  
    unsigned short skip_frame_thr		: 9; 
}struct1;


typedef struct _struct2_
{	
	unsigned short s_scaling_raw    : 2;       
    unsigned short zerro3           : 1;
    unsigned short s_scaling_comp   : 2;
    unsigned short large_char_osd   : 1; 
    unsigned short zerro2           : 2;  
    unsigned short vid_select       : 2;  
    unsigned short zerro1           : 4;
    unsigned short osd_enable       : 1; 
    unsigned short interlaced       : 1;
}struct2;


typedef struct _struct3_
{			   
	unsigned short framerate_nr  : 8;  
    unsigned short framerate_dr  : 4;
    unsigned short zerro         : 3;
    unsigned short framerate_ind : 1;      
}struct3;


void print_enc_config(pEncFrame_t pEncFrame)
{
	unsigned short img_hlength;                   
    unsigned short img_hstart;                   
    unsigned short img_vlength;                  
    unsigned short img_vstart;                   
    unsigned short s_act_hlength;                
    unsigned short act_hstart;                   
    unsigned short s_act_vlength;                
    unsigned short act_vstart;                   
    unsigned short dec_denominator;              
    unsigned short dec_numerator;                               
    unsigned short intra_sad_thr;                
    unsigned short gop_length;                                  
    unsigned short framerate_exp;                            
	
	union 
	{
        struct1	bits;
        unsigned short  ushort;
    } u_struct1;

	union 
	{
        struct2	bits;
        unsigned short  ushort;
    } u_struct2;

	union 
	{
        struct3	bits;
        unsigned short  ushort;
    } u_struct3;

	//init
	u_struct1.ushort = u_struct2.ushort = u_struct3.ushort = 0;

	//0
	img_hlength          = (unsigned short)pEncFrame->pInputData->frame_width;         
	
	//1
	img_hstart           = 0;                
	
	//2
	img_vlength          = (unsigned short)pEncFrame->pInputData->frame_height;   

	//3
	img_vstart           = 0;  

	//4
	s_act_hlength        = (unsigned short)pEncFrame->pInputData->frame_width;     

	//5
	act_hstart           = 0;        

	//6
	s_act_vlength        = (unsigned short)pEncFrame->pInputData->frame_height;      

	//7
	act_vstart           = 0;    

	//8
	dec_denominator      = 1;   

	//9
	dec_numerator        = 1;        
	
	//10
	u_struct1.bits.profile_and_level    = pEncFrame->pInputData->profile_and_level;        
	u_struct1.bits.usr_sr_epl           = pEncFrame->pInputData->use_sl_epl;
	u_struct1.bits.smooth               = 0;       
	u_struct1.bits.raw_yuv_format       = 0;        
	u_struct1.bits.enc_yuv_format       = 0;        
	u_struct1.bits.skip_frame_thr       = SKIP_FR_THR;       
	
	//11
	intra_sad_thr						 = INTRA_SAD_THR;       

	//12
	gop_length							 = pEncFrame->pInputData->gop_length;         
	
	//13
	u_struct2.bits.s_scaling_raw        = 0;        
	u_struct2.bits.zerro3               = 0;        
	u_struct2.bits.s_scaling_comp       = 0;        
	u_struct2.bits.large_char_osd       = 0;    
	u_struct2.bits.zerro2               = 0;
	u_struct2.bits.vid_select           = 1;   
	u_struct2.bits.zerro1               = 0;
	u_struct2.bits.osd_enable           = 0;        
	u_struct2.bits.interlaced           = 0;        
	
	//14
	framerate_exp						= pEncFrame->pInputData->framerate_exp;    

	//15
	u_struct3.bits.framerate_nr			= pEncFrame->pInputData->framerate_nr;
	u_struct3.bits.framerate_dr			= pEncFrame->pInputData->framerate_dr;
	u_struct3.bits.zerro				= 0;
	u_struct3.bits.framerate_ind		= pEncFrame->pInputData->framerate_ind;

}

void encode_frame(pEncFrame_t pEncFrame)
{
	UInt32 row;
	UInt32 col;
	UInt32 height;
	UInt32 width;
	blockInfo_t blockInfo;
	int is_skip=0;

	get_mem3D(&blockInfo.block8x8Buff.ppp_currentData, 6, 8, 8);
	height                       = pEncFrame->pInputData->frame_height;
	width                        = pEncFrame->pInputData->frame_width;
	blockInfo.mquantY            = pEncFrame->PQIndex;
	blockInfo.mquantC            = chroma_qp[blockInfo.mquantY];
	blockInfo.isNonUniformQ      = pEncFrame->isNonUniformQ;
	blockInfo.acPredFlag         = 0; 
	blockInfo.dcDiffTab          = pEncFrame->dcDiffTab;
	blockInfo.acTab   = pEncFrame->acTab;
	blockInfo.levelCsizeTab      = pEncFrame->levelCsizeTab;
	blockInfo.esc3_control       = 0;//init once /frame
	blockInfo.level_shift        = 1;
	blockInfo.default_dc_pred    = 0;
	blockInfo.max_bits_per_mb    = MAX_BITS_COEFF_MB;
	blockInfo.last_mb            = FALSE;
	blockInfo.frameType          = pEncFrame->frameType;
	blockInfo.frame_mb_width     = width>>4;
	blockInfo.frame_mb_height    = height>>4;
	blockInfo.mvDiffTab          = pEncFrame->mvDiffTab;
	blockInfo.isQuarterSample    = pEncFrame->pInputData->isQuarterSample;
	blockInfo.cbpcyTab           = pEncFrame->cbpcyTab;
	blockInfo.mvRange            = pEncFrame->mvRange;
	blockInfo.roundCtrl          = pEncFrame->roundCtrl;
	blockInfo.isFastUV           = pEncFrame->pInputData->isFastUV;
	blockInfo.pMvMemory          = pEncFrame->pMvMemory;
	blockInfo.frameCntr          = pEncFrame->frameCntr;

	blockInfo.skip_run = 0;

	//debug only
	if(pEncFrame->isFirstFrame)
		print_enc_config(pEncFrame);

	//loop over all Macroblocks in a frame
	for(row = 0; row < height; row+=16)
	{
		for(col = 0; col < width; col+=16) 
		{
			//set blockInfo
			blockInfo.mbX = col>>4;
			blockInfo.mbY = row>>4;

			if(row == height-16 && col == width-16)//last MB in frame
			{
				blockInfo.last_mb = TRUE;
			}

			blockInfo.pEncFrame = pEncFrame;

			//init
			blockInfo.mbNumb = blockInfo.mbX + (blockInfo.mbY * (pEncFrame->pInputData->frame_width>>4));
			blockInfo.sliceMbNumb = blockInfo.mbX;
			if(pEncFrame->frameType == I)
			{
				pEncFrame->pMvMemory[blockInfo.mbNumb].mv.mbType = INTRA;//overwrite MB type from ME
			}
			blockInfo.mbType = pEncFrame->pMvMemory[blockInfo.mbNumb].mv.mbType;//get type from ME into local struct

			if(blockInfo.mbX == 0 && blockInfo.mbY == 0)
			{
				mbLine = 0;//init
			}
			else if(blockInfo.mbX == 0)
			{
				mbLine++;
			}
			else
			{
				mbLine = mbLine;//do noting
			}

			if((blockInfo.mbX == 0) && (mbLine%2 == 0))//at the start of each even MB line
			{
				blockInfo.pCurrLineMem = pEncFrame->pLineMemory1;
				blockInfo.pPrevLineMem = pEncFrame->pLineMemory2;
			}
			if((blockInfo.mbX == 0) && (mbLine%2 == 1))//at the start of each even MB line
			{
				blockInfo.pCurrLineMem = pEncFrame->pLineMemory2;
				blockInfo.pPrevLineMem = pEncFrame->pLineMemory1;
			}
			
			//get current MB
			mbMemRead(width, height, row,  col, pEncFrame->pInFrameData, blockInfo.block8x8Buff.ppp_currentData);

			mb_texture_codec(&blockInfo);//mb_encoding
			
			writeCurrMemForIntraPred(&blockInfo);

			//compute cbp	
			h264_cbpcy_calc(&blockInfo);

			writeCurrMemForNnz(&blockInfo);

			if(pEncFrame->pMvMemory[blockInfo.mbNumb].skip && blockInfo.frameType == P)		
			{
				blockInfo.skip_run++;
				blockInfo.skip = 1;
			}
			else
			{
				blockInfo.skip = 0;
			}

			T264_macroblock_write_cavlc(&blockInfo);//bitstream_generation	
			

			mbWriteRefToMem(&blockInfo, pEncFrame);
		}
	}//loop over Macroblocks
	free_mem3D(blockInfo.block8x8Buff.ppp_currentData, 6);
}
