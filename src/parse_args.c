/*****************************************************************************  
 *  
 *  H264 AVC CODEC  
 *  
 *  Copyright(C) 2020 llcc <malik.cisse@abateck.com>  
 *  
 *  This program is free software ; you can redistribute it and/or modify  
 *  it under the terms of the GNU General Public License as published by  
 *  the Free Software Foundation ; either version 2 of the License, or  
 *  (at your option) any later version.  
 *  
 *  This program is distributed in the hope that it will be useful,  
 *  but WITHOUT ANY WARRANTY ; without even the implied warranty of  
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the  
 *  GNU General Public License for more details.  
 *  
 *  You should have received a copy of the GNU General Public License  
 *  along with this program ; if not, write to the Free Software  
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA  
 *  
 ****************************************************************************/ 
#include "parse_args.h"
#include "global.h"
#include "ctype.h"

void parseArgs(pInputInterface_t pcmd, Int argc, Char ** argv) 
{
	Int32 print_help = 0;
    Int32 arg_idx;
    Int i;
    FILE  *ip;
    Char cmd_line [256];
    Int j, arg_no = 1;
	
	if (!READ_FROM_STD_IN && (ip = fopen("options.cfg", "r")) != NULL)//open config file
    {
        while (fgets(cmd_line, sizeof(cmd_line), ip) != NULL)
        {
            if (arg_no >= NO_ARGS)
            {
               fprintf(stdout, "Too many arguments\n");
               exit (1);
            }

            for (i = 0; i < (Int) strlen(cmd_line); i++)
            {
                // Skip Trailing Blanks
                
                if (isspace(cmd_line[i]))
                {
                    for (; isspace(cmd_line[i]); i++)
                        ;
                }

                if (i + 1 < (Int) strlen(cmd_line) && cmd_line[i] == '/' && cmd_line[i+1] == '/')
                {
                    break;
                }
                else if (cmd_line[i] && ! isspace(cmd_line[i]))
                {
                    // Get Argument
                
                    if (cmd_line[i] != '\"')
                    {
                        myargv[arg_no][0] = cmd_line[i];

                        for (j = 1, i++; ! isspace(cmd_line[i]); i++, j++)
                            myargv[arg_no][j] = cmd_line[i];

                        myargv[arg_no][j] = 0;
                        arg_no++;
                    }
                    else
                    {
                        for (j = 0, i++; cmd_line[i] != '\"'; i++, j++)
                            myargv[arg_no][j] = cmd_line[i];

                        myargv[arg_no][j] = 0;
                        arg_no++;
                        i++;
                    }
                }
            }
        }
        argc = arg_no;
        fclose(ip);
    }
    else//no input file could be opened. Try to read from stdin
    {
        if (argc >= NO_ARGS)
        {
           fprintf(stdout, "Too many arguments\n");
           exit (1);
        }
        for (i = 0; i < argc; i++)
            strcpy (myargv[i], argv[i]);
    }
	
	// Set command line default parms
	print_help					        = 0;
    if (argc < 2) print_help            = 1; //no options selected

	//print_help					    = 0;
	pcmd->usePsnr                       = 0;
	pcmd->use_bstuffing                 = 0;
	pcmd->use_sl_epl					= 0;
	pcmd->gop_length					= 255;
	pcmd->framerate_ind					= 0;
	pcmd->framerate_nr					= 3;
	pcmd->framerate_dr					= 1;
	pcmd->framerate_exp					= 959;
	pcmd->frame_height		            = 144;
	pcmd->frame_width	    	        = 176;
	pcmd->numbFrames                    = 1;
	pcmd->profile_and_level             = 0;
	pcmd->frame_rate			        = 30;
	pcmd->brcIFrOffset					= 0;
	pcmd->PQIndexMin					= 1;
	pcmd->PQIndexMax					= 31;
	pcmd->PQIndexConst		            = 4;
    pcmd->search_algorithm              = 0;
	pcmd->isFastUV                      = 1;
	pcmd->isQuarterSample               = 0;	
	pcmd->interH264Data.dcDiffTab        = 1;
	pcmd->interH264Data.acTab            = 2;
	pcmd->interH264Data.mvDiffTab        = 0;
	pcmd->interH264Data.cbpcyTab         = 1;
	pcmd->interH264Data.isNonUniformQ    = 0;
    pcmd->multiRes                      = 0;
    pcmd->isImplicitQ                   = 0;
    pcmd->interH264Data.isBIFrame        = 0;
    pcmd->isDeblockFilterOn             = 0;
	pcmd->intraH264Data.dcDiffTab        = 1;
	pcmd->intraH264Data.acTab            = 2;
	pcmd->intraH264Data.mvDiffTab        = 0;
	pcmd->intraH264Data.cbpcyTab         = 0;//not needed
	pcmd->intraH264Data.levelCsizeTab    = 0;
	pcmd->intraH264Data.isNonUniformQ    = 0;
    pcmd->intraH264Data.isBIFrame        = 0;
	pcmd->disableFilter                 = 1;
	strcpy(pcmd->in_file_name,    "default.yuv");
	strcpy(pcmd->test_name,       "test_0");

    for (arg_idx=1; arg_idx < argc; arg_idx++ ) 
	{
        
		if (strcmp(myargv[arg_idx],"-h") == 0) {
			// help option
			print_help = 1;
			}
		else if ((strcmp(myargv[arg_idx],"-height") == 0) && (arg_idx + 1 < argc)) {
			pcmd->frame_height=(UInt32)atoi(myargv[arg_idx+1]);
			arg_idx++;
			}
		else if ((strcmp(myargv[arg_idx],"-width") == 0) && (arg_idx + 1 < argc)) {
			pcmd->frame_width=(UInt32)atoi(myargv[arg_idx+1]);
			arg_idx++;
			}
		else if ((strcmp(myargv[arg_idx],"-if") == 0) && (arg_idx + 1 < argc)) {
			//input file name
			strcpy(pcmd->in_file_name,myargv[arg_idx+1]);
			arg_idx++;
			}
		else if ((strcmp(myargv[arg_idx],"-numbFrames") == 0) && (arg_idx + 1 < argc)) {
			//number of frames to be processes
			pcmd->numbFrames = (UInt32)atoi(myargv[arg_idx+1]);
			arg_idx++;
			}
		else if  ((strcmp(myargv[arg_idx],"-fr") == 0)  && (arg_idx + 1 < argc)) {
		    pcmd->frame_rate = (UInt8)atoi(myargv[arg_idx+1]);
			arg_idx++;
			}
		else if ((strcmp(myargv[arg_idx],"-q4") == 0) && (arg_idx + 1 < argc)) {
			pcmd->isQuarterSample = (UInt8)atoi(myargv[arg_idx+1]);
			arg_idx++;
			}
		else if ((strcmp(myargv[arg_idx],"-isFastUV") == 0) && (arg_idx + 1 < argc)) {
			pcmd->isFastUV = (UInt8)atoi(myargv[arg_idx+1]);
			arg_idx++;
			}		
		else if ((strcmp(myargv[arg_idx],"-pl") == 0) && (arg_idx + 1 < argc)) {
			pcmd->profile_and_level = (UInt8)atoi(myargv[arg_idx+1]);
			arg_idx++;
		}
		else if ((strcmp(myargv[arg_idx],"-use_sl_epl") == 0) && (arg_idx + 1 < argc)) {
			pcmd->use_sl_epl = (UInt8)atoi(myargv[arg_idx+1]);
			arg_idx++;
		}
		else if ((strcmp(myargv[arg_idx],"-gop_length") == 0) && (arg_idx + 1 < argc)) {
			pcmd->gop_length = (UInt8)atoi(myargv[arg_idx+1]);
			arg_idx++;
		}
		else if ((strcmp(myargv[arg_idx],"-fr_ind") == 0) && (arg_idx + 1 < argc)) {
			pcmd->framerate_ind = (UInt8)atoi(myargv[arg_idx+1]);
			arg_idx++;
		}
		else if ((strcmp(myargv[arg_idx],"-fr_nr") == 0) && (arg_idx + 1 < argc)) {
			pcmd->framerate_nr = (UInt8)atoi(myargv[arg_idx+1]);
			arg_idx++;
		}
		else if ((strcmp(myargv[arg_idx],"-fr_dr") == 0) && (arg_idx + 1 < argc)) {
			pcmd->framerate_dr = (UInt8)atoi(myargv[arg_idx+1]);
			arg_idx++;
		}
		else if ((strcmp(myargv[arg_idx],"-fr_exp") == 0) && (arg_idx + 1 < argc)) {
			pcmd->framerate_exp = (UInt16)atoi(myargv[arg_idx+1]);
			arg_idx++;
		}			
		else if ((strcmp(myargv[arg_idx],"-brcIFrOffset") == 0)  && (arg_idx + 1 < argc)) {
			pcmd->brcIFrOffset      = (UInt8)atoi(myargv[arg_idx+1]);
			arg_idx++;
			}
		else if ((strcmp(myargv[arg_idx],"-PQIndexMin") == 0)  && (arg_idx + 1 < argc)) {
			pcmd->PQIndexMin      = (UInt8)atoi(myargv[arg_idx+1]);
			arg_idx++;
			}
		else if ((strcmp(myargv[arg_idx],"-PQIndexMax") == 0)  && (arg_idx + 1 < argc)) {
			pcmd->PQIndexMax      = (UInt8)atoi(myargv[arg_idx+1]);
			arg_idx++;
			}
		else if ((strcmp(myargv[arg_idx],"-implQ") == 0)  && (arg_idx + 1 < argc)) {
			pcmd->isImplicitQ      = (UInt8)atoi(myargv[arg_idx+1]);
			arg_idx++;
			}
		else if ((strcmp(myargv[arg_idx],"-sear") == 0)  && (arg_idx + 1 < argc)) {
			pcmd->search_algorithm      = (UInt8)atoi(myargv[arg_idx+1]);
			arg_idx++;
			}
		else if ((strcmp(myargv[arg_idx],"-name") == 0)  && (arg_idx + 1 < argc)) {
			strcpy(pcmd->test_name, myargv[arg_idx+1]);
			arg_idx++;
			}
		else if ((strcmp(myargv[arg_idx],"-pqIndex") == 0)  && (arg_idx + 1 < argc)) {
			pcmd->PQIndexConst = (UInt8)atoi(myargv[arg_idx+1]);
			arg_idx++;
			}
		else if ((strcmp(myargv[arg_idx],"-dcDiffTab") == 0)  && (arg_idx + 1 < argc)) {
			pcmd->interH264Data.dcDiffTab = (UInt8)atoi(myargv[arg_idx+1]);
			arg_idx++;
			}
		else if ((strcmp(myargv[arg_idx],"-acTab") == 0)  && (arg_idx + 1 < argc)) {
			pcmd->interH264Data.acTab = (UInt8)atoi(myargv[arg_idx+1]);
			arg_idx++;
			}
		else if ((strcmp(myargv[arg_idx],"-mvDiffTab") == 0)  && (arg_idx + 1 < argc)) {
			pcmd->interH264Data.mvDiffTab = (UInt8)atoi(myargv[arg_idx+1]);
			arg_idx++;
			}
		else if ((strcmp(myargv[arg_idx],"-cbpcyTab") == 0)  && (arg_idx + 1 < argc)) {
			pcmd->interH264Data.cbpcyTab = (UInt8)atoi(myargv[arg_idx+1]);
			arg_idx++;
			}
		else if ((strcmp(myargv[arg_idx],"-isNonUniformQ") == 0)  && (arg_idx + 1 < argc)) {
			pcmd->interH264Data.isNonUniformQ = (UInt8)atoi(myargv[arg_idx+1]);
			arg_idx++;
			}
		else if ((strcmp(myargv[arg_idx],"-multiRes") == 0)  && (arg_idx + 1 < argc)) {
			pcmd->multiRes = (UInt8)atoi(myargv[arg_idx+1]);
			arg_idx++;
			}
   		else if ((strcmp(myargv[arg_idx],"-loopFilt") == 0)  && (arg_idx + 1 < argc)) {
			pcmd->isDeblockFilterOn = (UInt8)atoi(myargv[arg_idx+1]);
			arg_idx++;                        
			}		
		else if ((strcmp(myargv[arg_idx],"-idcDiffTab") == 0)  && (arg_idx + 1 < argc)) {
			pcmd->intraH264Data.dcDiffTab = (UInt8)atoi(myargv[arg_idx+1]);
			arg_idx++;
			}
		else if ((strcmp(myargv[arg_idx],"-stuffing") == 0)  && (arg_idx + 1 < argc)) {
			pcmd->use_bstuffing = (UInt8)atoi(myargv[arg_idx+1]);
			arg_idx++;
			}
		else if ((strcmp(myargv[arg_idx],"-iacTab") == 0)  && (arg_idx + 1 < argc)) {
			pcmd->intraH264Data.acTab = (UInt8)atoi(myargv[arg_idx+1]);
			arg_idx++;
		}
		else if ((strcmp(myargv[arg_idx],"-imvDiffTab") == 0)  && (arg_idx + 1 < argc)) {
			pcmd->intraH264Data.mvDiffTab = (UInt8)atoi(myargv[arg_idx+1]);
			arg_idx++;
			}
		else if ((strcmp(myargv[arg_idx],"-icbpcyTab") == 0)  && (arg_idx + 1 < argc)) {
			pcmd->intraH264Data.cbpcyTab = (UInt8)atoi(myargv[arg_idx+1]);
			arg_idx++;
			}
		else if ((strcmp(myargv[arg_idx],"-iisNonUniformQ") == 0)  && (arg_idx + 1 < argc)) {
			pcmd->intraH264Data.isNonUniformQ = (UInt8)atoi(myargv[arg_idx+1]);
			arg_idx++;
			}  
		else if ((strcmp(myargv[arg_idx],"-highProfile") == 0)  && (arg_idx + 1 < argc)) {
			pcmd->useHighProfile = (UInt8)atoi(myargv[arg_idx+1]);
			arg_idx++;
			}  
		else if ((strcmp(myargv[arg_idx],"-disableFilter") == 0)  && (arg_idx + 1 < argc)) {
			pcmd->disableFilter = (UInt8)atoi(myargv[arg_idx+1]);
			arg_idx++;
			}  
		else if ((strcmp(myargv[arg_idx],"-usePsnr") == 0)  && (arg_idx + 1 < argc)) {
			pcmd->usePsnr = (UInt8)atoi(myargv[arg_idx+1]);
			arg_idx++;
			}  
		else if ((strcmp(myargv[arg_idx],"-SkipMbYOffset") == 0)  && (arg_idx + 1 < argc)) {
			pcmd->SkipMbYOffset = (UInt8)atoi(myargv[arg_idx+1]);
			arg_idx++;
		} 
		else if ((strcmp(myargv[arg_idx],"-SkipMbYSlope") == 0)  && (arg_idx + 1 < argc)) {
			pcmd->SkipMbYSlope = (UInt8)atoi(myargv[arg_idx+1]);
			arg_idx++;
			}  
		else if ((strcmp(myargv[arg_idx],"-SkipMbCOffset") == 0)  && (arg_idx + 1 < argc)) {
			pcmd->SkipMbCOffset = (UInt8)atoi(myargv[arg_idx+1]);
			arg_idx++;
			}  
		else if ((strcmp(myargv[arg_idx],"-SkipMbCSlope") == 0)  && (arg_idx + 1 < argc)) {
			pcmd->SkipMbCSlope = (UInt8)atoi(myargv[arg_idx+1]);
			arg_idx++;
			}  
		else {
			// unsupported option
            fprintf (stdout, "Argument %s not recognized\n", myargv[arg_idx]);
			print_help = 1;
			}
		}

    if (print_help == 1) 
	{
		fprintf (stdout, "\nReproduction in whole or in part is prohibited\n");
		fprintf (stdout, "without the written permission of the copyright owner.\n\n");
		fprintf (stdout, "Contact: malik.cisse@abateck.com\n");

		fprintf (stdout, "Usage: %s [options]\n",myargv[0]);
		fprintf (stdout, "Options: \n");
		fprintf (stdout, "-h                 Print this message\n");
		fprintf (stdout, "-if                Input filename with full path {%s}\n",pcmd->in_file_name);
		fprintf (stdout, "-name              Test Name \n");
		fprintf (stdout, "-numbFrames        number of frames to be processed\n");
		fprintf (stdout, "-pl                Profile and level {%d}\n", pcmd->profile_and_level);
		fprintf (stdout, "                   0       : Advanced Profile Level0\n");
		fprintf (stdout, "                   1       : Advanced Profile Level1\n");
		fprintf (stdout, "                   2       : Advanced Profile Level2\n");
		fprintf (stdout, "                   3       : Advanced Profile Level3\n");
		fprintf (stdout, "                   4       : Advanced Profile Level4\n");
		fprintf (stdout, "-isFastUV          Multi resolution {%d}\n",pcmd->isFastUV);		
		fprintf (stdout, "                   0       : [64x32]\n");
		fprintf (stdout, "                   1       : [128x64]\n");
		fprintf (stdout, "                   2       : [512x128]\n");
		fprintf (stdout, "                   3       : [1024x256]\n");

		fprintf (stdout, "-pqIndex     PQIndex for constant Q mode & P-Frames {%d}\n",pcmd->PQIndexConst);
		fprintf (stdout, "-dcDiffTab         DC differential VLC table {%d}\n",pcmd->interH264Data.dcDiffTab);
		fprintf (stdout, "-acTab			 VLC table {%d}. 0: LM, 1: HM, 2: MR, 3: HR\n",pcmd->interH264Data.acTab);
		fprintf (stdout, "-mvDiffTab         Motion vector differential VLC table {%d}\n",pcmd->interH264Data.mvDiffTab);
		fprintf (stdout, "-cbpcyTab          VLC table {%d}\n",pcmd->interH264Data.cbpcyTab);
		fprintf (stdout, "-multiRes          Multi resolution {%d}\n",pcmd->multiRes);

		fprintf (stdout, "-isNonUniformQ     VLC table {%d}\n",pcmd->interH264Data.isNonUniformQ);
		fprintf (stdout, "-idcDiffTab        VLC table {%d}\n",pcmd->intraH264Data.dcDiffTab);
		fprintf (stdout, "-iacTab            VLC table {%d}. 0: LM, 1: HM, 2: MR, 3: HR\n",pcmd->intraH264Data.acTab);

		fprintf (stdout, "-imvDiffTab        VLC table {%d}\n",pcmd->intraH264Data.mvDiffTab);
		fprintf (stdout, "-icbpcyTab         VLC table {%d}\n",pcmd->intraH264Data.cbpcyTab);
		fprintf (stdout, "-iisNonUniformQ    VLC table {%d}\n",pcmd->interH264Data.isNonUniformQ);

		fprintf (stdout, "\n\n");
		fprintf (stdout, "Note: Numbers in brackets {} are default  or curretnly set values used by this model.");
		fprintf (stdout, "\n\n");
		exit(0); //stop the program
    }

	//print user settings to screen
	fprintf (stdout, "\nInput file \t\t : %s\n",pcmd->in_file_name);
	fprintf (stdout, "Test Name \t\t : %s\n",pcmd->test_name);
	fprintf (stdout, "no of Frames \t\t : %d\n",pcmd->numbFrames);
	fprintf (stdout, "Profile&level \t\t : %s\n",
		pcmd->profile_and_level == 0 ? "Advanced Profile Level0"  :
		pcmd->profile_and_level == 1 ? "Advanced Profile Level1" :
		pcmd->profile_and_level == 2 ? "Advanced Profile Level2"    :
		pcmd->profile_and_level == 3 ? "Advanced Profile Level3"   :
		pcmd->profile_and_level == 4 ? "Advanced Profile Level4"   : "????");        
	fprintf (stdout, "pqIndex \t\t : %d\n",pcmd->PQIndexConst);
	fprintf (stdout, "\n\n");
} // parseArgs
