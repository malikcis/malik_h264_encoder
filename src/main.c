/*****************************************************************************  
 *  
 *  H264 AVC CODEC  
 *  
 *  Copyright(C) 2020 llcc <malik.cisse@abateck.com>  
 *  
 *  This program is free software ; you can redistribute it and/or modify  
 *  it under the terms of the GNU General Public License as published by  
 *  the Free Software Foundation ; either version 2 of the License, or  
 *  (at your option) any later version.  
 *  
 *  This program is distributed in the hope that it will be useful,  
 *  but WITHOUT ANY WARRANTY ; without even the implied warranty of  
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the  
 *  GNU General Public License for more details.  
 *  
 *  You should have received a copy of the GNU General Public License  
 *  along with this program ; if not, write to the Free Software  
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA  
 *  
 ****************************************************************************/ 

#include "global.h"
#include "bitstream.h"
#include "encode_frame.h"
#include "write_headers.h"
#include "parse_args.h"
#include "rate_control.h"
#include "encoder_manager.h"
#include "time.h"
#include "me.h"
#include <math.h>//log sqrt

#define DBG_H264_CFG 0
#define WRITE_RECONSTRUCTED 0

int main(int argc, char *argv[]) 
{
	UInt32 frameLength;//width*height*1.5
	UInt32 yLength;//width*height
	UInt32 uvLength;//width*height/4
	double y_psnr_avg, u_psnr_avg, v_psnr_avg;
	UInt32 numbOfSliceMBs;
	FILE *fileInput;
	FILE *fileStream;
	Char h264_file[256];
#if WRITE_RECONSTRUCTED
	FILE *fileReconstr;
	Char reconstructed_file[256];
#endif
	encFrame_t encFrame;
	inputInterface_t cmd;
	UInt32 accumulatedBytes=0;
	UInt32 total_bytes;
	time_t startTime, endTime;

	startTime = clock();
	set_scep(0);//init
#if !GLOBAL_DEBUG
	printf("encoder started.......\n");
#endif
	//parse cmd line or input file
	parseArgs(&cmd, argc, argv) ;

	//process input sequence
	if ((fileInput = fopen(cmd.in_file_name,"rb"))  == NULL) perror (cmd.in_file_name);

#if WRITE_MV_FILE
	if ((fileMVectors_w = fopen("mv_w.txt","wb"))  == NULL) perror ("mv_w.txt");
	encFrame.fileMVectors_w = fileMVectors_w;
#endif

	sprintf (h264_file, "%s.264", cmd.test_name);//h.264 compressed output file
	if ((fileStream = fopen(h264_file,"wb"))  == NULL) perror (h264_file);
#if WRITE_RECONSTRUCTED
	sprintf (reconstructed_file, "%s.yuv", cmd.test_name);
	if ((fileReconstr = fopen(reconstructed_file,"wb"))  == NULL) perror (reconstructed_file);
#endif
#if 0//BSG_DEBUG
	if ((fileDebug = fopen("bsg_debug.txt","wb"))  == NULL) perror ("bsg_debug.txt");
#endif

#if DBG_H264_CFG
	if ((encFrame.dbg_files.level_vlc     = fopen("test/level_vlc.txt","wb"))       == NULL) perror ("level_vlc.txt");
	if ((encFrame.dbg_files.level_len     = fopen("test/level_len.txt","wb"))       == NULL) perror ("level_len.txt");
	if ((encFrame.dbg_files.run_before    = fopen("test/run_before.txt","wb"))      == NULL) perror ("run_before.txt");
	if ((encFrame.dbg_files.run_before_len = fopen("test/run_before_len.txt","wb"))      == NULL) perror ("run_before_len.txt");
	if ((encFrame.dbg_files.tzeros        = fopen("test/total_zeros.txt","wb"))     == NULL) perror ("total_zeros.txt");
	if ((encFrame.dbg_files.t1s           = fopen("test/t1s.txt","wb"))             == NULL) perror ("t1s.txt");
	if ((encFrame.dbg_files.ctoken        = fopen("test/ctoken.txt","wb"))          == NULL) perror ("ctoken.txt");
	if ((encFrame.dbg_files.ctoken_len    = fopen("test/ctoken_len.txt","wb"))      == NULL) perror ("ctoken_len.txt");
	if ((encFrame.dbg_files.bsg_data      = fopen("test/bsg_data.txt","wb"))        == NULL) perror ("bsg_data.txt");
	if ((encFrame.dbg_files.residual      = fopen("test/residual.txt","wb"))        == NULL) perror ("residual.txt");	
	if ((encFrame.dbg_files.tc_din        = fopen("test/tc_din.txt","wb"))        == NULL) perror ("tc_din.txt");	
	if ((encFrame.dbg_files.reconstructed = fopen("test/reconstructed.txt","wb"))   == NULL) perror ("reconstructed.txt");
	if ((encFrame.dbg_files.cbpcy         = fopen("test/cbpcy.txt","wb"))           == NULL) perror ("cbpcy.txt");
	if ((encFrame.dbg_files.h264_cfg      = fopen("test/h264_cfg.txt","wb"))        == NULL) perror ("h264_cfg.txt");
	if ((encFrame.dbg_files.quant2x2dc    = fopen("test/quant2x2dc.txt","wb"))      == NULL) perror ("quant2x2dc.txt");
	if ((encFrame.dbg_files.quant4x4dc    = fopen("test/quant4x4dc.txt","wb"))      == NULL) perror ("quant4x4dc.txt");
	if ((encFrame.dbg_files.quant4x4ac    = fopen("test/quant4x4ac.txt","wb"))      == NULL) perror ("quant4x4ac.txt");
	if ((encFrame.dbg_files.h264_cfg_qp   = fopen("test/h264_cfg_qp.txt","wb"))      == NULL) perror ("h264_cfg_qp.txt");
	if ((encFrame.dbg_files.h264_cfg_fr_type = fopen("test/h264_cfg_fr_type.txt","wb"))      == NULL) perror ("h264_cfg_fr_type.txt");
	if ((encFrame.dbg_files.hp_mvx           = fopen("test/hp_mvx.txt","wb"))                == NULL) perror ("hp_mvx.txt");
	if ((encFrame.dbg_files.hp_mvy           = fopen("test/hp_mvy.txt","wb"))                == NULL) perror ("hp_mvy.txt");

	if ((encFrame.dbg_files.res                       = fopen("test/res.txt","wb"))  == NULL) perror ("res.txt");	
	if ((encFrame.dbg_files.res_a                     = fopen("test/res_a.txt","wb"))  == NULL) perror ("res_a.txt");
	if ((encFrame.dbg_files.res_b                     = fopen("test/res_b.txt","wb"))  == NULL) perror ("res_b.txt");
	if ((encFrame.dbg_files.hp_mv_xy                  = fopen("test/hp_mv.txt","wb"))  == NULL) perror ("hp_mv.txt");		
	if ((encFrame.dbg_files.qp_mv_xy                  = fopen("test/qp_mv.txt","wb"))  == NULL) perror ("qp_mv.txt");		
	if ((encFrame.dbg_files.fp_mv_xy                  = fopen("test/fp_mv.txt","wb"))  == NULL) perror ("fp_mv.txt");	
	if ((encFrame.dbg_files.writeback                 = fopen("test/writeback.txt","wb"))  == NULL) perror ("writeback.txt");	
	if ((encFrame.dbg_files.writeback_hex             = fopen("test/writeback_hex.txt","wb"))  == NULL) perror ("writeback_hex.txt");
	if ((encFrame.dbg_files.writeback_ab              = fopen("test/writeback_ab.txt","wb"))  == NULL) perror ("writeback_ab.txt");
	if ((encFrame.dbg_files.mv_pred                   = fopen("test/mv_pred.txt","wb"))  == NULL) perror ("mv_pred.txt");
	if ((encFrame.dbg_files.mb_act                    = fopen("test/mb_act.txt","wb"))  == NULL) perror ("mb_act.txt");
	if ((encFrame.dbg_files.mb_cand                   = fopen("test/mb_cand.txt","wb"))  == NULL) perror ("mb_cand.txt");


	FPRINTF_MC(encFrame.dbg_files.h264_cfg, "%d\n", cmd.frame_width/16);
	FPRINTF_MC(encFrame.dbg_files.h264_cfg, "%d\n", cmd.frame_height/16);
	FPRINTF_MC(encFrame.dbg_files.h264_cfg, "%d\n", cmd.useHighProfile);
	FPRINTF_MC(encFrame.dbg_files.h264_cfg, "%d\n", cmd.profile_and_level);
	//FPRINTF_MC(encFrame.dbg_files.h264_cfg, "%d\n", cmd.PQIndexConst);
#endif 

	//get cmd line parameters into internal data struct
	encFrame.pInputData  = &cmd;

	frameLength = (cmd.frame_width * cmd.frame_height * 3)/2; //4:2:0
	yLength     = cmd.frame_width * cmd.frame_height;
	uvLength    = yLength/4;
	y_psnr_avg = 0;
	u_psnr_avg = 0;
	v_psnr_avg = 0;
	numbOfSliceMBs  = cmd.frame_width>>4;

	//memory allocation
	encFrame.pInFrameData  = (UInt8 *)malloc(frameLength * sizeof(char));
	encFrame.pOutFrameData = (UInt8 *)malloc(frameLength * sizeof(char));
	encFrame.pRefFrameData = (UInt8 *)malloc(frameLength * sizeof(char));
	encFrame.pLineMemory1  = (pLineMemory_t)calloc(numbOfSliceMBs,sizeof(lineMemory_t));
	encFrame.pLineMemory2  = (pLineMemory_t)calloc(numbOfSliceMBs,sizeof(lineMemory_t));
	encFrame.pMvMemory     = (pMvMemory_t)calloc((cmd.frame_width * cmd.frame_height)>>8, sizeof(mvMemory_t));
	encFrame.streamBuffer.pPictStream  = malloc(frameLength*5);//should be sufficient memory for 1 frame
	encFrame.streamBuffer.pPictHeader  = malloc(1000000); //malloc(20);
	encFrame.streamBuffer.pSliceHeader = malloc(1000000); //malloc(20);
	encFrame.streamBuffer.pSequenceHeader    = malloc(1000000); //malloc(46);
	encFrame.bcrBuffer = 0;
	encFrame.PQIndex = 5;	
	encFrame.lastFrameBytes = 0;

	//get approximate frame rate. 6.1.14.4	
    if (encFrame.pInputData->framerate_ind == 0)//FR is implicitly declared
    {
		encFrame.framerate = 25; //default
		//get framerate approximation only from framerate_nr
        switch(encFrame.pInputData->framerate_nr)
		{
			case 0:
				//forbidden
			case 1:
				encFrame.framerate = 24;
				break;
			case 2:
				encFrame.framerate = 25;
				break;
			case 3:
				encFrame.framerate = 30;
				break;
			case 4:
				encFrame.framerate = 50;
				break;
			case 5:
				encFrame.framerate = 60;
				break;
			case 6:
				encFrame.framerate = 48;
				break;
			case 7:
				encFrame.framerate = 72;
				break;
			default:
				//SMPTE reserved
				break;
		}			    
    }
    else//FR is explicitly declared
    {
        // FRAMERATEEXP: 6.1.14.4.4
        encFrame.framerate = (encFrame.pInputData->framerate_exp + 1) / 32;//frame rate = (FRAMERATEEXP + 1) / 32.0 Hz.
    }
	
	/***************************************************************************************/
	/********************************Begin processing***************************************/
	/***************************************************************************************/
	

	//loop over frames
	encFrame.h264_frame_num = 0;
	encFrame.idr_pic_id     = 1;
	encFrame.poc            = 0;
	for(encFrame.frameCntr = 0; encFrame.frameCntr < cmd.numbFrames; encFrame.frameCntr++)
	{
#if 0

		printf("encoding frame Nr: %d    *************************************\n",encFrame.frameCntr);
#endif		
		//read input file
		fread(encFrame.pInFrameData, sizeof(char), frameLength, fileInput);

		encFrame.isFirstFrame = FALSE;
		if(encFrame.frameCntr == 0)
		{
			encFrame.isFirstFrame = TRUE;
		}
		
		//hardware compliance
		//mvRange 0       : [64x32]
		//mvRange 1       : [128x64]
		//mvRange 2       : [512x128]
		//mvRange 3       : [1024x256]
		if( encFrame.pInputData->frame_width < 352 )
		{
            encFrame.mvRange = 0;
		}
        else if( encFrame.pInputData->frame_width < 752 )
		{
            encFrame.mvRange = 1;
		}
        else if( encFrame.pInputData->frame_width < 1280 )
		{
            encFrame.mvRange = 2;
		}
        else
		{
            encFrame.mvRange = 3;
		}
		
		frame_motion_estimation(&encFrame); //motion estimation over entire frame
		encoder_manager(&encFrame);         //decide gop structure
		rdOpt(&encFrame);					//make frame level decisions

#if DBG_H264_CFG
	FPRINTF_MC(encFrame.dbg_files.h264_cfg_qp, "%d\n", encFrame.PQIndex);
	FPRINTF_MC(encFrame.dbg_files.h264_cfg_fr_type, "%d\n", encFrame.frameType);
#endif 
		
		encFrame.lastFrameBytes = 0;
		encFrame.streamBufferInfo.wrtSeqHdrFlag = FALSE;
		
		if(encFrame.frameType == I)		
		{
			encFrame.h264_frame_num = 0;
			encFrame.poc            = 0;
			encFrame.PQIndexInit    = encFrame.PQIndex;

			/**************write sequence layer**********************/
			encFrame.streamBufferInfo.wrtSeqHdrFlag = TRUE;//set flags for later writing strem file
			bitstream_Init(encFrame.streamBuffer.pSequenceHeader);
			writeSequenceHeader(&encFrame);
			// byte alignment
			encFrame.lastFrameBytes += bitstream_Close();
			encFrame.streamBufferInfo.seqHeaderSize = bitstream_GetLength();

			/**************write picture layer**********************/   		
			bitstream_Init(encFrame.streamBuffer.pPictHeader);
			writePictureHeader(&encFrame);
			// byte alignment
			encFrame.lastFrameBytes += bitstream_Close();
			encFrame.streamBufferInfo.pictHdrSize = bitstream_GetLength();
		}
		
		/**************write slice layer**********************/   		
		bitstream_Init(encFrame.streamBuffer.pPictStream);
		writeSliceHeader(&encFrame);
		
		//start encoding frame
		if(encFrame.frameType != S)//don't encode for skip frame
		{
			encode_frame(&encFrame);/************core encoding routine*****************/
		}
		// byte alignment
		
		bitstream_Close();	

		encFrame.lastFrameBytes += bitstream_GetLength();

		//disable start code emulation from this point
		set_scep(0);		
		//bitstream_Close_16bits();		
		total_bytes = bitstream_GetLength();		

		if(encFrame.pInputData->use_bstuffing)
			bitstream_Stuffing ( total_bytes );//stuff to min picture size (2048 bytes).

		encFrame.streamBufferInfo.pictStreamSize = bitstream_GetLength();				

		//********only for debug printf*********
		accumulatedBytes += encFrame.lastFrameBytes;
		printf("picture %d size: %d Byte",encFrame.frameCntr, encFrame.streamBufferInfo.pictStreamSize);
#if GLOBAL_DEBUG	
		FPRINTF_MC(pic_bytes, "%d\n", encFrame.streamBufferInfo.pictStreamSize);
#endif

		if(encFrame.pInputData->usePsnr)
		{
			//PSNR stuff
			double y_psnr1, u_psnr1, v_psnr1;
			y_psnr1 = calcPSNR(encFrame.pOutFrameData, encFrame.pInFrameData, yLength);
			printf("   PSNR Y: %.2f dB",y_psnr1);

			u_psnr1 = calcPSNR(encFrame.pOutFrameData+yLength, encFrame.pInFrameData+yLength, uvLength);
			printf("   PSNR U: %.2f dB",u_psnr1);

			v_psnr1 = calcPSNR(encFrame.pOutFrameData+yLength+uvLength, encFrame.pInFrameData+yLength+uvLength, uvLength);
			printf("   PSNR V: %.2f dB \n",v_psnr1);

			y_psnr_avg += y_psnr1;
			u_psnr_avg += u_psnr1;
			v_psnr_avg += v_psnr1;
		}
		else
		{
			printf("\n");
		}

		if(encFrame.frameCntr == cmd.numbFrames - 1)
		{
			//FPRINTF_MC(stdout, "Encoding completed...\n");
			printf( "\nAVG  PSNR Y: %.2f dB\n", y_psnr_avg/cmd.numbFrames);
			printf( "AVG  PSNR U: %.2f dB\n", u_psnr_avg/cmd.numbFrames);
			printf( "AVG  PSNR V: %.2f dB\n\n", v_psnr_avg/cmd.numbFrames);
			printf("stream size: %d Bytes, avgRate: %d Bits/s \n",accumulatedBytes, (accumulatedBytes / cmd.numbFrames) * 8*8);
		}
		//********only for debug printf*********

		//write stream to H.264 file
		writeStreamToFile(&encFrame, fileStream);
#if WRITE_RECONSTRUCTED
		//dump reconstructed yuv
		fwrite(encFrame.pOutFrameData, 1, frameLength, fileReconstr);
#endif
		//dump frame data for future reference
		memcpy(encFrame.pRefFrameData, encFrame.pOutFrameData, frameLength);

		encFrame.h264_frame_num++;
		encFrame.poc += 2;
	}
	fclose(fileInput);
	fclose(fileStream);
#if WRITE_RECONSTRUCTED
	fclose(fileReconstr);
#endif
	//free memory
	free(encFrame.streamBuffer.pPictStream);
	free(encFrame.pInFrameData);
	free(encFrame.pOutFrameData);
	free(encFrame.pRefFrameData);
	free(encFrame.pLineMemory1);
	free(encFrame.pLineMemory2);
	free(encFrame.pMvMemory);
#if !GLOBAL_DEBUG
	printf("execution has finished!\n");
#endif
	endTime = clock();
#if 1
    printf( "Execution took %ld seconds\n", ( endTime - startTime)/CLOCKS_PER_SEC);
#endif
	exit(EXIT_SUCCESS);
}

double calcPSNR(unsigned char *in, unsigned char *orig, int n) 
{
	double error = 0;
	double mse, rmse, psnr;
	int i;
	
	for (i = 0; i < n; i++) {
		int diff = (int) in[i] - (int) orig[i];
		error += diff * diff;
	}
	
	mse = error / n;
	rmse = sqrt(mse);
	psnr = 20.0 * log(255.0 / rmse) / log(10.0);
	
	return psnr;
}

void writeStreamToFile(pEncFrame_t pEncFrame, FILE *fileStream)
{
	if(pEncFrame->streamBufferInfo.wrtSeqHdrFlag)
	{
		fwrite(pEncFrame->streamBuffer.pSequenceHeader, 1, pEncFrame->streamBufferInfo.seqHeaderSize, fileStream);

		fwrite(pEncFrame->streamBuffer.pPictHeader, 1, pEncFrame->streamBufferInfo.pictHdrSize, fileStream);
	}
	
	//write picture data
	fwrite(pEncFrame->streamBuffer.pPictStream, 1, pEncFrame->streamBufferInfo.pictStreamSize, fileStream);	
}


