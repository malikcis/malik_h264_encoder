/*****************************************************************************  
 *  
 *  H264 AVC CODEC  
 *  
 *  Copyright(C) 2020 llcc <malik.cisse@abateck.com>  
 *  
 *  This program is free software ; you can redistribute it and/or modify  
 *  it under the terms of the GNU General Public License as published by  
 *  the Free Software Foundation ; either version 2 of the License, or  
 *  (at your option) any later version.  
 *  
 *  This program is distributed in the hope that it will be useful,  
 *  but WITHOUT ANY WARRANTY ; without even the implied warranty of  
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the  
 *  GNU General Public License for more details.  
 *  
 *  You should have received a copy of the GNU General Public License  
 *  along with this program ; if not, write to the Free Software  
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA  
 *  
 ****************************************************************************/ 
#include "mem_control.h"
#include "assert.h"


void writeCurrMemForNnz(pBlockInfo_t pBlockInfo)
{
	int i;
	for (i = 0; i < 24; i++)
	{
		//save nnz
		pBlockInfo->pCurrLineMem[pBlockInfo->sliceMbNumb].nnz[i] = (Int16)pBlockInfo->nnz[i];
	}			
}

void writeCurrMemForIntraPred(pBlockInfo_t pBlockInfo)
{
	UInt8 blockIdx;

	//inf: tcReconst[blockIdx][y][x]
	//luma intra 16x16 right column
	blockIdx = 1;
	pBlockInfo->pCurrLineMem[pBlockInfo->sliceMbNumb].intraPR[0] = pBlockInfo->block8x8Buff.tcReconst[blockIdx][0][7];
	pBlockInfo->pCurrLineMem[pBlockInfo->sliceMbNumb].intraPR[1] = pBlockInfo->block8x8Buff.tcReconst[blockIdx][1][7];
	pBlockInfo->pCurrLineMem[pBlockInfo->sliceMbNumb].intraPR[2] = pBlockInfo->block8x8Buff.tcReconst[blockIdx][2][7];
	pBlockInfo->pCurrLineMem[pBlockInfo->sliceMbNumb].intraPR[3] = pBlockInfo->block8x8Buff.tcReconst[blockIdx][3][7];
	pBlockInfo->pCurrLineMem[pBlockInfo->sliceMbNumb].intraPR[4] = pBlockInfo->block8x8Buff.tcReconst[blockIdx][4][7];
	pBlockInfo->pCurrLineMem[pBlockInfo->sliceMbNumb].intraPR[5] = pBlockInfo->block8x8Buff.tcReconst[blockIdx][5][7];
	pBlockInfo->pCurrLineMem[pBlockInfo->sliceMbNumb].intraPR[6] = pBlockInfo->block8x8Buff.tcReconst[blockIdx][6][7];
	pBlockInfo->pCurrLineMem[pBlockInfo->sliceMbNumb].intraPR[7] = pBlockInfo->block8x8Buff.tcReconst[blockIdx][7][7];

	blockIdx = 3;
	pBlockInfo->pCurrLineMem[pBlockInfo->sliceMbNumb].intraPR[8] =  pBlockInfo->block8x8Buff.tcReconst[blockIdx][0][7];
	pBlockInfo->pCurrLineMem[pBlockInfo->sliceMbNumb].intraPR[9] =  pBlockInfo->block8x8Buff.tcReconst[blockIdx][1][7];
	pBlockInfo->pCurrLineMem[pBlockInfo->sliceMbNumb].intraPR[10] = pBlockInfo->block8x8Buff.tcReconst[blockIdx][2][7];
	pBlockInfo->pCurrLineMem[pBlockInfo->sliceMbNumb].intraPR[11] = pBlockInfo->block8x8Buff.tcReconst[blockIdx][3][7];
	pBlockInfo->pCurrLineMem[pBlockInfo->sliceMbNumb].intraPR[12] = pBlockInfo->block8x8Buff.tcReconst[blockIdx][4][7];
	pBlockInfo->pCurrLineMem[pBlockInfo->sliceMbNumb].intraPR[13] = pBlockInfo->block8x8Buff.tcReconst[blockIdx][5][7];
	pBlockInfo->pCurrLineMem[pBlockInfo->sliceMbNumb].intraPR[14] = pBlockInfo->block8x8Buff.tcReconst[blockIdx][6][7];
	pBlockInfo->pCurrLineMem[pBlockInfo->sliceMbNumb].intraPR[15] = pBlockInfo->block8x8Buff.tcReconst[blockIdx][7][7];

	//luma intra 16x16 bottom line
	blockIdx = 2;
	pBlockInfo->pCurrLineMem[pBlockInfo->sliceMbNumb].intraPB[0] = pBlockInfo->block8x8Buff.tcReconst[blockIdx][7][0];
	pBlockInfo->pCurrLineMem[pBlockInfo->sliceMbNumb].intraPB[1] = pBlockInfo->block8x8Buff.tcReconst[blockIdx][7][1];
	pBlockInfo->pCurrLineMem[pBlockInfo->sliceMbNumb].intraPB[2] = pBlockInfo->block8x8Buff.tcReconst[blockIdx][7][2];
	pBlockInfo->pCurrLineMem[pBlockInfo->sliceMbNumb].intraPB[3] = pBlockInfo->block8x8Buff.tcReconst[blockIdx][7][3];
	pBlockInfo->pCurrLineMem[pBlockInfo->sliceMbNumb].intraPB[4] = pBlockInfo->block8x8Buff.tcReconst[blockIdx][7][4];
	pBlockInfo->pCurrLineMem[pBlockInfo->sliceMbNumb].intraPB[5] = pBlockInfo->block8x8Buff.tcReconst[blockIdx][7][5];
	pBlockInfo->pCurrLineMem[pBlockInfo->sliceMbNumb].intraPB[6] = pBlockInfo->block8x8Buff.tcReconst[blockIdx][7][6];
	pBlockInfo->pCurrLineMem[pBlockInfo->sliceMbNumb].intraPB[7] = pBlockInfo->block8x8Buff.tcReconst[blockIdx][7][7];

	blockIdx = 3;
	pBlockInfo->pCurrLineMem[pBlockInfo->sliceMbNumb].intraPB[8] =  pBlockInfo->block8x8Buff.tcReconst[blockIdx][7][0];
	pBlockInfo->pCurrLineMem[pBlockInfo->sliceMbNumb].intraPB[9] =  pBlockInfo->block8x8Buff.tcReconst[blockIdx][7][1];
	pBlockInfo->pCurrLineMem[pBlockInfo->sliceMbNumb].intraPB[10] = pBlockInfo->block8x8Buff.tcReconst[blockIdx][7][2];
	pBlockInfo->pCurrLineMem[pBlockInfo->sliceMbNumb].intraPB[11] = pBlockInfo->block8x8Buff.tcReconst[blockIdx][7][3];
	pBlockInfo->pCurrLineMem[pBlockInfo->sliceMbNumb].intraPB[12] = pBlockInfo->block8x8Buff.tcReconst[blockIdx][7][4];
	pBlockInfo->pCurrLineMem[pBlockInfo->sliceMbNumb].intraPB[13] = pBlockInfo->block8x8Buff.tcReconst[blockIdx][7][5];
	pBlockInfo->pCurrLineMem[pBlockInfo->sliceMbNumb].intraPB[14] = pBlockInfo->block8x8Buff.tcReconst[blockIdx][7][6];
	pBlockInfo->pCurrLineMem[pBlockInfo->sliceMbNumb].intraPB[15] = pBlockInfo->block8x8Buff.tcReconst[blockIdx][7][7];

	//chroma u 8x8 predictors right border
	blockIdx = 4;
	pBlockInfo->pCurrLineMem[pBlockInfo->sliceMbNumb].intraPRu[0] = pBlockInfo->block8x8Buff.tcReconst[blockIdx][0][7];
	pBlockInfo->pCurrLineMem[pBlockInfo->sliceMbNumb].intraPRu[1] = pBlockInfo->block8x8Buff.tcReconst[blockIdx][1][7];
	pBlockInfo->pCurrLineMem[pBlockInfo->sliceMbNumb].intraPRu[2] = pBlockInfo->block8x8Buff.tcReconst[blockIdx][2][7];
	pBlockInfo->pCurrLineMem[pBlockInfo->sliceMbNumb].intraPRu[3] = pBlockInfo->block8x8Buff.tcReconst[blockIdx][3][7];
	pBlockInfo->pCurrLineMem[pBlockInfo->sliceMbNumb].intraPRu[4] = pBlockInfo->block8x8Buff.tcReconst[blockIdx][4][7];
	pBlockInfo->pCurrLineMem[pBlockInfo->sliceMbNumb].intraPRu[5] = pBlockInfo->block8x8Buff.tcReconst[blockIdx][5][7];
	pBlockInfo->pCurrLineMem[pBlockInfo->sliceMbNumb].intraPRu[6] = pBlockInfo->block8x8Buff.tcReconst[blockIdx][6][7];
	pBlockInfo->pCurrLineMem[pBlockInfo->sliceMbNumb].intraPRu[7] = pBlockInfo->block8x8Buff.tcReconst[blockIdx][7][7];

	//chroma u 8x8 predictors bottom border
	blockIdx = 4;
	pBlockInfo->pCurrLineMem[pBlockInfo->sliceMbNumb].intraPBu[0] = pBlockInfo->block8x8Buff.tcReconst[blockIdx][7][0];
	pBlockInfo->pCurrLineMem[pBlockInfo->sliceMbNumb].intraPBu[1] = pBlockInfo->block8x8Buff.tcReconst[blockIdx][7][1];
	pBlockInfo->pCurrLineMem[pBlockInfo->sliceMbNumb].intraPBu[2] = pBlockInfo->block8x8Buff.tcReconst[blockIdx][7][2];
	pBlockInfo->pCurrLineMem[pBlockInfo->sliceMbNumb].intraPBu[3] = pBlockInfo->block8x8Buff.tcReconst[blockIdx][7][3];
	pBlockInfo->pCurrLineMem[pBlockInfo->sliceMbNumb].intraPBu[4] = pBlockInfo->block8x8Buff.tcReconst[blockIdx][7][4];
	pBlockInfo->pCurrLineMem[pBlockInfo->sliceMbNumb].intraPBu[5] = pBlockInfo->block8x8Buff.tcReconst[blockIdx][7][5];
	pBlockInfo->pCurrLineMem[pBlockInfo->sliceMbNumb].intraPBu[6] = pBlockInfo->block8x8Buff.tcReconst[blockIdx][7][6];
	pBlockInfo->pCurrLineMem[pBlockInfo->sliceMbNumb].intraPBu[7] = pBlockInfo->block8x8Buff.tcReconst[blockIdx][7][7];

	//chroma v 8x8 predictors right border
	blockIdx = 5;
	pBlockInfo->pCurrLineMem[pBlockInfo->sliceMbNumb].intraPRv[0] = pBlockInfo->block8x8Buff.tcReconst[blockIdx][0][7];
	pBlockInfo->pCurrLineMem[pBlockInfo->sliceMbNumb].intraPRv[1] = pBlockInfo->block8x8Buff.tcReconst[blockIdx][1][7];
	pBlockInfo->pCurrLineMem[pBlockInfo->sliceMbNumb].intraPRv[2] = pBlockInfo->block8x8Buff.tcReconst[blockIdx][2][7];
	pBlockInfo->pCurrLineMem[pBlockInfo->sliceMbNumb].intraPRv[3] = pBlockInfo->block8x8Buff.tcReconst[blockIdx][3][7];
	pBlockInfo->pCurrLineMem[pBlockInfo->sliceMbNumb].intraPRv[4] = pBlockInfo->block8x8Buff.tcReconst[blockIdx][4][7];
	pBlockInfo->pCurrLineMem[pBlockInfo->sliceMbNumb].intraPRv[5] = pBlockInfo->block8x8Buff.tcReconst[blockIdx][5][7];
	pBlockInfo->pCurrLineMem[pBlockInfo->sliceMbNumb].intraPRv[6] = pBlockInfo->block8x8Buff.tcReconst[blockIdx][6][7];
	pBlockInfo->pCurrLineMem[pBlockInfo->sliceMbNumb].intraPRv[7] = pBlockInfo->block8x8Buff.tcReconst[blockIdx][7][7];

	//chroma v 8x8 predictors bottom border
	blockIdx = 5;
	pBlockInfo->pCurrLineMem[pBlockInfo->sliceMbNumb].intraPBv[0] = pBlockInfo->block8x8Buff.tcReconst[blockIdx][7][0];
	pBlockInfo->pCurrLineMem[pBlockInfo->sliceMbNumb].intraPBv[1] = pBlockInfo->block8x8Buff.tcReconst[blockIdx][7][1];
	pBlockInfo->pCurrLineMem[pBlockInfo->sliceMbNumb].intraPBv[2] = pBlockInfo->block8x8Buff.tcReconst[blockIdx][7][2];
	pBlockInfo->pCurrLineMem[pBlockInfo->sliceMbNumb].intraPBv[3] = pBlockInfo->block8x8Buff.tcReconst[blockIdx][7][3];
	pBlockInfo->pCurrLineMem[pBlockInfo->sliceMbNumb].intraPBv[4] = pBlockInfo->block8x8Buff.tcReconst[blockIdx][7][4];
	pBlockInfo->pCurrLineMem[pBlockInfo->sliceMbNumb].intraPBv[5] = pBlockInfo->block8x8Buff.tcReconst[blockIdx][7][5];
	pBlockInfo->pCurrLineMem[pBlockInfo->sliceMbNumb].intraPBv[6] = pBlockInfo->block8x8Buff.tcReconst[blockIdx][7][6];
	pBlockInfo->pCurrLineMem[pBlockInfo->sliceMbNumb].intraPBv[7] = pBlockInfo->block8x8Buff.tcReconst[blockIdx][7][7];
}

void mbMemRead(UInt32 frame_width, 
			   UInt32 frame_height, 
			   UInt32 row, 
			   UInt32 col, 
			   UInt8 *pPicture_data,
			   UInt8 ***mb_data)
{
	UInt8 i;
	UInt8 j;
	UInt32 baseAddr;

	//blockIdx == 0:
	for (i = 0; i < 8; i++)//get block data
	{
		for (j = 0; j < 8; j++)
		{
			mb_data[0][i][j] = pPicture_data[(row+i)*frame_width + (col+j)];
		}
	}
	
	//blockIdx == 1:
	for (i = 0; i < 8; i++)//get block data
	{
		for (j = 0; j < 8; j++)
		{
			mb_data[1][i][j] = pPicture_data[(row+i)*frame_width + (col+8+j)];
		}
	}
		
	//blockIdx == 2:
	for (i = 0; i < 8; i++)//get block data
	{
		for (j = 0; j < 8; j++)
		{
			mb_data[2][i][j] = pPicture_data[(row+8+i)*frame_width + (col+j)];
		}
	}

	//blockIdx == 3:
	for (i = 0; i < 8; i++)//get block data
	{
		for (j = 0; j < 8; j++)
		{
			mb_data[3][i][j] = pPicture_data[(row+8+i)*frame_width + (col+8+j)];
		}
	}

	//blockIdx == 4:
	baseAddr = frame_width * frame_height;//base address cb
	for (i = 0; i < 8; i++)//get block data
	{
		for (j = 0; j < 8; j++)
		{
			mb_data[4][i][j] = pPicture_data[((row>>1)+i)*(frame_width>>1) + (col>>1)+j + baseAddr];
		}
	}

	//blockIdx == 5:
	baseAddr = 5*(frame_width * frame_height) / 4;//base address cr
	for (i = 0; i < 8; i++)//get block data
	{
		for (j = 0; j < 8; j++)
		{
			mb_data[5][i][j] = pPicture_data[((row>>1)+i)*(frame_width>>1) + (col>>1)+j + baseAddr];
		}
	}	
}

void me_get_padded_lum_block(UInt32 frame_width, 
							UInt32 frame_height, 
							pos_t pos,
							blockSize_t blockSize,
							UInt8 *pPicture_data,
							UInt8 **mb_data)
{
	UInt8 x;
	UInt8 y;
	Int32 row, col;
	Int32 vert_offs;
	Int32 hor_offs;

	row = pos.ver;
	col = pos.hor;

	//loop over luma block
	for (y = 0; y < blockSize.rows ; y++)
	{
		for (x = 0; x < blockSize.cols; x++)
		{
			//mb_data[y][x] = pPicture_data[(row+y)*frame_width + (col+x)];//normal case withouth padding
			hor_offs = col+x;
			vert_offs = row+y;
			if(hor_offs < 0)//horizontal padding
			{
				hor_offs = 0;
			}
			if(hor_offs >= (Int32)frame_width)//horizontal padding
			{
				hor_offs = frame_width - 1;
			}
			if(vert_offs < 0)//vertical padding
			{
				vert_offs = 0;
			}
			if(vert_offs >= (Int32)frame_height)//vertical padding
			{
				vert_offs = frame_height - 1;
			}
			mb_data[y][x] = pPicture_data[vert_offs*frame_width + hor_offs];
		}
	}
}

void me_get_padded_chr_blocks(UInt32 frame_width, 
						UInt32 frame_height, 
						pos_t pos,
						blockSize_t blockSize,
						UInt8 *pPicture_data,
						UInt8 **data_u,
						UInt8 **data_v)
{
	UInt8 x;
	UInt8 y;
	Int32 row, col;
	UInt32 baseAddr_u;
	UInt32 baseAddr_v;
	Int32 vert_offs;
	Int32 hor_offs;

	row = pos.ver;
	col = pos.hor;

	baseAddr_u = frame_width * frame_height;//base address cb
	baseAddr_v = 5*(frame_width * frame_height) / 4;//base address cr
	for (y = 0; y < blockSize.rows; y++)//get block data
	{
		for (x = 0; x < blockSize.cols; x++)
		{
			//data_u[y][x] = pPicture_data[(row+y)*(frame_width>>1) + col+x + baseAddr_u];//normal case withouth padding
			//data_v[y][x] = pPicture_data[(row+y)*(frame_width>>1) + col+x + baseAddr_v];//normal case withouth padding
			hor_offs = col+x;
			vert_offs = row+y;
			if(hor_offs < 0)//horizontal padding
			{
				hor_offs = 0;
			}
			if(hor_offs >= (Int32)(frame_width>>1))//horizontal padding
			{
				hor_offs = (frame_width>>1) - 1;
			}
			if(vert_offs < 0)//vertical padding
			{
				vert_offs = 0;
			}
			if(vert_offs >= (Int32)(frame_height>>1))//vertical padding
			{
				vert_offs = (frame_height>>1) - 1;
			}
			data_u[y][x] = pPicture_data[vert_offs*(frame_width>>1) + hor_offs + baseAddr_u];
			data_v[y][x] = pPicture_data[vert_offs*(frame_width>>1) + hor_offs + baseAddr_v];
		}
	}	
}

void mbWriteRefToMem(pBlockInfo_t pBlockInfo, pEncFrame_t pEncFrame)
{
	//loop over 6 8x8 blocks
	for (pBlockInfo->blockIdx = 0; pBlockInfo->blockIdx < 6; pBlockInfo->blockIdx++)
	{
		switch(pBlockInfo->blockIdx)
		{
		case 0:   
		case 1: 
		case 2:  
		case 3:
		   writeLumBlock(pBlockInfo, pEncFrame);
			break;
		case 4:
		case 5:
		   writeChrBlock(pBlockInfo, pEncFrame);
			break;
		default: // blockNmb == 5, nothing more to do
			break;
		}
	}
}

void writeLumBlock(pBlockInfo_t pBlockInfo, pEncFrame_t pEncFrame)
{
	UInt8 i;
	UInt8 j;
	UInt32 row;
	UInt32 col;

	col = pBlockInfo->mbX<<4;
	row = pBlockInfo->mbY<<4;

	switch (pBlockInfo->blockIdx) 
	{
	case 0:
		for (i = 0; i < 8; i++)//get block data
		{
			for (j = 0; j < 8; j++)
			{
				pEncFrame->pOutFrameData[(row+i)*pEncFrame->pInputData->frame_width + (col+j)] = (UInt8)pBlockInfo->block8x8Buff.tcReconst[0][i][j];
			}
		}
		break;
	case 1:
		for (i = 0; i < 8; i++)//get block data
		{
			for (j = 0; j < 8; j++)
			{
				pEncFrame->pOutFrameData[(row+i)*pEncFrame->pInputData->frame_width + (col+8+j)] = (UInt8)pBlockInfo->block8x8Buff.tcReconst[1][i][j];
			}
		}
		break;
	case 2:
		for (i = 0; i < 8; i++)//get block data
		{
			for (j = 0; j < 8; j++)
			{
				pEncFrame->pOutFrameData[(row+8+i)*pEncFrame->pInputData->frame_width + (col+j)] = (UInt8)pBlockInfo->block8x8Buff.tcReconst[2][i][j];
			}
		}
		break;
	case 3:
		for (i = 0; i < 8; i++)//get block data
		{
			for (j = 0; j < 8; j++)
			{
				pEncFrame->pOutFrameData[(row+8+i)*pEncFrame->pInputData->frame_width + (col+8+j)] = (UInt8)pBlockInfo->block8x8Buff.tcReconst[3][i][j];
			}
		}
		break;
	default:
		break;
	}
}

void writeChrBlock(pBlockInfo_t pBlockInfo, pEncFrame_t pEncFrame)
{
	UInt8 i;
	UInt8 j;
	UInt32 baseAddr;
	UInt32 row;
	UInt32 col;

	col = pBlockInfo->mbX<<4;
	row = pBlockInfo->mbY<<4;

	switch (pBlockInfo->blockIdx) 
	{
	case 4:
		baseAddr = pEncFrame->pInputData->frame_width * pEncFrame->pInputData->frame_height;//base address cb
		for (i = 0; i < 8; i++)//get block data
		{
			for (j = 0; j < 8; j++)
			{
				pEncFrame->pOutFrameData[((row>>1)+i)*(pEncFrame->pInputData->frame_width>>1) + (col>>1)+j + baseAddr] = (UInt8)pBlockInfo->block8x8Buff.tcReconst[4][i][j];
			}
		}
		break;
	case 5:
		baseAddr = 5*(pEncFrame->pInputData->frame_width * pEncFrame->pInputData->frame_height) / 4;//base address cr
		for (i = 0; i < 8; i++)//get block data
		{
			for (j = 0; j < 8; j++)
			{
				pEncFrame->pOutFrameData[((row>>1)+i)*(pEncFrame->pInputData->frame_width>>1) + (col>>1)+j + baseAddr] = (UInt8)pBlockInfo->block8x8Buff.tcReconst[5][i][j];
			}
		}
		break;
	default:
		break;
	}
}

//Allocate 2D memory array -> unsigned char array2D[rows][columns]
void get_mem2D(UInt8 ***array2D, Int32 rows, Int32 columns)
{
	Int32 i;

	*array2D = (UInt8**)calloc(rows, sizeof(UInt8*));
	assert(*array2D  != NULL);

	(*array2D)[0] = (UInt8* )calloc(columns*rows,sizeof(UInt8 ));
	assert(*array2D  != NULL);

	for(i=1;i<rows;i++)
	{
		(*array2D)[i] = (*array2D)[i-1] + columns ;
	}

}
//Allocate 3D memory array -> unsigned char array3D[idx][rows][columns]
void get_mem3D(UInt8 ****array3D, Int32 idx, Int32 rows, Int32 columns)
{
  Int32 j;

 (*array3D) = (UInt8***)calloc(idx,sizeof(UInt8**));
  assert(*array3D  != NULL);

  for(j=0;j<idx;j++)
  {
    get_mem2D( (*array3D)+j, rows, columns );
  }

}

//Allocate 2D memory array -> Int32 array2D[rows][columns]
void get_int32_mem2D(Int32 ***array2D, Int32 rows, Int32 columns)
{
	Int32 i;

	*array2D = (Int32**)calloc(rows, sizeof(UInt8*));
	assert(*array2D  != NULL);

	(*array2D)[0] = (Int32* )calloc(columns*rows,sizeof(UInt8 ));
	assert(*array2D  != NULL);

	for(i=1;i<rows;i++)
	{
		(*array2D)[i] = (*array2D)[i-1] + columns ;
	}

}
//Allocate 3D memory array -> Int32 array3D[idx][rows][columns]
void get_int32_mem3D(Int32 ****array3D, Int32 idx, Int32 rows, Int32 columns)
{
  Int32 j;

 (*array3D) = (Int32***)calloc(idx,sizeof(UInt8**));
  assert(*array3D  != NULL);

  for(j=0;j<idx;j++)
  {
    get_int32_mem2D( (*array3D)+j, rows, columns );
  }

}

/*!
 ************************************************************************
 * \brief
 *    free 2D memory array
 *    which was allocated with get_mem2D()
 ************************************************************************
 */
void free_mem2D(UInt8 **array2D)
{
  if (array2D)
  {
    if (array2D[0])
      free (array2D[0]);
    else printf ("free_mem2D: trying to free unused memory");

    free (array2D);
  } else
  {
    printf ("free_mem2D: trying to free unused memory");
  }
}

/*!
 ************************************************************************
 * \brief
 *    free 3D memory array
 *    which was allocated with get_mem3D()
 ************************************************************************
 */
void free_mem3D(UInt8 ***array3D, Int32 idx)
{
  int i;

  if (array3D)
  {
    for (i=0;i<idx;i++)
    { 
      free_mem2D(array3D[i]);
    }
   free (array3D);
  } else
  {
    printf ("free_mem3D: trying to free unused memory");
  }
}
