/*****************************************************************************  
 *  
 *  H264 AVC CODEC  
 *  
 *  Copyright(C) 2020 llcc <malik.cisse@abateck.com>  
 *  
 *  This program is free software ; you can redistribute it and/or modify  
 *  it under the terms of the GNU General Public License as published by  
 *  the Free Software Foundation ; either version 2 of the License, or  
 *  (at your option) any later version.  
 *  
 *  This program is distributed in the hope that it will be useful,  
 *  but WITHOUT ANY WARRANTY ; without even the implied warranty of  
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the  
 *  GNU General Public License for more details.  
 *  
 *  You should have received a copy of the GNU General Public License  
 *  along with this program ; if not, write to the Free Software  
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA  
 *  
 ****************************************************************************/ 
#include "me.h"
#include "mem_control.h"
#include "me_algo.h"
#include <math.h>

#define MB_BLOCK_SIZE   16

//debug stuff
#define PRINT_MV_PREDS	0
#define MV_PRED_RESULTS 0
#define DBG_HP_MVS      0
#define GLOBAL_DEBUG	0


Int16 s_RndTbl[4]  = {0, 0, 0, 1};
Int16 RndTbl[3] = {1, 0, -1};

void frame_motion_estimation(pEncFrame_t pEncFrame)
{
	UInt32 row;
	UInt32 col;
	UInt32 height;
	UInt32 width;
	blockInfo_t blockInfo;
	
	//frame initialisation
	get_mem3D(&blockInfo.block8x8Buff.ppp_currentData, 6, 8, 8);
	get_mem2D(&blockInfo.block8x8Buff.pp_ref_lum, 16, 16);
	get_mem2D(&blockInfo.block8x8Buff.pp_ref_chr_u_gross, 9, 9);
	get_mem2D(&blockInfo.block8x8Buff.pp_ref_chr_v_gross, 9, 9);
	get_mem2D(&blockInfo.block8x8Buff.pp_ref_chr_u_gross_d, 12, 10);
	get_mem2D(&blockInfo.block8x8Buff.pp_ref_chr_v_gross_d, 12, 10);
	get_mem2D(&blockInfo.block8x8Buff.pp_ref_chr_u, 8, 8);
	get_mem2D(&blockInfo.block8x8Buff.pp_ref_chr_v, 8, 8);
	get_mem2D(&blockInfo.block8x8Buff.pp_ref_lum_19x19, 19, 19);	
	get_mem2D(&blockInfo.block8x8Buff.pp_ref_lum_32x24, 32, 24);//debug
	get_mem2D(&blockInfo.block8x8Buff.pp_ref_lum_17x17, 17, 17);

	height                       = pEncFrame->pInputData->frame_height;
	width                        = pEncFrame->pInputData->frame_width;
	blockInfo.frame_mb_width     = width>>4;
	blockInfo.frame_mb_height    = height>>4;
	blockInfo.isQuarterSample    = pEncFrame->pInputData->isQuarterSample;
	blockInfo.mvRange            = pEncFrame->mvRange;
	blockInfo.roundCtrl          = pEncFrame->roundCtrl;
	blockInfo.isFastUV           = pEncFrame->pInputData->isFastUV;
	blockInfo.pMvMemory          = pEncFrame->pMvMemory;
	blockInfo.skip_sad_thr       = SKIP_SAD_THR;
	blockInfo.intra_sad_thr      = (unsigned short)INTRA_SAD_THR;
	pEncFrame->frame_is_skip	 = FALSE;
	pEncFrame->frame_is_sc		 = FALSE;
	pEncFrame->frame_is_noisy	 = FALSE;
	pEncFrame->sumMvX			 = 0;
	pEncFrame->sumMvY			 = 0;

	FPRINTF_MC(pEncFrame->dbg_files.mb_act, "   ");
	for(col = 0; col < width; col+=16) 
	{
		FPRINTF_MC(pEncFrame->dbg_files.mb_act, "  %3d", col >> 4);
	}
	FPRINTF_MC(pEncFrame->dbg_files.mb_act, "\n");
	
	pEncFrame->dbg_files.mb_cand_buf[0][0] = 0;
	pEncFrame->dbg_files.mb_cand_buf[1][0] = 0;
	pEncFrame->dbg_files.mb_cand_buf[2][0] = 0;

	//loop over Macroblocks
	for(row = 0; row < height; row+=16)
	{
		FPRINTF_MC(pEncFrame->dbg_files.mb_act, "%2d:", row >> 4);
		for(col = 0; col < width; col+=16) 
		{
			//set blockInfo
			blockInfo.mbX = col>>4;
			blockInfo.mbY = row>>4;

			//init
			blockInfo.mbNumb = blockInfo.mbX + (blockInfo.mbY * (pEncFrame->pInputData->frame_width>>4));
			blockInfo.sliceMbNumb = blockInfo.mbX;

			//get current MB
			mbMemRead(width, height, row,  col, pEncFrame->pInFrameData, blockInfo.block8x8Buff.ppp_currentData);
			
			if(pEncFrame->isFirstFrame == FALSE)//run ME even on Intra pictures for statistic (act and temporal me cands)
			{
				mb_motion_estimation(&blockInfo, pEncFrame);//motion_estimation_mb
			}
			else
			{
#if GLOBAL_DEBUG
	FPRINTF_MC(pEncFrame->dbg_files.fp_mv_xy, "#Fr Nr: %d, MB(%d, %d), fp_mv_x, fp_mv_y\n", pEncFrame->frameCntr, blockInfo.mbX, blockInfo.mbY);			
	FPRINTF_MC(pEncFrame->dbg_files.fp_mv_xy, "?\t%d\t%d\n",0, 0);			
	FPRINTF_MC(pEncFrame->dbg_files.hp_mv_xy, "#Fr Nr: %d, MB(%d, %d), hp_mv_x, hp_mv_y\n", pEncFrame->frameCntr, blockInfo.mbX, blockInfo.mbY);			
	FPRINTF_MC(pEncFrame->dbg_files.hp_mv_xy, "?\t%d\t%d\n",0, 0);

	FPRINTF_MC(pEncFrame->dbg_files.qp_mv_xy, "#Fr Nr: %d, MB(%d, %d), hp_mv_x, hp_mv_y\n", pEncFrame->frameCntr, blockInfo.mbX, blockInfo.mbY);			
	FPRINTF_MC(pEncFrame->dbg_files.qp_mv_xy, "?\t%d\t%d\n",0, 0);


#endif
#if DBG_HP_MVS
	FPRINTF_MC(pEncFrame->dbg_files.hp_mvx, "%d\n", 0);
	FPRINTF_MC(pEncFrame->dbg_files.hp_mvy, "%d\n", 0);
#endif
			}
		}
		FPRINTF_MC(pEncFrame->dbg_files.mb_act, "\n");
		FPRINTF_MC(pEncFrame->dbg_files.mb_cand, "%s\n%s\n%s\n\n",
		        pEncFrame->dbg_files.mb_cand_buf[0],
		        pEncFrame->dbg_files.mb_cand_buf[1],
		        pEncFrame->dbg_files.mb_cand_buf[2] );
	}//loop over Macroblocks
	FPRINTF_MC(pEncFrame->dbg_files.mb_act,  "\n\n");
	FPRINTF_MC(pEncFrame->dbg_files.mb_cand, "\n\n");

	//free memory
	free_mem3D(blockInfo.block8x8Buff.ppp_currentData, 6);
	free_mem2D(blockInfo.block8x8Buff.pp_ref_lum);
	free_mem2D(blockInfo.block8x8Buff.pp_ref_chr_u_gross);
	free_mem2D(blockInfo.block8x8Buff.pp_ref_chr_v_gross);
	free_mem2D(blockInfo.block8x8Buff.pp_ref_chr_u_gross_d);
	free_mem2D(blockInfo.block8x8Buff.pp_ref_chr_v_gross_d);
	free_mem2D(blockInfo.block8x8Buff.pp_ref_chr_u);
	free_mem2D(blockInfo.block8x8Buff.pp_ref_chr_v);
	free_mem2D(blockInfo.block8x8Buff.pp_ref_lum_19x19);
	free_mem2D(blockInfo.block8x8Buff.pp_ref_lum_32x24);
	free_mem2D(blockInfo.block8x8Buff.pp_ref_lum_17x17);
}

void mb_motion_estimation(pBlockInfo_t pBlockInfo, pEncFrame_t pEncFrame)
{
	UInt32 height, width;
	vector_t best_match, hp_mv, fp_mv;

	//init
	height = pEncFrame->pInputData->frame_height;
	width  = pEncFrame->pInputData->frame_width;

	//init
	pBlockInfo->mbType = INTER;
	pBlockInfo->mvX = 0;
	pBlockInfo->mvY = 0;

	//get mv predictor
	mv_prediction(pBlockInfo);
	
	/******************** motion estimation****************************************/ 

	motion_estimation(pBlockInfo, pEncFrame, &best_match, &hp_mv, &fp_mv);

	pBlockInfo->mvX = best_match.hor;
	pBlockInfo->mvY = best_match.ver;

	/********************end motion estimation************************************/
	
	//clipping of vector according to mvRange is already done in me_algo

	pEncFrame->sumMvX += abs(fp_mv.hor);
	pEncFrame->sumMvY += abs(fp_mv.ver);
	
	//save mv in mem for prediction
	pEncFrame->pMvMemory[pBlockInfo->mbNumb].mv.hor    = pBlockInfo->mvX;
	pEncFrame->pMvMemory[pBlockInfo->mbNumb].mv.ver    = pBlockInfo->mvY;
	pEncFrame->pMvMemory[pBlockInfo->mbNumb].fp_mv.hor = fp_mv.hor;
	pEncFrame->pMvMemory[pBlockInfo->mbNumb].fp_mv.ver = fp_mv.ver;
	pEncFrame->pMvMemory[pBlockInfo->mbNumb].skip      = pBlockInfo->skip;
	pEncFrame->pMvMemory[pBlockInfo->mbNumb].mv.mbType = pBlockInfo->mbType;
	
#if GLOBAL_DEBUG
	FPRINTF_MC(pEncFrame->dbg_files.fp_mv_xy, "#Fr Nr: %d, MB(%d, %d), fp_mv_x, fp_mv_y\n", pEncFrame->frameCntr, pBlockInfo->mbX, pBlockInfo->mbY);			
	FPRINTF_MC(pEncFrame->dbg_files.fp_mv_xy, "\t%d\t%d\n",fp_mv.hor, fp_mv.ver);						
	FPRINTF_MC(pEncFrame->dbg_files.qp_mv_xy, "#Fr Nr: %d, MB(%d, %d), hp_mv_x, hp_mv_y\n", pEncFrame->frameCntr, pBlockInfo->mbX, pBlockInfo->mbY);			
	FPRINTF_MC(pEncFrame->dbg_files.qp_mv_xy, "\t%d\t%d\n",pBlockInfo->mvX, pBlockInfo->mvY);

	FPRINTF_MC(pEncFrame->dbg_files.hp_mv_xy, "#Fr Nr: %d, MB(%d, %d), hp_mv_x, hp_mv_y\n", pEncFrame->frameCntr, pBlockInfo->mbX, pBlockInfo->mbY);			
	FPRINTF_MC(pEncFrame->dbg_files.hp_mv_xy, "\t%d\t%d\n",hp_mv.hor, hp_mv.ver);	
#endif

#if DBG_HP_MVS
	FPRINTF_MC(pEncFrame->dbg_files.hp_mvx, "%d\n", hp_mv.hor);
	FPRINTF_MC(pEncFrame->dbg_files.hp_mvy, "%d\n", hp_mv.ver);
#endif


	if(pBlockInfo->mbType != INTRA)
	{
		get_best_match(pBlockInfo, pEncFrame);//write interpolated and motion compensated bestmatch to memory
	}

}


void get_best_match(pBlockInfo_t pBlockInfo, pEncFrame_t pEncFrame)
{
	pos_t pos;
	blockSize_t blockSize;
	UInt8 **pp_ref_lum;         //[16][16]
	UInt8 **pp_ref_chr_u_gross; //[9][9]
	UInt8 **pp_ref_chr_v_gross; //[9][9]
	//UInt8 **pp_ref_chr_u_gross_d; //[12][10]
	//UInt8 **pp_ref_chr_v_gross_d; //[12][10]
	UInt8 **pp_ref_chr_u;       //[8][8]
	UInt8 **pp_ref_chr_v;       //[8][8]
	UInt8 int_ver;
	UInt8 int_hor;
	UInt32 col, row;
	UInt32 width, height;
	Int16 cmv_x, cmv_y;
	UInt32 x, y;
	Int16 mvX, mvY;

	mvX = pBlockInfo->mvX;//qp
	mvY = pBlockInfo->mvY;//qp

	//**********************luma processing*************************
	pp_ref_lum = pBlockInfo->block8x8Buff.pp_ref_lum;
	get_ref_luma_block(pBlockInfo, pEncFrame, mvX, mvY, pp_ref_lum);//get interpolated reference

	//**********************Chroma processing*************************
	pp_ref_chr_u_gross = pBlockInfo->block8x8Buff.pp_ref_chr_u_gross;
	pp_ref_chr_v_gross = pBlockInfo->block8x8Buff.pp_ref_chr_v_gross;	
	pp_ref_chr_u = pBlockInfo->block8x8Buff.pp_ref_chr_u;
	pp_ref_chr_v = pBlockInfo->block8x8Buff.pp_ref_chr_v;
	col    = pBlockInfo->mbX*16;
	row    = pBlockInfo->mbY*16;
	height = pEncFrame->pInputData->frame_height;
	width  = pEncFrame->pInputData->frame_width;	
	
	//mvX is at the same time 1/4pel luma MV as well as 1/8pel chroma MV since cmv(in 1/8pel) = (lmv/2)*2
	cmv_x = mvX;
	cmv_y = mvY;

	int_hor = cmv_x & 7;
	int_ver = cmv_y & 7;	

	//get FP chroma pos	
	pos.hor = (Int32)(col/2) + (cmv_x>>3);
	pos.ver = (Int32)(row/2) + (cmv_y>>3);
	blockSize.cols = 9;
	blockSize.rows = 9;
	me_get_padded_chr_blocks(width, height, pos, blockSize, pEncFrame->pRefFrameData, pp_ref_chr_u_gross, pp_ref_chr_v_gross);//get padded ref
	
	//1/8 pel chroma bilinear interpolation
	for(y = 0; y< 8; y++)
	{
		for(x = 0; x< 8; x++)
		{
			pp_ref_chr_u[y][x] = ( ( 8 - int_hor ) * ( 8 - int_ver ) * pp_ref_chr_u_gross[y][x] + 
									int_hor * ( 8 - int_ver ) * pp_ref_chr_u_gross[y][x+1] + 
									( 8 - int_hor ) * int_ver * pp_ref_chr_u_gross[y+1][x] + 
									int_hor * int_ver * pp_ref_chr_u_gross[y+1][x+1] + 32 ) >> 6; //(8-274)
			pp_ref_chr_v[y][x] = ( ( 8 - int_hor ) * ( 8 - int_ver ) * pp_ref_chr_v_gross[y][x] + 
									int_hor * ( 8 - int_ver ) * pp_ref_chr_v_gross[y][x+1] + 
									( 8 - int_hor ) * int_ver * pp_ref_chr_v_gross[y+1][x] + 
									int_hor * int_ver * pp_ref_chr_v_gross[y+1][x+1] + 32 ) >> 6; //(8-274)
		}
	}	

	//copy interpolation result back to bestmatch buffer. not need to be  done
	for(y = 0; y< 8; y++)
	{
		for(x = 0; x< 8; x++)
		{
			//luma
			pBlockInfo->pMvMemory[pBlockInfo->mbNumb].bestMatch[0][y][x] = pp_ref_lum[y][x];
			pBlockInfo->pMvMemory[pBlockInfo->mbNumb].bestMatch[1][y][x] = pp_ref_lum[y][x+8];
			pBlockInfo->pMvMemory[pBlockInfo->mbNumb].bestMatch[2][y][x] = pp_ref_lum[y+8][x];
			pBlockInfo->pMvMemory[pBlockInfo->mbNumb].bestMatch[3][y][x] = pp_ref_lum[y+8][x+8];
			//chroma
			pBlockInfo->pMvMemory[pBlockInfo->mbNumb].bestMatch[4][y][x] = pp_ref_chr_u[y][x];
			pBlockInfo->pMvMemory[pBlockInfo->mbNumb].bestMatch[5][y][x] = pp_ref_chr_v[y][x];
		}
	}
}

/*!
 ************************************************************************
 * \brief
 *    Interpolation of 1/4 subpixel. From JM10 decoder
 ************************************************************************
 */
void get_ref_luma_block(pBlockInfo_t pBlockInfo, pEncFrame_t pEncFrame, Int16 mv_x, Int16 mv_y, UInt8 **ref_lum)
{
	//JM stuff
	int x_pos;
	int y_pos;
	int dx, dy;
	int x, y;
	int i, j;
	int maxold_x,maxold_y;
	int result;
	int pres_x;
	int pres_y; 
	int tmp_res[21][21];
	static const int COEF[6] = { 1, -5, 20, 20, -5, 1 };
	Int32 vert_offs;
	Int32 hor_offs;
	UInt32 frame_width=pEncFrame->pInputData->frame_width;
	UInt32 frame_height=pEncFrame->pInputData->frame_height; 

	dx    = mv_x&3;
	dy    = mv_y&3;
	x_pos = (mv_x-dx)/4 + (pBlockInfo->mbX*16);
	y_pos = (mv_y-dy)/4 + (pBlockInfo->mbY*16);

	maxold_x = frame_width-1;
	maxold_y = frame_height-1;

	if (dx == 0 && dy == 0) /* fullpel position */
	{  
		for (j = 0; j < MB_BLOCK_SIZE; j++)
			for (i = 0; i < MB_BLOCK_SIZE; i++)
			{
				vert_offs     = MAX(0,MIN(maxold_y,y_pos+j));//padding
				hor_offs      = MAX(0,MIN(maxold_x,x_pos+i));//padding
				ref_lum[j][i] = pEncFrame->pRefFrameData[vert_offs*frame_width + hor_offs];			
			}		
	}
	else /* other positions */
	{ 
		if (dy == 0) 
		{ /* No vertical interpolation */
			
			for (j = 0; j < MB_BLOCK_SIZE; j++) 
			{
				for (i = 0; i < MB_BLOCK_SIZE; i++) 
				{
					for (result = 0, x = -2; x < 4; x++)
					{
						vert_offs = MAX(0,MIN(maxold_y,y_pos+j));//padding
						hor_offs  = MAX(0,MIN(maxold_x,x_pos+i+x));//padding
						result   += pEncFrame->pRefFrameData[vert_offs*frame_width + hor_offs]*COEF[x+2];
					}
					ref_lum[j][i] = MAX(0, MIN(255, (result+16)/32));//(8-247). pos b
				}
			}

			if ((dx&1) == 1) //qp pos
			{
				for (j = 0; j < MB_BLOCK_SIZE; j++)
				{
					for (i = 0; i < MB_BLOCK_SIZE; i++)
					{
						vert_offs = MAX(0,MIN(maxold_y,y_pos+j));//padding
						hor_offs  = MAX(0,MIN(maxold_x,x_pos+i+dx/2));//padding
						ref_lum[j][i] = (ref_lum[j][i] + pEncFrame->pRefFrameData[vert_offs*frame_width + hor_offs] +1 )/2;//(8-254). pos a, c
					}
				}
			}
		}
		else if (dx == 0) 
		{  /* No horizontal interpolation */

			for (j = 0; j < MB_BLOCK_SIZE; j++) 
			{
				for (i = 0; i < MB_BLOCK_SIZE; i++) 
				{
					for (result = 0, y = -2; y < 4; y++)
					{						
						vert_offs = MAX(0,MIN(maxold_y,y_pos+j+y));//padding
						hor_offs  = MAX(0,MIN(maxold_x,x_pos+i));//padding
						result   += pEncFrame->pRefFrameData[vert_offs*frame_width + hor_offs]*COEF[y+2];//pos h
					}
					ref_lum[j][i] = MAX(0, MIN(255, (result+16)/32));
				}
			}

			if ((dy&1) == 1) 
			{
				for (j = 0; j < MB_BLOCK_SIZE; j++)
					for (i = 0; i < MB_BLOCK_SIZE; i++)
					{
						vert_offs = MAX(0,MIN(maxold_y,y_pos+j+dy/2));//padding
						hor_offs  = MAX(0,MIN(maxold_x,x_pos+i));//padding
						ref_lum[j][i] = (ref_lum[j][i] + pEncFrame->pRefFrameData[vert_offs*frame_width + hor_offs] +1 )/2;//pos d, n
					}
			}
		}
		else if (dx == 2) 
		{  /* Vertical & horizontal interpolation */

			for (j = -2; j < MB_BLOCK_SIZE+3; j++) 
			{
				for (i = 0; i < MB_BLOCK_SIZE; i++)
					for (tmp_res[j+2][i] = 0, x = -2; x < 4; x++)
					{
						vert_offs = MAX(0,MIN(maxold_y,y_pos+j));//padding
						hor_offs  = MAX(0,MIN(maxold_x,x_pos+i+x));//padding
						tmp_res[j+2][i] += pEncFrame->pRefFrameData[vert_offs*frame_width + hor_offs]*COEF[x+2];//j1 = aa � 5 * bb + 20 * b1 + 20 * s1 � 5 * gg + hh (8-250)
					}
			}

			for (j = 0; j < MB_BLOCK_SIZE; j++) 
			{
				for (i = 0; i < MB_BLOCK_SIZE; i++) 
				{
					for (result = 0, y = -2; y < 4; y++)
						result += tmp_res[j+y+2][i]*COEF[y+2];
					ref_lum[j][i] = MAX(0, MIN(255, (result+512)/1024));//pos j = Clip1Y( ( j1 + 512 ) >> 10 ) (8-251)
				} 
			}

			if ((dy&1) == 1)
			{
				for (j = 0; j < MB_BLOCK_SIZE; j++)
					for (i = 0; i < MB_BLOCK_SIZE; i++)
						ref_lum[j][i] = (ref_lum[j][i] + MAX(0, MIN(255, (tmp_res[j+2+dy/2][i]+16)/32)) +1 )/2;//pos f, q
			}
		}
		else if (dy == 2)
		{  /* Horizontal & vertical interpolation */

			for (j = 0; j < MB_BLOCK_SIZE; j++)
			{
				for (i = -2; i < MB_BLOCK_SIZE+3; i++)
					for (tmp_res[j][i+2] = 0, y = -2; y < 4; y++)
					{
						vert_offs = MAX(0,MIN(maxold_y,y_pos+j+y));//padding
						hor_offs  = MAX(0,MIN(maxold_x,x_pos+i));//padding
						tmp_res[j][i+2] += pEncFrame->pRefFrameData[vert_offs*frame_width + hor_offs]*COEF[y+2];//j1 = cc � 5 * dd + 20 * h1 + 20 * m1 � 5 * ee + ff, (8-249)
					}
			}

			for (j = 0; j < MB_BLOCK_SIZE; j++)
			{
				for (i = 0; i < MB_BLOCK_SIZE; i++)
				{
					for (result = 0, x = -2; x < 4; x++)
						result += tmp_res[j][i+x+2]*COEF[x+2];
					ref_lum[j][i] = MAX(0, MIN(255, (result+512)/1024));//j = Clip1Y( ( j1 + 512 ) >> 10 ) (8-251)
				}
			}

			if ((dx&1) == 1)
			{
				for (j = 0; j < MB_BLOCK_SIZE; j++)
					for (i = 0; i < MB_BLOCK_SIZE; i++)
						ref_lum[j][i] = (ref_lum[j][i] + MAX(0, MIN(255, (tmp_res[j][i+2+dx/2]+16)/32))+1)/2;//pos i, k
			}
		}
		else
		{  /* Diagonal interpolation pos e, g, p, r*/

			for (j = 0; j < MB_BLOCK_SIZE; j++)
			{
				for (i = 0; i < MB_BLOCK_SIZE; i++)
				{
					pres_y = dy == 1 ? y_pos+j : y_pos+j+1;//dy == 1 or dy == 2
					pres_y = MAX(0,MIN(maxold_y,pres_y));
					for (result = 0, x = -2; x < 4; x++)
					{
						vert_offs = pres_y;//padding
						hor_offs  = MAX(0,MIN(maxold_x,x_pos+i+x));//padding
						result += pEncFrame->pRefFrameData[vert_offs*frame_width + hor_offs]*COEF[x+2];
					}
					ref_lum[j][i] = MAX(0, MIN(255, (result+16)/32));//pos b, s
				}
			}

			for (j = 0; j < MB_BLOCK_SIZE; j++)
			{
				for (i = 0; i < MB_BLOCK_SIZE; i++)
				{
					pres_x = dx == 1 ? x_pos+i : x_pos+i+1;
					pres_x = MAX(0,MIN(maxold_x,pres_x));
					for (result = 0, y = -2; y < 4; y++)
					{
						vert_offs = MAX(0,MIN(maxold_y,y_pos+j+y));//padding
						hor_offs  = pres_x;//padding
						result += pEncFrame->pRefFrameData[vert_offs*frame_width + hor_offs]*COEF[y+2];//pos h, m without rounding (MIN(255, (result+16)/32)))
					}
					ref_lum[j][i] = (ref_lum[j][i] + MAX(0, MIN(255, (result+16)/32)) +1 ) / 2;//pos e, g, p, r
				}
			}

		}
	}
}

void get_mb_sad(UInt8 pp_currentData[16][16], UInt8 **pp_ref_lum, UInt32 *mb_sad_curr)
{
	UInt8 x, y;
	long long tmp1=0;
	long long tmp2=0;
	long long tmp3=0;

	for(y = 0; y< 16; y++)
	{
		for(x = 0; x< 16; x++)
		{
			*mb_sad_curr += abs((Int32)pp_currentData[y][x] - (Int32)pp_ref_lum[y][x]);
		}
	}
	
	//printf("sad: %d\n", *mb_sad_curr);
}


void mv_prediction(pBlockInfo_t pBlockInfo)
{
	Int16    pred_v, pred_h;             /*predictors*/ 
	vector_t predicted_vector;
	vector_t vector_zero;
	vector_t predictorA, predictorB, predictorC;

	//debug stuff
#if PRINT_MV_PREDS
	//A
	FPRINTF_MC(fileDebug, "%d\n",pBlockInfo->pMvMemory[pBlockInfo->mbNumb - pBlockInfo->frame_mb_width].mv.hor);
	//FPRINTF_MC(fileDebug, "%d\n",pBlockInfo->pMvMemory[pBlockInfo->mbNumb - pBlockInfo->frame_mb_width].mv.ver);
	//B
	//FPRINTF_MC(fileDebug, "%d\n",pBlockInfo->pMvMemory[pBlockInfo->mbNumb - pBlockInfo->frame_mb_width + 1].mv.hor);
	//FPRINTF_MC(fileDebug, "%d\n",pBlockInfo->pMvMemory[pBlockInfo->mbNumb - pBlockInfo->frame_mb_width + 1].mv.ver);
	//C
	//FPRINTF_MC(fileDebug, "%d\n",pBlockInfo->pMvMemory[pBlockInfo->mbNumb - 1].mv.hor);
	//FPRINTF_MC(fileDebug, "%d\n",pBlockInfo->pMvMemory[pBlockInfo->mbNumb - 1].mv.ver);
	//D
	//FPRINTF_MC(fileDebug, "%d\n",pBlockInfo->pMvMemory[pBlockInfo->mbNumb - pBlockInfo->frame_mb_width - 1].mv.hor);
	//FPRINTF_MC(fileDebug, "%d\n",pBlockInfo->pMvMemory[pBlockInfo->mbNumb - pBlockInfo->frame_mb_width - 1].mv.ver);
#endif

	//init
	vector_zero.hor = 0;
	vector_zero.ver = 0;	
	predicted_vector.hor = vector_zero.hor;
	predicted_vector.ver = vector_zero.ver;

	if(pBlockInfo->mbX == 1 && pBlockInfo->mbY == 1)
		printf("");


	if(pBlockInfo->mbX == 0 && pBlockInfo->mbY == 0)//first MB
	{
		predictorA = vector_zero;
		predictorB = vector_zero;
		predictorC = vector_zero;
	}
	else if(pBlockInfo->mbX == 0)//left border
	{
		predictorA = vector_zero;
		predictorB = pBlockInfo->pMvMemory[pBlockInfo->mbNumb - pBlockInfo->frame_mb_width].mv;
		predictorC = pBlockInfo->pMvMemory[pBlockInfo->mbNumb - pBlockInfo->frame_mb_width + 1].mv;
	}
	else if(pBlockInfo->mbY == 0)//top border
	{
		predictorA = pBlockInfo->pMvMemory[pBlockInfo->mbNumb - 1].mv;
		predictorB = predictorA;//8-204
		predictorC = predictorA;//8-205
	}
	else if(pBlockInfo->mbX == pBlockInfo->frame_mb_width-1)//right border
	{
		predictorA = pBlockInfo->pMvMemory[pBlockInfo->mbNumb - 1].mv;
		predictorB = pBlockInfo->pMvMemory[pBlockInfo->mbNumb - pBlockInfo->frame_mb_width].mv;
		predictorC = pBlockInfo->pMvMemory[pBlockInfo->mbNumb - pBlockInfo->frame_mb_width - 1].mv;//up left. special condition. 8-211
	}
	else
	{
		predictorA = pBlockInfo->pMvMemory[pBlockInfo->mbNumb - 1].mv;			
		predictorB = pBlockInfo->pMvMemory[pBlockInfo->mbNumb - pBlockInfo->frame_mb_width].mv;			
		predictorC = pBlockInfo->pMvMemory[pBlockInfo->mbNumb - pBlockInfo->frame_mb_width + 1].mv;
	}

	// calculate predictor from A, B and C predictor candidates
	pred_h = MEDIAN(predictorA.hor, predictorB.hor, predictorC.hor);
	pred_v = MEDIAN(predictorA.ver, predictorB.ver, predictorC.ver);
	
	predicted_vector.hor = pred_h;
	predicted_vector.ver = pred_v;

#if MV_PRED_RESULTS
		//FPRINTF_MC(fileDebug, "%d\n",predicted_vector.hor);
		//FPRINTF_MC(fileDebug, "%d\n",predicted_vector.ver);
		//FPRINTF_MC(fileDebug, "%d\n",hybrid_mode.value);
#endif

	pBlockInfo->pMvMemory[pBlockInfo->mbNumb].pred_mv = predicted_vector;
}

