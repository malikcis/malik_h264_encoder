/*****************************************************************************  
 *  
 *  H264 AVC CODEC  
 *  
 *  Copyright(C) 2020 llcc <malik.cisse@abateck.com>  
 *  
 *  This program is free software ; you can redistribute it and/or modify  
 *  it under the terms of the GNU General Public License as published by  
 *  the Free Software Foundation ; either version 2 of the License, or  
 *  (at your option) any later version.  
 *  
 *  This program is distributed in the hope that it will be useful,  
 *  but WITHOUT ANY WARRANTY ; without even the implied warranty of  
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the  
 *  GNU General Public License for more details.  
 *  
 *  You should have received a copy of the GNU General Public License  
 *  along with this program ; if not, write to the Free Software  
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA  
 *  
 ****************************************************************************/ 
#include "me.h"
#include "mem_control.h"
#include "me_algo.h"
#include <math.h>

#define USE_REFINEMENT 0 //activate half pel and quarter pel refinement
#define SECURITY 2 

#ifndef MIN
#define  MIN(a,b)       (((a) < (b)) ? (a) : (b))
#endif

UInt16 clipp_table_x[4]   = {63, 127, 511, 511};  //full pel. for symetrie real range max-1 taken
UInt16 clipp_table_y[4]   = {31, 63, 127, 255};    //full pel. for symetrie real range max-1 taken

Int16 rnd[4] ={0, -1, -2, 1 };//round to next full pel position

//this is a simplified motion estimation where previously computed vector at top position is reused as new search center. We do a +/-4 full pixel search arround this center. Also colocated position is checked.
void motion_estimation(pBlockInfo_t pBlockInfo, pEncFrame_t pEncFrame, vector_t *best_match, vector_t *hp_mv, vector_t *fp_mv)
{
	Int16 cand_x, cand_y;
	UInt8 pp_currentData[16][16];
	UInt8 incr; 
	UInt32 mb_sad_curr;	
	UInt32 mb_sad_prev;
	UInt8 **pp_ref_lum;    //[16][16]
	vector_t search_center;      //gavity center of the search window
	vector_t zero_mv;        
	Int16 x, y;
	UInt8 search_width;    //reduced search window
	UInt8 search_height;   //reduced search window
#if USE_REFINEMENT
	UInt8 window;
	Int16 fp_bm_x, fp_bm_y;
#endif

	//init
	zero_mv.hor    = zero_mv.ver = 0;
	zero_mv.mbType = INTER;
	search_center        = zero_mv;
	search_width   = 16;
	search_height  = 16;

	for(y = 0; y< 8; y++)//get current data in a 16x16 buffer for simplicity
	{
		for(x = 0; x< 8; x++)
		{
			//luma
			pp_currentData[y][x]     = pBlockInfo->block8x8Buff.ppp_currentData[0][y][x]; 
			pp_currentData[y][x+8]   = pBlockInfo->block8x8Buff.ppp_currentData[1][y][x]; 
			pp_currentData[y+8][x]   = pBlockInfo->block8x8Buff.ppp_currentData[2][y][x]; 
			pp_currentData[y+8][x+8] = pBlockInfo->block8x8Buff.ppp_currentData[3][y][x]; 
		}
	}

	/******************************************get top macroblock's motion vector ******************************************/
	if(pBlockInfo->mbY == 0)	
		search_center  = zero_mv;// first line MB : no candidate	
	else	        
		search_center  = pEncFrame->pMvMemory[pBlockInfo->mbNumb - pBlockInfo->frame_mb_width].fp_mv;//// spatial top  candidate

	//clipp the search_center to be within the search range	
	adjust_motion_vector(pBlockInfo, &search_center);

	//convert back to QP
	search_center.hor = (search_center.hor * 4);
	search_center.ver = (search_center.ver * 4);

	//loop over all full pel motion search
	incr = 4;//ful pel
	pp_ref_lum = pBlockInfo->block8x8Buff.pp_ref_lum;
	mb_sad_prev = 0xFFFFFFFF;
	for(y = -search_height; y<= search_height; y +=incr)//full search reduced search window
	{
		for(x = -search_width; x<= search_width; x +=incr)
		{
			cand_x = search_center.hor + x;
			cand_y = search_center.ver + y;
			get_ref_luma_block(pBlockInfo, pEncFrame, cand_x, cand_y, pp_ref_lum);//get reference			
			mb_sad_curr = 0;
			get_mb_sad(pp_currentData, pp_ref_lum, &mb_sad_curr);

			if(mb_sad_curr <= mb_sad_prev)
			{
				best_match->hor = cand_x;
				best_match->ver = cand_y;
				mb_sad_prev = mb_sad_curr;
			}
		}
	}

    //check 0,0 position
	get_ref_luma_block(pBlockInfo, pEncFrame, 0, 0, pp_ref_lum);//get reference
	mb_sad_curr = 0;	
	get_mb_sad(pp_currentData, pp_ref_lum, &mb_sad_curr);
	if(mb_sad_curr <= mb_sad_prev)
	{
		best_match->hor = 0;
		best_match->ver = 0;
		mb_sad_prev = mb_sad_curr;
	}

	fp_mv->hor = best_match->hor/4;
	fp_mv->ver = best_match->ver/4;	
	
#if USE_REFINEMENT //half pel refinement 3. Step search (TSS)
	fp_bm_x = best_match->hor;//full pel best match
	fp_bm_y = best_match->ver;//full pel best match
	mb_sad_prev = 0xFFFFFFFF;	
	//refine hp candidate
	window = 2;//hp
	incr = 2;//hp
	for(y = fp_bm_y - window; y<= fp_bm_y + window; y +=incr)//full search on 1/4 or 1/2 pix pos
	{
		for(x = fp_bm_x - window; x<= fp_bm_x + window; x +=incr)
		{
			cand_x = x;
			cand_y = y;
			get_ref_luma_block(pBlockInfo, pEncFrame, cand_x, cand_y, pp_ref_lum);//get interpolated reference
			mb_sad_curr = 0;
			get_mb_sad(pp_currentData, pp_ref_lum, &mb_sad_curr);												
			if(mb_sad_curr <= mb_sad_prev)
			{
				best_match->hor = cand_x;
				best_match->ver = cand_y;
				mb_sad_prev = mb_sad_curr;
			}			
		}
	}
#endif

	hp_mv->hor = best_match->hor;
	hp_mv->ver = best_match->ver;

#if USE_REFINEMENT //quarter pel refinement 3. Step search (TSS)
	fp_bm_x = best_match->hor;//half pel best match
	fp_bm_y = best_match->ver;//half pel best match
	mb_sad_prev = 0xFFFFFFFF;	
	//refine hp candidate
	window = 1;//qp
	incr = 1;//qp
	for(y = fp_bm_y - window; y<= fp_bm_y + window; y +=incr)//full search on 1/4 or 1/2 pix pos
	{
		for(x = fp_bm_x - window; x<= fp_bm_x + window; x +=incr)
		{
			cand_x = x;
			cand_y = y;
			get_ref_luma_block(pBlockInfo, pEncFrame, cand_x, cand_y, pp_ref_lum);//get interpolated reference
			mb_sad_curr = 0;
			get_mb_sad(pp_currentData, pp_ref_lum, &mb_sad_curr);
			if(mb_sad_curr <= mb_sad_prev)
			{
				best_match->hor = cand_x;
				best_match->ver = cand_y;
				mb_sad_prev = mb_sad_curr;
			}
		}
	}
#endif

	pBlockInfo->skip = 0;
}

void adjust_motion_vector(pBlockInfo_t pBlockInfo, vector_t *search_center)
{
	//mvRange 0       : [64x32]
	//mvRange 1       : [128x64]
	//mvRange 2       : [512x128]
	//mvRange 3       : [1024x256]
	Int16 gp_x, gp_y;
	Int32 frame_mb_height, frame_mb_width;
	UInt32 mb_x, mb_y;
	UInt32 mb_x_offs, mb_y_offs;
	UInt16 mvRange_x, mvRange_y;
	UInt16 security = SECURITY;// at least 7 pixels remein in image. ~1% loss in bitrate saving for 160x64

	//copy data
	frame_mb_height = pBlockInfo->frame_mb_height * 16;
	frame_mb_width  = pBlockInfo->frame_mb_width * 16;
	mb_x			= pBlockInfo->mbX * 16;
	mb_y			= pBlockInfo->mbY * 16;

	gp_x = search_center->hor;
	gp_y = search_center->ver;

	//processing done to obtain harware bit trueness
	gp_x -= 4;//shift to top left 24x24 search window. Not used in SW
	gp_y -= 4;

	//clip according to picture border to avoid longer vectors than padding
	mb_x_offs = frame_mb_width - mb_x;
	mb_y_offs = frame_mb_height - mb_y;

	if(gp_x < 0)
		gp_x = -1 * (Int16)MIN(mb_x + 15 - security, abs(gp_x));      																																						 
	else
		gp_x = (Int16)MIN(mb_x_offs - 9 - security, gp_x);

	if(gp_y < 0)
		gp_y = -1 * (Int16)MIN(mb_y + 15 - security, abs(gp_y));      																																						 
	else
		gp_y = (Int16)MIN(mb_y_offs - 9 - security, gp_y);


	//clip according to motion vector range	
	mvRange_x = clipp_table_x[pBlockInfo->mvRange] - 8; // -8 is to compensate for search center -4,+4 search area
	mvRange_y = clipp_table_y[pBlockInfo->mvRange] - 8; // -8 is to compensate for search center -4,+4 search area
	if(gp_x > mvRange_x)
	{
		gp_x = mvRange_x;
	}
	if(gp_y > mvRange_y)
	{
		gp_y = mvRange_y;
	}
	if(gp_x < -1*mvRange_x)
	{
		gp_x = -1*mvRange_x;
	}
	if(gp_y < -1*mvRange_y)
	{
		gp_y = -1*mvRange_y;
	}
		
	//write back result
	search_center->hor = (gp_x + 4);//shift to center of 24x24 search pos and convert to quarterpal
	search_center->ver = (gp_y + 4);
}