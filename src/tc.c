/*****************************************************************************  
 *  
 *  H264 AVC CODEC  
 *  
 *  Copyright(C) 2020 llcc <malik.cisse@abateck.com>  
 *  
 *  This program is free software ; you can redistribute it and/or modify  
 *  it under the terms of the GNU General Public License as published by  
 *  the Free Software Foundation ; either version 2 of the License, or  
 *  (at your option) any later version.  
 *  
 *  This program is distributed in the hope that it will be useful,  
 *  but WITHOUT ANY WARRANTY ; without even the implied warranty of  
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the  
 *  GNU General Public License for more details.  
 *  
 *  You should have received a copy of the GNU General Public License  
 *  along with this program ; if not, write to the Free Software  
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA  
 *  
 ****************************************************************************/ 
#include "tc.h"

#define DBG_RECONSTRUCTED 0
#define DBG_RESIDUAL      0
#define DBG_BSG_DATA      0
#define DBG_QUANT2X2DC    0
#define DBG_QUANT4X4DC    0
#define DBG_QUANT4X4AC    0
#define GLOBAL_DEBUG	  0

//////////////////////////////////////////////////////
// static var
Int16 quant[6][ 4 * 4] =
{
    13107, 8066, 13107, 8066, 8066, 5243, 8066, 5243, 13107, 8066, 13107, 8066, 8066, 5243, 8066, 5243,
    11916, 7490, 11916, 7490, 7490, 4660, 7490, 4660, 11916, 7490, 11916, 7490, 7490, 4660, 7490, 4660,
    10082, 6554, 10082, 6554, 6554, 4194, 6554, 4194, 10082, 6554, 10082, 6554, 6554, 4194, 6554, 4194,
     9362, 5825,  9362, 5825, 5825, 3647, 5825, 3647, 9362,  5825,  9362, 5825, 5825, 3647, 5825, 3647,
     8192, 5243,  8192, 5243, 5243, 3355, 5243, 3355, 8192,  5243,  8192, 5243, 5243, 3355, 5243, 3355,
     7282, 4559,  7282, 4559, 4559, 2893, 4559, 2893, 7282,  4559,  7282, 4559, 4559, 2893, 4559, 2893
};

Int16 dequant[6][ 4 * 4] =
{
    10, 13, 10, 13, 13, 16, 13, 16, 10, 13, 10, 13, 13, 16, 13, 16,
    11, 14, 11, 14, 14, 18, 14, 18, 11, 14, 11, 14, 14, 18, 14, 18,
    13, 16, 13, 16, 16, 20, 16, 20, 13, 16, 13, 16, 16, 20, 16, 20,
    14, 18, 14, 18, 18, 23, 18, 23, 14, 18, 14, 18, 18, 23, 18, 23,
    16, 20, 16, 20, 20, 25, 20, 25, 16, 20, 16, 20, 20, 25, 20, 25,
    18, 23, 18, 23, 23, 29, 23, 29, 18, 23, 18, 23, 23, 29, 23, 29
};

void scan_zig_4x4(Int16* zig, Int16* dct)
{
    zig[0]  = dct[0];
    zig[1]  = dct[1];
    zig[2]  = dct[4];
    zig[3]  = dct[8];
    zig[4]  = dct[5];
    zig[5]  = dct[2];
    zig[6]  = dct[3];
    zig[7]  = dct[6];
    zig[8]  = dct[9];
    zig[9]  = dct[12];
    zig[10] = dct[13];
    zig[11] = dct[10];
    zig[12] = dct[7];
    zig[13] = dct[11];
    zig[14] = dct[14];
    zig[15] = dct[15];
}

/* (ref: JVT-B118)-> I.3	Elimination of single coefficients in inter macroblocks
 * x264_mb_decimate_score: given dct coeffs it returns a score to see if we could empty this dct coeffs
 * to 0 (low score means set it to null)
 * Used in inter macroblock (luma and chroma)
 *  luma: for a 8x8 block: if score < 4 -> null
 *        for the complete mb: if score < 6 -> null
 *  chroma: for the complete mb: if score < 7 -> null
 */
int mb_decimate_score( Int16 *dct)
{
	int i_max = 16;
    static const int i_ds_table4[16] = {
        3,2,2,1,1,1,0,0,0,0,0,0,0,0,0,0 };

    const int *ds_table = i_ds_table4;
    int i_score = 0;
    int idx = i_max - 1;

    while( idx >= 0 && dct[idx] == 0 )
        idx--;

    while( idx >= 0 )
    {
        int i_run;

		if( abs(dct[idx--] ) > 1 )
            return 255;

        i_run = 0;
        while( idx >= 0 && dct[idx] == 0 )
        {
            idx--;
            i_run++;
        }
        i_score += ds_table[i_run];
    }

    return i_score;
}

void mb_texture_codec(pBlockInfo_t pBlockInfo)//mb_encoding
{	
	Int8 blk4x4cnt; 
	Int16 lumaDCcoeffs[16];
	Int16 lumaDCcoeffs_tmp[16];
	Int16 chromaDCcoeffsU[4];
	Int16 chromaDCcoeffsV[4];
	Int16 *chromaDCcoeffs;
	Int16 lumaDCcoeffsCnt = 0;
	int i, j;
	static Int32 MB_buffer_prev_y[16][16];
	static Int32 MB_buffer_prev_c[8][16];

	//printf("\nMB: x=%d, y=%d\n", pBlockInfo->mbX, pBlockInfo->mbY);

	Int32 is_intra = (Int32)(pBlockInfo->mbType == INTRA);

	if(pBlockInfo->mbType == INTRA)	
	{
		getIntraPredictorY(pBlockInfo);
		getIntraPredictorU(pBlockInfo);
		getIntraPredictorV(pBlockInfo);
	}
	else
	{
		printf("");
	}

	get_chroma_skip(pBlockInfo);

#if DBG_RESIDUAL
	//residual
	for (pBlockInfo->blockIdx = 0; pBlockInfo->blockIdx < 6; pBlockInfo->blockIdx++)
	{
#if GLOBAL_DEBUG
		//if(pBlockInfo->mbType != INTRA)
		FPRINTF_MC(pBlockInfo->pEncFrame->dbg_files.res, "#FR: %d, Type: %d MB(%d, %d) Block %d\n",pBlockInfo->frameCntr, pBlockInfo->frameType, pBlockInfo->mbX, pBlockInfo->mbY, pBlockInfo->blockIdx);

		if( pBlockInfo->mbX % 2 == 0 )
		{
			FPRINTF_MC(pBlockInfo->pEncFrame->dbg_files.res_a, "#FR: %d, Type: %d MB(%d, %d) Block %d\n",pBlockInfo->frameCntr, pBlockInfo->frameType, pBlockInfo->mbX, pBlockInfo->mbY, pBlockInfo->blockIdx);
		}

		if( pBlockInfo->mbX % 2 == 1 )
		{
			FPRINTF_MC(pBlockInfo->pEncFrame->dbg_files.res_b, "#FR: %d, Type: %d MB(%d, %d) Block %d\n",pBlockInfo->frameCntr, pBlockInfo->frameType, pBlockInfo->mbX, pBlockInfo->mbY, pBlockInfo->blockIdx);
		}
#endif

		// step 1: perform motion compensation. Get difference pixel data (non intra)
		get_residual(pBlockInfo, 1);//tc_mcnr_fn(pmem, pblockinfo);//get residual

		for (y=0; y<8; y++ ) 
		{
			for (x=0; x<8; x++ ) 
			{
				FPRINTF_MC(pBlockInfo->pEncFrame->dbg_files.tc_din,"%d\n", pBlockInfo->block8x8Buff.dctInp[pBlockInfo->blockIdx][y][x]);	    
			}
		}		

		//unpack 8x8 into 4-4x4
		get_4x4_blocks(pBlockInfo);
	}
	print_yuv_in_hw_order(pBlockInfo);
#endif

	//######################## loop over 4-luma 8x8 blocks #############################
	for (pBlockInfo->blockIdx = 0; pBlockInfo->blockIdx < 4; pBlockInfo->blockIdx++)
	{

		// step 1: perform motion compensation. Get difference pixel data (non intra)
		get_residual(pBlockInfo, 0);//tc_mcnr_fn(pmem, pblockinfo);//get residual

		if(pBlockInfo->mbType == INTRA)	
			intraPred(pBlockInfo);
		
		//unpack 8x8 into 4-4x4
		get_4x4_blocks(pBlockInfo);

		//block FW DCT
		for(blk4x4cnt= 0; blk4x4cnt<4; blk4x4cnt++)
		{

			dct4x4_c(pBlockInfo->block8x8Buff.dct4x4inp[(pBlockInfo->blockIdx * 4) + blk4x4cnt]);

			if(pBlockInfo->mbType == INTRA)	
			{
				//get Luma DC coeff
				lumaDCcoeffs[lumaDCcoeffsCnt++] = pBlockInfo->block8x8Buff.dct4x4inp[(pBlockInfo->blockIdx * 4) + blk4x4cnt][0];
			}
			//quant4x4
			quant4x4_c(pBlockInfo->block8x8Buff.dct4x4inp[(pBlockInfo->blockIdx * 4) + blk4x4cnt], pBlockInfo->mquantY, is_intra);
						
		}		
	}	

	if(pBlockInfo->mbType == INTRA)	
	{
		//the following is necessary, because of 8x8 (VC-1 like) block organisation
		//0  1  4  5 
		//1  2  6  7 
		//8  9  12 13
		//10 11 14 15
		//to
		//0  1  2  3 
		//4  5  6  7 
		//8  9  10 11
		//12 13 14 15
		//conversion

		//luma
		//organize in normal 4x4 order within MB
		lumaDCcoeffs_tmp[0] = lumaDCcoeffs[0];
		lumaDCcoeffs_tmp[1] = lumaDCcoeffs[1];
		lumaDCcoeffs_tmp[2] = lumaDCcoeffs[4];
		lumaDCcoeffs_tmp[3] = lumaDCcoeffs[5];
		lumaDCcoeffs_tmp[4] = lumaDCcoeffs[2];
		lumaDCcoeffs_tmp[5] = lumaDCcoeffs[3];
		lumaDCcoeffs_tmp[6] = lumaDCcoeffs[6];
		lumaDCcoeffs_tmp[7] = lumaDCcoeffs[7];
		lumaDCcoeffs_tmp[8] = lumaDCcoeffs[8];
		lumaDCcoeffs_tmp[9] = lumaDCcoeffs[9];
		lumaDCcoeffs_tmp[10] = lumaDCcoeffs[12];
		lumaDCcoeffs_tmp[11] = lumaDCcoeffs[13];
		lumaDCcoeffs_tmp[12] = lumaDCcoeffs[10];
		lumaDCcoeffs_tmp[13] = lumaDCcoeffs[11];
		lumaDCcoeffs_tmp[14] = lumaDCcoeffs[14];
		lumaDCcoeffs_tmp[15] = lumaDCcoeffs[15];
	}
	
	if(pBlockInfo->mbType == INTRA)	
	{
		//do 4x4 Luma DC transform (hadamard)
		dct4x4dc_c(lumaDCcoeffs_tmp);
	}

	if(pBlockInfo->mbType == INTRA)	
	{
		//quant4x4dc
		quant4x4dc_c(lumaDCcoeffs_tmp, pBlockInfo->mquantY);

#if DBG_QUANT4X4DC //see mb buffer vhdl for hw order						
		FPRINTF_MC(pBlockInfo->pEncFrame->dbg_files.quant4x4dc, "%d\n",lumaDCcoeffs_tmp[0]);		
		FPRINTF_MC(pBlockInfo->pEncFrame->dbg_files.quant4x4dc, "%d\n",lumaDCcoeffs_tmp[4]);		
		FPRINTF_MC(pBlockInfo->pEncFrame->dbg_files.quant4x4dc, "%d\n",lumaDCcoeffs_tmp[8]);		
		FPRINTF_MC(pBlockInfo->pEncFrame->dbg_files.quant4x4dc, "%d\n",lumaDCcoeffs_tmp[12]);		
		FPRINTF_MC(pBlockInfo->pEncFrame->dbg_files.quant4x4dc, "%d\n",lumaDCcoeffs_tmp[1]);		
		FPRINTF_MC(pBlockInfo->pEncFrame->dbg_files.quant4x4dc, "%d\n",lumaDCcoeffs_tmp[5]);		
		FPRINTF_MC(pBlockInfo->pEncFrame->dbg_files.quant4x4dc, "%d\n",lumaDCcoeffs_tmp[9]);		
		FPRINTF_MC(pBlockInfo->pEncFrame->dbg_files.quant4x4dc, "%d\n",lumaDCcoeffs_tmp[13]);		
		FPRINTF_MC(pBlockInfo->pEncFrame->dbg_files.quant4x4dc, "%d\n",lumaDCcoeffs_tmp[2]);		
		FPRINTF_MC(pBlockInfo->pEncFrame->dbg_files.quant4x4dc, "%d\n",lumaDCcoeffs_tmp[6]);		
		FPRINTF_MC(pBlockInfo->pEncFrame->dbg_files.quant4x4dc, "%d\n",lumaDCcoeffs_tmp[10]);		
		FPRINTF_MC(pBlockInfo->pEncFrame->dbg_files.quant4x4dc, "%d\n",lumaDCcoeffs_tmp[14]);		
		FPRINTF_MC(pBlockInfo->pEncFrame->dbg_files.quant4x4dc, "%d\n",lumaDCcoeffs_tmp[3]);		
		FPRINTF_MC(pBlockInfo->pEncFrame->dbg_files.quant4x4dc, "%d\n",lumaDCcoeffs_tmp[7]);		
		FPRINTF_MC(pBlockInfo->pEncFrame->dbg_files.quant4x4dc, "%d\n",lumaDCcoeffs_tmp[11]);		
		FPRINTF_MC(pBlockInfo->pEncFrame->dbg_files.quant4x4dc, "%d\n",lumaDCcoeffs_tmp[15]);		
#endif
	}

	//######################## loop over 2-chroma 8x8 blocks #############################
	for (pBlockInfo->blockIdx = 4; pBlockInfo->blockIdx < 6; pBlockInfo->blockIdx++)
	{

		if(pBlockInfo->blockIdx == 4)
			chromaDCcoeffs = chromaDCcoeffsU;
		else
			chromaDCcoeffs = chromaDCcoeffsV;

		// perform motion compensation. Get difference pixel data (non intra)
		get_residual(pBlockInfo, 0);//get residual

		if(pBlockInfo->mbType == INTRA)	
			intraPred(pBlockInfo);
		
		//convert 8x8 blocks into 4-4x4 blocks
		get_4x4_blocks(pBlockInfo);

		//block FW DCT
		for(blk4x4cnt= 0; blk4x4cnt<4; blk4x4cnt++)
		{
			Int32 is_intra;

			dct4x4_c(pBlockInfo->block8x8Buff.dct4x4inp[(pBlockInfo->blockIdx * 4) + blk4x4cnt]);
			
			//get Chroma DC coeff
			chromaDCcoeffs[blk4x4cnt] = pBlockInfo->block8x8Buff.dct4x4inp[(pBlockInfo->blockIdx * 4) + blk4x4cnt][0];
	
			//quant4x4
			is_intra = (Int32)(pBlockInfo->mbType == INTRA);
			quant4x4_c(pBlockInfo->block8x8Buff.dct4x4inp[(pBlockInfo->blockIdx * 4) + blk4x4cnt], pBlockInfo->mquantC, is_intra);
		}			

		//do 2x2 chroma DC transform (hadamard)
		dct2x2dc_c(chromaDCcoeffs);		

		//quant2x2dc
		quant2x2dc_c(chromaDCcoeffs, pBlockInfo->mquantC, is_intra);		

#if DBG_QUANT2X2DC
		for(i= 0; i<4; i++)
		{				
			FPRINTF_MC(pBlockInfo->pEncFrame->dbg_files.quant2x2dc, "%d\n",chromaDCcoeffs[i]);
		}	
#endif
	}
		
	if(pBlockInfo->mbType == INTRA)
	{
		//Put AC data in BSG buffer
		for(i= 0; i<24; i++)
		{		
			scan_zig_4x4(pBlockInfo->block8x8Buff.dct4x4out[i], pBlockInfo->block8x8Buff.dct4x4inp[i]);
		}
	}
	else //P-MB coefficient decimation
	{
		int b_decimate = 0;//use decimation

		//orig values
		//int cost_th_8x8_y = 4;
		//int cost_th_mb = 6;
		//int cost_th_8x8_c = 7;

		int cost_th_8x8_y = 4;
		int cost_th_mb = 6;
		int cost_th_8x8_c = 7;

		//init
		int i_decimate_mb = 0;
		int i_decimate_8x8 = 0;
		int i_decimate_score = 0;

		//############################## end luma #############################
		//8x8 block 0
		i_decimate_8x8 = 0;
		for(i= 0; i<4; i++)
		{		
			scan_zig_4x4(pBlockInfo->block8x8Buff.dct4x4out[i], pBlockInfo->block8x8Buff.dct4x4inp[i]);			
			i_decimate_8x8 += mb_decimate_score(pBlockInfo->block8x8Buff.dct4x4out[i]);			
		}	
		i_decimate_mb += i_decimate_8x8;
		if( (i_decimate_8x8 < cost_th_8x8_y) && b_decimate )/* decimate this 8x8 block */
		{
			for(i= 0; i<4; i++)		
				for(j= 0; j<16; j++)
					pBlockInfo->block8x8Buff.dct4x4out[i][j]=0;
		}

		//8x8 block 1
		i_decimate_8x8 = 0;
		for(i= 4; i<8; i++)
		{		
			scan_zig_4x4(pBlockInfo->block8x8Buff.dct4x4out[i], pBlockInfo->block8x8Buff.dct4x4inp[i]);
			i_decimate_8x8 += mb_decimate_score(pBlockInfo->block8x8Buff.dct4x4out[i]);			
		}	
		i_decimate_mb += i_decimate_8x8;
		if( (i_decimate_8x8 < cost_th_8x8_y) && b_decimate )/* decimate this 8x8 block */
		{
			for(i= 4; i<8; i++)		
				for(j= 0; j<16; j++)
					pBlockInfo->block8x8Buff.dct4x4out[i][j]=0;
		}

		//8x8 block 2
		i_decimate_8x8 = 0;
		for(i= 8; i<12; i++)
		{		
			scan_zig_4x4(pBlockInfo->block8x8Buff.dct4x4out[i], pBlockInfo->block8x8Buff.dct4x4inp[i]);
			i_decimate_8x8 += mb_decimate_score(pBlockInfo->block8x8Buff.dct4x4out[i]);			
		}	
		i_decimate_mb += i_decimate_8x8;
		if( (i_decimate_8x8 < cost_th_8x8_y) && b_decimate )/* decimate this 8x8 block */
		{
			for(i= 8; i<12; i++)		
				for(j= 0; j<16; j++)
					pBlockInfo->block8x8Buff.dct4x4out[i][j]=0;
		}

		//8x8 block 3
		i_decimate_8x8 = 0;
		for(i= 12; i<16; i++)
		{		
			scan_zig_4x4(pBlockInfo->block8x8Buff.dct4x4out[i], pBlockInfo->block8x8Buff.dct4x4inp[i]);
			i_decimate_8x8 += mb_decimate_score(pBlockInfo->block8x8Buff.dct4x4out[i]);			
		}	
		i_decimate_mb += i_decimate_8x8;
		if( (i_decimate_8x8 < cost_th_8x8_y) && b_decimate )/* decimate this 8x8 block */
		{
			for(i= 12; i<16; i++)		
				for(j= 0; j<16; j++)
					pBlockInfo->block8x8Buff.dct4x4out[i][j]=0;
		}
		
		///////////////////
		if( (i_decimate_mb < cost_th_mb) && b_decimate )/* decimate this luma macroblock */
		{
			for(i= 0; i<16; i++)		
				for(j= 0; j<16; j++)
					pBlockInfo->block8x8Buff.dct4x4out[i][j]=0;
		}
		else
		{
			//if(b_decimate )
				//printf("i_decimate_mb: %d\n", i_decimate_mb);
		}
		 //############################## end luma #############################

		//8x8 block 4
		i_decimate_score = 0;
		for(i= 16; i<20; i++)
		{		
			scan_zig_4x4(pBlockInfo->block8x8Buff.dct4x4out[i], pBlockInfo->block8x8Buff.dct4x4inp[i]);
			pBlockInfo->block8x8Buff.dct4x4out[i][0]=0;//eliminate DC
			i_decimate_score += mb_decimate_score(pBlockInfo->block8x8Buff.dct4x4out[i]);			
		}	
		if( (i_decimate_score < cost_th_8x8_c) && b_decimate )/* decimate this 8x8 block */
		{
			for(i= 16; i<20; i++)		
				for(j= 0; j<16; j++)
					pBlockInfo->block8x8Buff.dct4x4out[i][j]=0;
		}
		else
		{
			//if(b_decimate )
				//printf("i_decimate_chroma1: %d\n", i_decimate_score);
		}

		//8x8 block 5
		i_decimate_score = 0;
		for(i= 20; i<24; i++)
		{		
			scan_zig_4x4(pBlockInfo->block8x8Buff.dct4x4out[i], pBlockInfo->block8x8Buff.dct4x4inp[i]);
			pBlockInfo->block8x8Buff.dct4x4out[i][0]=0;//eliminate DC
			i_decimate_score += mb_decimate_score(pBlockInfo->block8x8Buff.dct4x4out[i]);			
		}	
		if( (i_decimate_score < cost_th_8x8_c) && b_decimate )/* decimate this 8x8 block */
		{
			for(i= 20; i<24; i++)		
				for(j= 0; j<16; j++)
					pBlockInfo->block8x8Buff.dct4x4out[i][j]=0;
		}
		else
		{
			//if(b_decimate )
				//printf("i_decimate_chroma2: %d\n", i_decimate_score);
		}
	}//P-MB coefficient decimation

	//also send DC		
	if(pBlockInfo->mbType == INTRA)	
	{
		scan_zig_4x4(pBlockInfo->block8x8Buff.dct4x4outDcY, lumaDCcoeffs_tmp);
	}

	//chroma 
	for(j= 0; j<4; j++)
	{		
		pBlockInfo->block8x8Buff.dct2x2outDcU[j] = chromaDCcoeffsU[j];
	}

	for(j= 0; j<4; j++)
	{		
		pBlockInfo->block8x8Buff.dct2x2outDcV[j] = chromaDCcoeffsV[j];
	}

#if DBG_QUANT4X4AC
	if(pBlockInfo->mbType == INTRA)	//set DC to 0
	{
		for(i = 0; i<16; i++)
			pBlockInfo->block8x8Buff.dct4x4inp[i][0] = 0;
	}
	for(i = 0; i<16; i++)
		FPRINTF_MC(pBlockInfo->pEncFrame->dbg_files.quant4x4ac,"%d\n", pBlockInfo->block8x8Buff.dct4x4inp[5][i]);
	for(i = 0; i<16; i++)
		FPRINTF_MC(pBlockInfo->pEncFrame->dbg_files.quant4x4ac,"%d\n", pBlockInfo->block8x8Buff.dct4x4inp[7][i]);
	for(i = 0; i<16; i++)
		FPRINTF_MC(pBlockInfo->pEncFrame->dbg_files.quant4x4ac,"%d\n", pBlockInfo->block8x8Buff.dct4x4inp[13][i]);
	for(i = 0; i<16; i++)
		FPRINTF_MC(pBlockInfo->pEncFrame->dbg_files.quant4x4ac,"%d\n", pBlockInfo->block8x8Buff.dct4x4inp[15][i]);
	for(i = 0; i<16; i++)
		FPRINTF_MC(pBlockInfo->pEncFrame->dbg_files.quant4x4ac,"%d\n", pBlockInfo->block8x8Buff.dct4x4inp[4][i]);
	for(i = 0; i<16; i++)
		FPRINTF_MC(pBlockInfo->pEncFrame->dbg_files.quant4x4ac,"%d\n", pBlockInfo->block8x8Buff.dct4x4inp[6][i]);
	for(i = 0; i<16; i++)
		FPRINTF_MC(pBlockInfo->pEncFrame->dbg_files.quant4x4ac,"%d\n", pBlockInfo->block8x8Buff.dct4x4inp[12][i]);
	for(i = 0; i<16; i++)
		FPRINTF_MC(pBlockInfo->pEncFrame->dbg_files.quant4x4ac,"%d\n", pBlockInfo->block8x8Buff.dct4x4inp[14][i]);
	for(i = 0; i<16; i++)
		FPRINTF_MC(pBlockInfo->pEncFrame->dbg_files.quant4x4ac,"%d\n", pBlockInfo->block8x8Buff.dct4x4inp[1][i]);
	for(i = 0; i<16; i++)
		FPRINTF_MC(pBlockInfo->pEncFrame->dbg_files.quant4x4ac,"%d\n", pBlockInfo->block8x8Buff.dct4x4inp[3][i]);
	for(i = 0; i<16; i++)
		FPRINTF_MC(pBlockInfo->pEncFrame->dbg_files.quant4x4ac,"%d\n", pBlockInfo->block8x8Buff.dct4x4inp[9][i]);
	for(i = 0; i<16; i++)
		FPRINTF_MC(pBlockInfo->pEncFrame->dbg_files.quant4x4ac,"%d\n", pBlockInfo->block8x8Buff.dct4x4inp[11][i]);
	for(i = 0; i<16; i++)
		FPRINTF_MC(pBlockInfo->pEncFrame->dbg_files.quant4x4ac,"%d\n", pBlockInfo->block8x8Buff.dct4x4inp[0][i]);
	for(i = 0; i<16; i++)
		FPRINTF_MC(pBlockInfo->pEncFrame->dbg_files.quant4x4ac,"%d\n", pBlockInfo->block8x8Buff.dct4x4inp[2][i]);
	for(i = 0; i<16; i++)
		FPRINTF_MC(pBlockInfo->pEncFrame->dbg_files.quant4x4ac,"%d\n", pBlockInfo->block8x8Buff.dct4x4inp[8][i]);
	for(i = 0; i<16; i++)
		FPRINTF_MC(pBlockInfo->pEncFrame->dbg_files.quant4x4ac,"%d\n", pBlockInfo->block8x8Buff.dct4x4inp[10][i]);

	//chroma
	for(i = 16; i<24; i++)
			pBlockInfo->block8x8Buff.dct4x4inp[i][0] = 0;

	for(i = 0; i<16; i++)
		FPRINTF_MC(pBlockInfo->pEncFrame->dbg_files.quant4x4ac,"%d\n", pBlockInfo->block8x8Buff.dct4x4inp[16][i]);
	for(i = 0; i<16; i++)
		FPRINTF_MC(pBlockInfo->pEncFrame->dbg_files.quant4x4ac,"%d\n", pBlockInfo->block8x8Buff.dct4x4inp[17][i]);
	for(i = 0; i<16; i++)
		FPRINTF_MC(pBlockInfo->pEncFrame->dbg_files.quant4x4ac,"%d\n", pBlockInfo->block8x8Buff.dct4x4inp[18][i]);
	for(i = 0; i<16; i++)
		FPRINTF_MC(pBlockInfo->pEncFrame->dbg_files.quant4x4ac,"%d\n", pBlockInfo->block8x8Buff.dct4x4inp[19][i]);
	for(i = 0; i<16; i++)
		FPRINTF_MC(pBlockInfo->pEncFrame->dbg_files.quant4x4ac,"%d\n", pBlockInfo->block8x8Buff.dct4x4inp[20][i]);
	for(i = 0; i<16; i++)
		FPRINTF_MC(pBlockInfo->pEncFrame->dbg_files.quant4x4ac,"%d\n", pBlockInfo->block8x8Buff.dct4x4inp[21][i]);
	for(i = 0; i<16; i++)
		FPRINTF_MC(pBlockInfo->pEncFrame->dbg_files.quant4x4ac,"%d\n", pBlockInfo->block8x8Buff.dct4x4inp[22][i]);
	for(i = 0; i<16; i++)
		FPRINTF_MC(pBlockInfo->pEncFrame->dbg_files.quant4x4ac,"%d\n", pBlockInfo->block8x8Buff.dct4x4inp[23][i]);

#endif

#if DBG_BSG_DATA //print coeffs in bsg inverse zigzag order (like in HW). Also called "bsg_data" in HW
	//bsg data
	for(i= 15; i>=0; i--)
	{	
		if(pBlockInfo->mbType == INTRA)	
			//printf("%d\n", pBlockInfo->block8x8Buff.dct4x4outDcY[i]);
			FPRINTF_MC(pBlockInfo->pEncFrame->dbg_files.bsg_data, "%d\n",pBlockInfo->block8x8Buff.dct4x4outDcY[i]);
	}	

	//luma ac
	for(i= 0; i<16; i++)
	{		
		for(j= 15; j>=0; j--)//inverse scan order
		{
			if(pBlockInfo->mbType == INTRA)	
				pBlockInfo->block8x8Buff.dct4x4out[i][0]=0;//make hw like
		}
		for(j= 15; j>=0; j--)
		{
			//printf("%d\n", pBlockInfo->block8x8Buff.dct4x4out[i][j]);
			FPRINTF_MC(pBlockInfo->pEncFrame->dbg_files.bsg_data, "%d\n",pBlockInfo->block8x8Buff.dct4x4out[i][j]);
		}
	}

	//chroma dc
	for(j= 3; j>=0; j--)
	{		
		//printf("%d\n", chromaDCcoeffsU[j]);
		FPRINTF_MC(pBlockInfo->pEncFrame->dbg_files.bsg_data, "%d\n",chromaDCcoeffsU[j]);
	}

	for(j= 3; j>=0; j--)
	{		
		//printf("%d\n", chromaDCcoeffsV[j]);
		FPRINTF_MC(pBlockInfo->pEncFrame->dbg_files.bsg_data, "%d\n",chromaDCcoeffsV[j]);
	}

	//chroma ac
	for(i= 16; i<24; i++)
	{		
		for(j= 15; j>=0; j--)//inverse scan order
		{			
			pBlockInfo->block8x8Buff.dct4x4out[i][0]=0;
		}
		for(j= 15; j>=0; j--)
		{
			//printf("%d\n", pBlockInfo->block8x8Buff.dct4x4out[i][j]);
			FPRINTF_MC(pBlockInfo->pEncFrame->dbg_files.bsg_data, "%d\n",pBlockInfo->block8x8Buff.dct4x4out[i][j]);
		}
	}

#endif

	//#####################################################################
	//#####################################################################
	//#####################################################################
	//#####################################################################
	//######################## Backward Loop  #############################
	//#####################################################################
	//#####################################################################
	//#####################################################################
	//#####################################################################

	//input to this process is pBlockInfo->block8x8Buff.dct4x4inp[i][j]:
	lumaDCcoeffsCnt = 0;
	
	if(pBlockInfo->mbType == INTRA)	
	{
		//idct4x4dc
		idct4x4dc_c(lumaDCcoeffs_tmp);
	}

	if(pBlockInfo->mbType == INTRA)	
	{
		iquant4x4dc_c(lumaDCcoeffs_tmp, pBlockInfo->mquantY);
	}

	if(pBlockInfo->mbType == INTRA)	
	{
		//this step should be removed later in order to increase performance
		lumaDCcoeffs[0]  = lumaDCcoeffs_tmp[0]; 
		lumaDCcoeffs[1]  = lumaDCcoeffs_tmp[1]; 
		lumaDCcoeffs[4]  = lumaDCcoeffs_tmp[2]; 
		lumaDCcoeffs[5]  = lumaDCcoeffs_tmp[3]; 
		lumaDCcoeffs[2]  = lumaDCcoeffs_tmp[4]; 
		lumaDCcoeffs[3]  = lumaDCcoeffs_tmp[5]; 
		lumaDCcoeffs[6]  = lumaDCcoeffs_tmp[6]; 
		lumaDCcoeffs[7]  = lumaDCcoeffs_tmp[7]; 
		lumaDCcoeffs[8]  = lumaDCcoeffs_tmp[8]; 
		lumaDCcoeffs[9]  = lumaDCcoeffs_tmp[9]; 
		lumaDCcoeffs[12] = lumaDCcoeffs_tmp[10];
		lumaDCcoeffs[13] = lumaDCcoeffs_tmp[11];
		lumaDCcoeffs[10] = lumaDCcoeffs_tmp[12];
		lumaDCcoeffs[11] = lumaDCcoeffs_tmp[13];
		lumaDCcoeffs[14] = lumaDCcoeffs_tmp[14];
		lumaDCcoeffs[15] = lumaDCcoeffs_tmp[15];
	}

	//######################## loop over 4-luma 8x8 blocks #############################	
	for (pBlockInfo->blockIdx = 0; pBlockInfo->blockIdx < 4; pBlockInfo->blockIdx++)
	{		
		//block BW DCT
		for(blk4x4cnt= 0; blk4x4cnt<4; blk4x4cnt++)
		{
			//iquant4x4
			iquant4x4_c(pBlockInfo->block8x8Buff.dct4x4inp[(pBlockInfo->blockIdx * 4) + blk4x4cnt], pBlockInfo->mquantY);						

			if(pBlockInfo->mbType == INTRA)	
			{
				//put DC back before inverse transform
				pBlockInfo->block8x8Buff.dct4x4inp[(pBlockInfo->blockIdx * 4) + blk4x4cnt][0] = lumaDCcoeffs[lumaDCcoeffsCnt++];
			}

			idct4x4_c(pBlockInfo->block8x8Buff.dct4x4inp[(pBlockInfo->blockIdx * 4) + blk4x4cnt]);	

		}

		//convert 4-4x4 blocks into tcReconst 8x8 blocks 
		write_4x4_blocks(pBlockInfo);
	}	

	//######################## loop over 2-chroma 8x8 blocks #############################	

	//loop over chroma 8x8 blocks
	for (pBlockInfo->blockIdx = 4; pBlockInfo->blockIdx < 6; pBlockInfo->blockIdx++)
	{	
		if(pBlockInfo->blockIdx == 4)
			chromaDCcoeffs = chromaDCcoeffsU;
		else
			chromaDCcoeffs = chromaDCcoeffsV;

		//idct2x2dc
		idct2x2dc_c(chromaDCcoeffs);

		//iquant2x2dc
		iquant2x2dc_c(chromaDCcoeffs, pBlockInfo->mquantC);

		//block FW DCT
		for(blk4x4cnt= 0; blk4x4cnt<4; blk4x4cnt++)
		{
			//iquant4x4
			iquant4x4_c(pBlockInfo->block8x8Buff.dct4x4inp[(pBlockInfo->blockIdx * 4) + blk4x4cnt], pBlockInfo->mquantC);			

			//if(pBlockInfo->mbType == INTRA)	
			{
				//put DC back before inverse transform
				pBlockInfo->block8x8Buff.dct4x4inp[(pBlockInfo->blockIdx * 4) + blk4x4cnt][0] = chromaDCcoeffs[blk4x4cnt];
			}			

			//idct4x4
			idct4x4_c(pBlockInfo->block8x8Buff.dct4x4inp[(pBlockInfo->blockIdx * 4) + blk4x4cnt]);
		}	

		//convert 4-4x4 blocks into tcReconst 8x8 blocks 
		write_4x4_blocks(pBlockInfo);
	}	

	if(pBlockInfo->mbType == INTRA)	
	{
		iIntraPred(pBlockInfo);
		for (pBlockInfo->blockIdx = 0; pBlockInfo->blockIdx < 6; pBlockInfo->blockIdx++)
		{
			intra_clipping(pBlockInfo);//for debug print only
		}
	}

#if DBG_RECONSTRUCTED
	//reconstructed
	for (pBlockInfo->blockIdx = 0; pBlockInfo->blockIdx < 6; pBlockInfo->blockIdx++)
	{
		get_4x4_blocks_dbg(pBlockInfo);
	}
	print_reconstructed(pBlockInfo);
#endif

	//MB motion compensation
	for (pBlockInfo->blockIdx = 0; pBlockInfo->blockIdx < 6; pBlockInfo->blockIdx++)
	{
		motion_compensation(pBlockInfo);	
	}

#if GLOBAL_DEBUG
	

	FPRINTF_MC(pBlockInfo->pEncFrame->dbg_files.writeback, "#FR: %d Type: %d MB(%d, %d)\n",pBlockInfo->frameCntr, pBlockInfo->frameType, pBlockInfo->mbX, pBlockInfo->mbY);

	if( pBlockInfo->mbX % 2 == 1 )
	{
		FPRINTF_MC(pBlockInfo->pEncFrame->dbg_files.writeback_ab, "#FR: %d Type: %d MB(%d_%d, %d)\n",pBlockInfo->frameCntr, pBlockInfo->frameType, pBlockInfo->mbX - 1,pBlockInfo->mbX, pBlockInfo->mbY);
	}

	FPRINTF_MC(pBlockInfo->pEncFrame->dbg_files.writeback_hex, "#FR: %d Type: %d MB(%d, %d)\n",pBlockInfo->frameCntr, pBlockInfo->frameType, pBlockInfo->mbX, pBlockInfo->mbY);	
	for(y = 0; y < 8; y++)//get current data in a 16x16 buffer for simplicity
	{
		for(x = 0; x < 8; x++)
		{
			//luma
			MB_buffer_y[y][x]     = pBlockInfo->block8x8Buff.tcReconst[0][y][x]; 
			MB_buffer_y[y][x+8]   = pBlockInfo->block8x8Buff.tcReconst[1][y][x]; 
			MB_buffer_y[y+8][x]   = pBlockInfo->block8x8Buff.tcReconst[2][y][x]; 
			MB_buffer_y[y+8][x+8] = pBlockInfo->block8x8Buff.tcReconst[3][y][x]; 

			//chroma
			MB_buffer_c[y][x*2]	  = pBlockInfo->block8x8Buff.tcReconst[4][y][x];
			MB_buffer_c[y][x*2+1] = pBlockInfo->block8x8Buff.tcReconst[5][y][x];
		}
	}

	for(y = 0; y< 16; y++)//get current data in a 16x16 buffer for simplicity
	{
		for(x = 0; x< 16; x++)
		{
			FPRINTF_MC(pBlockInfo->pEncFrame->dbg_files.writeback, "\t%d",MB_buffer_y[y][x]);
			FPRINTF_MC(pBlockInfo->pEncFrame->dbg_files.writeback_hex, "\t%02X",MB_buffer_y[y][x]);
		}
		FPRINTF_MC(pBlockInfo->pEncFrame->dbg_files.writeback, "\n");
		FPRINTF_MC(pBlockInfo->pEncFrame->dbg_files.writeback_hex, "\n");

		if( pBlockInfo->mbX % 2 == 1 )
		{
			for(x = 0; x< 16; x++)
			{
				FPRINTF_MC(pBlockInfo->pEncFrame->dbg_files.writeback_ab, "\t%d",MB_buffer_prev_y[y][x]);
			}
			for(x = 0; x< 16; x++)
			{
				FPRINTF_MC(pBlockInfo->pEncFrame->dbg_files.writeback_ab, "\t%d",MB_buffer_y[y][x]);
			}
			FPRINTF_MC(pBlockInfo->pEncFrame->dbg_files.writeback_ab, "\n");
		}

	}
	FPRINTF_MC(pBlockInfo->pEncFrame->dbg_files.writeback, " \n");
	FPRINTF_MC(pBlockInfo->pEncFrame->dbg_files.writeback_hex, " \n");

	if( pBlockInfo->mbX % 2 == 1 )
	{
		FPRINTF_MC(pBlockInfo->pEncFrame->dbg_files.writeback_ab, " \n");
	}

	for(y = 0; y< 8; y++)//get current data in a 16x16 buffer for simplicity
	{
		for(x = 0; x< 16; x++)
		{
			FPRINTF_MC(pBlockInfo->pEncFrame->dbg_files.writeback, "\t%d", MB_buffer_c[y][x]);
			FPRINTF_MC(pBlockInfo->pEncFrame->dbg_files.writeback_hex, "\t%02X", MB_buffer_c[y][x]);
		}
		FPRINTF_MC(pBlockInfo->pEncFrame->dbg_files.writeback, "\n");
		FPRINTF_MC(pBlockInfo->pEncFrame->dbg_files.writeback_hex, "\n");

		if( pBlockInfo->mbX % 2 == 1 )
		{
			for(x = 0; x < 16; x++)
			{
				FPRINTF_MC(pBlockInfo->pEncFrame->dbg_files.writeback_ab, "\t%d",MB_buffer_prev_c[y][x]);
			}
			for(x = 0; x < 16; x++)
			{
				FPRINTF_MC(pBlockInfo->pEncFrame->dbg_files.writeback_ab, "\t%d",MB_buffer_c[y][x]);
			}

			FPRINTF_MC(pBlockInfo->pEncFrame->dbg_files.writeback_ab, "\n");
		}
	}

	for(y = 0; y < 8; y++)//get current data in a 16x16 buffer for simplicity
	{
		for(x = 0; x < 16; x++)
		{
			//luma
			MB_buffer_prev_y[y][x]		= MB_buffer_y[y][x];
			MB_buffer_prev_y[y+8][x]	= MB_buffer_y[y+8][x];

			//chroma
			MB_buffer_prev_c[y][x]      = MB_buffer_c[y][x];
		}
	}

#endif
}

void get_residual(pBlockInfo_t pBlockInfo, int do_dbg_print) //tc_mcnr_fn
{
	UInt8  x, y; 
	Int32 currrent, bestMatch, dctInp, threshold;

	threshold = 10;

	for (y=0; y<8; y++ ) 
	{
		for (x=0; x<8; x++ ) 
		{
			if(pBlockInfo->mbType == INTRA)
			{
				pBlockInfo->block8x8Buff.dctInp[pBlockInfo->blockIdx][y][x] = 
					(Int32)pBlockInfo->block8x8Buff.ppp_currentData[pBlockInfo->blockIdx][y][x];
			}
			else
			{				
				currrent	= (Int32)pBlockInfo->block8x8Buff.ppp_currentData[pBlockInfo->blockIdx][y][x];									
				bestMatch	= (Int32)pBlockInfo->pMvMemory[pBlockInfo->mbNumb].bestMatch[pBlockInfo->blockIdx][y][x];
				if(pBlockInfo->pEncFrame->pMvMemory[pBlockInfo->mbNumb].skip)
					dctInp = 0;
				else
					dctInp = currrent - bestMatch;			
				pBlockInfo->block8x8Buff.dctInp[pBlockInfo->blockIdx][y][x] = dctInp;
			}
		}
	}

#if GLOBAL_DEBUG
	if(do_dbg_print)
	{
		for (y=0; y<8; y++ ) 
		{
			for (x=0; x<8; x++ ) 
			{
				FPRINTF_MC(pBlockInfo->pEncFrame->dbg_files.res, "\t%d", pBlockInfo->block8x8Buff.dctInp[pBlockInfo->blockIdx][y][x]);

				if( pBlockInfo->mbX % 2 == 0 )
				{
					FPRINTF_MC(pBlockInfo->pEncFrame->dbg_files.res_a, "\t%d", pBlockInfo->block8x8Buff.dctInp[pBlockInfo->blockIdx][y][x]);
				}

				if( pBlockInfo->mbX % 2 == 1 )
				{
					FPRINTF_MC(pBlockInfo->pEncFrame->dbg_files.res_b, "\t%d", pBlockInfo->block8x8Buff.dctInp[pBlockInfo->blockIdx][y][x]);
				}
			}
			FPRINTF_MC(pBlockInfo->pEncFrame->dbg_files.res, "\n");

			if( pBlockInfo->mbX % 2 == 0 )
			{
				FPRINTF_MC(pBlockInfo->pEncFrame->dbg_files.res_a, "\n");
			}

			if( pBlockInfo->mbX % 2 == 1 )
			{
				FPRINTF_MC(pBlockInfo->pEncFrame->dbg_files.res_b, "\n");
			}
		}
	}
#endif
}

void get_chroma_skip(pBlockInfo_t pBlockInfo)
{
	UInt8  x, y; 
	Int32 currrent, bestMatch;
	Int32 sad_chroma=0;
	//int sad_th_slope  = 12;//SAD Threshold slope 
	//int sad_th_offset = 50;
	UInt32 sad_threshold;

	if(pBlockInfo->pEncFrame->pMvMemory[pBlockInfo->mbNumb].skip && pBlockInfo->mbType == INTER)//chroma
	{
		sad_threshold = pBlockInfo->pEncFrame->pInputData->SkipMbCSlope*pBlockInfo->pEncFrame->PQIndex + pBlockInfo->pEncFrame->pInputData->SkipMbCOffset; //Th = a.QP + b (e.g for luma: a= 23, b= 100). algo invented by MC
		for (y=0; y<8; y++ ) 
		{
			for (x=0; x<8; x++ ) 
			{							
				currrent	= (Int32)pBlockInfo->block8x8Buff.ppp_currentData[3][y][x];									
				bestMatch	= (Int32)pBlockInfo->pMvMemory[pBlockInfo->mbNumb].bestMatch[3][y][x];					
				sad_chroma  += abs(currrent - bestMatch);											
			}
		}
		for (y=0; y<8; y++ ) 
		{
			for (x=0; x<8; x++ ) 
			{							
				currrent	= (Int32)pBlockInfo->block8x8Buff.ppp_currentData[4][y][x];									
				bestMatch	= (Int32)pBlockInfo->pMvMemory[pBlockInfo->mbNumb].bestMatch[4][y][x];					
				sad_chroma  += abs(currrent - bestMatch);											
			}
		}
		//bilan
		if(sad_chroma < sad_threshold)
			pBlockInfo->pEncFrame->pMvMemory[pBlockInfo->mbNumb].skip = 1;
		else
			pBlockInfo->pEncFrame->pMvMemory[pBlockInfo->mbNumb].skip = 0;

		//if(pBlockInfo->mbX == 11 && pBlockInfo->mbY == 36)
			//printf("");
		
		//pBlockInfo->pEncFrame->pMvMemory[pBlockInfo->mbNumb].skip = 1
	}
}

void motion_compensation(pBlockInfo_t pBlockInfo)
{
	UInt8  x, y;  
	Int16 in;
	Int16 out;
	Int16 bmatch;

	for (y=0; y<8; y++ ) 
	{
		for (x=0; x<8; x++ ) 
		{
			// *************** select source data ***************************************
			in = pBlockInfo->block8x8Buff.tcReconst[pBlockInfo->blockIdx][y][x];
			bmatch = pBlockInfo->pMvMemory[pBlockInfo->mbNumb].bestMatch[pBlockInfo->blockIdx][y][x];
			//printf("%d\n", in);

			//if(pBlockInfo->frameType == P)
				//printf("%d \n",in);
			      
			// perform motion compensation only for non intra MB's
			if(pBlockInfo->mbType == INTRA)
			{
				//out = CLIP264(in); and #if 1 bellow
				out = in;
			}
			else 
			{
				out = in + bmatch;
			}
			
#if 1
			// clip values to 0..255 (8 bit). 8.1.3.10
			if (out > 255) 
			{
				out = 255;
			}
			else if (out < 0) 
			{
				out = 0;
			}
#endif
			pBlockInfo->block8x8Buff.tcReconst[pBlockInfo->blockIdx][y][x] = out;
			
		}//x loop 
	}//y loop
}

void intra_clipping(pBlockInfo_t pBlockInfo)
{
	UInt8  x, y;  
	Int16 out;

	for (y=0; y<8; y++ ) 
	{
		for (x=0; x<8; x++ ) 
		{
			// *************** select source data ***************************************
			out = pBlockInfo->block8x8Buff.tcReconst[pBlockInfo->blockIdx][y][x];
		
			// clip values to 0..255 (8 bit). 8.1.3.10
			if (out > 255) 
			{
				out = 255;
			}
			else if (out < 0) 
			{
				out = 0;
			}
			pBlockInfo->block8x8Buff.tcReconst[pBlockInfo->blockIdx][y][x] = out;
			
		}//x loop 
	}//y loop
}

void get_4x4_blocks(pBlockInfo_t pBlockInfo)//mb_encoding
{
	Int8 x, y, i, blk4x4cnt;  
	Int8 start_x, start_y;  
	
	//Int16 **dct = dct1;
	 
	//get 8x8 into 4-4x4 blocks
	start_x   = 0;
	start_y   = 0;	
	i         = 0;
	blk4x4cnt = 0;
	for (y=start_y; y < start_y+4; y++ ) 
	{
		for (x=start_x; x < start_x+4; x++ ) 
		{
			pBlockInfo->block8x8Buff.dct4x4inp[(pBlockInfo->blockIdx * 4) + blk4x4cnt][i++] = (Int16)pBlockInfo->block8x8Buff.dctInp[pBlockInfo->blockIdx][y][x];
		}
	}

	start_x   = 4;
	start_y   = 0;	
	i         = 0;
	blk4x4cnt = 1;
	for (y=start_y; y < start_y+4; y++ ) 
	{
		for (x=start_x; x < start_x+4; x++ ) 
		{
			pBlockInfo->block8x8Buff.dct4x4inp[(pBlockInfo->blockIdx * 4) + blk4x4cnt][i++] = (Int16)pBlockInfo->block8x8Buff.dctInp[pBlockInfo->blockIdx][y][x];
		}
	}

	start_x   = 0;
	start_y   = 4;	
	i         = 0;
	blk4x4cnt = 2;
	for (y=start_y; y < start_y+4; y++ ) 
	{
		for (x=start_x; x < start_x+4; x++ ) 
		{
			pBlockInfo->block8x8Buff.dct4x4inp[(pBlockInfo->blockIdx * 4) + blk4x4cnt][i++] = (Int16)pBlockInfo->block8x8Buff.dctInp[pBlockInfo->blockIdx][y][x];
		}
	}
	
	start_x   = 4;
	start_y   = 4;	
	i         = 0;
	blk4x4cnt = 3;
	for (y=start_y; y < start_y+4; y++ ) 
	{
		for (x=start_x; x < start_x+4; x++ ) 
		{
			pBlockInfo->block8x8Buff.dct4x4inp[(pBlockInfo->blockIdx * 4) + blk4x4cnt][i++] = (Int16)pBlockInfo->block8x8Buff.dctInp[pBlockInfo->blockIdx][y][x];
		}
	}
	printf("");
}

void write_4x4_blocks(pBlockInfo_t pBlockInfo)//mb_encoding
{
	Int8 x, y, i, blk4x4cnt;  
	Int8 start_x, start_y;  
	
	//Int16 **dct = dct1;
	 
	//get 8x8 into 4-4x4 blocks
	start_x   = 0;
	start_y   = 0;	
	i         = 0;
	blk4x4cnt = 0;
	for (y=start_y; y < start_y+4; y++ ) 
	{
		for (x=start_x; x < start_x+4; x++ ) 
		{
			pBlockInfo->block8x8Buff.tcReconst[pBlockInfo->blockIdx][y][x] = pBlockInfo->block8x8Buff.dct4x4inp[(pBlockInfo->blockIdx * 4) + blk4x4cnt][i++];
		}
	}

	start_x   = 4;
	start_y   = 0;	
	i         = 0;
	blk4x4cnt = 1;
	for (y=start_y; y < start_y+4; y++ ) 
	{
		for (x=start_x; x < start_x+4; x++ ) 
		{
			pBlockInfo->block8x8Buff.tcReconst[pBlockInfo->blockIdx][y][x] = pBlockInfo->block8x8Buff.dct4x4inp[(pBlockInfo->blockIdx * 4) + blk4x4cnt][i++];
		}
	}

	start_x   = 0;
	start_y   = 4;	
	i         = 0;
	blk4x4cnt = 2;
	for (y=start_y; y < start_y+4; y++ ) 
	{
		for (x=start_x; x < start_x+4; x++ ) 
		{
			pBlockInfo->block8x8Buff.tcReconst[pBlockInfo->blockIdx][y][x] = pBlockInfo->block8x8Buff.dct4x4inp[(pBlockInfo->blockIdx * 4) + blk4x4cnt][i++];
		}
	}
	
	start_x   = 4;
	start_y   = 4;	
	i         = 0;
	blk4x4cnt = 3;
	for (y=start_y; y < start_y+4; y++ ) 
	{
		for (x=start_x; x < start_x+4; x++ ) 
		{
			pBlockInfo->block8x8Buff.tcReconst[pBlockInfo->blockIdx][y][x] = pBlockInfo->block8x8Buff.dct4x4inp[(pBlockInfo->blockIdx * 4) + blk4x4cnt][i++];
		}
	}
	//printf("");
}

void getIntraPredictorY(pBlockInfo_t pBlockInfo) 
{
	Int32 intra16x16DCPred, intra16x16DCPredHor, intra16x16DCPredVer;
	Bool topAvailable, leftAvailable;
	int i;

	//check availability
	topAvailable  = FALSE;
	leftAvailable = FALSE;
	if(pBlockInfo->mbX != 0)
		leftAvailable = TRUE;
	if(pBlockInfo->mbY != 0)
		topAvailable = TRUE;

	//get intra16x16 DC predictor
	intra16x16DCPred  = 0;
	intra16x16DCPredHor = 0;
	intra16x16DCPredVer = 0;

	if(topAvailable && leftAvailable)
	{
		for(i = 0; i<16; i++)
			intra16x16DCPredHor += pBlockInfo->pCurrLineMem[pBlockInfo->sliceMbNumb-1].intraPR[i];

		for(i = 0; i<16; i++)
			intra16x16DCPredVer += pBlockInfo->pPrevLineMem[pBlockInfo->sliceMbNumb].intraPB[i];

		intra16x16DCPred = (intra16x16DCPredHor + intra16x16DCPredVer + 16) >> 5;  //(8-114)
	}
	else if(!topAvailable && leftAvailable)
	{
		for(i = 0; i<16; i++)
			intra16x16DCPredHor += pBlockInfo->pCurrLineMem[pBlockInfo->sliceMbNumb-1].intraPR[i];		

		intra16x16DCPred = (intra16x16DCPredHor + 8) >> 4;  //(8-115)
	}
	else if(topAvailable && !leftAvailable)
	{
		for(i = 0; i<16; i++)
			intra16x16DCPredVer += pBlockInfo->pPrevLineMem[pBlockInfo->sliceMbNumb].intraPB[i];

		intra16x16DCPred = (intra16x16DCPredVer + 8) >> 4;  //(8-116)
	}
	else//first MB
	{
		intra16x16DCPred = 128;//(8-117)
	}	

	pBlockInfo->intra16x16DCPred = intra16x16DCPred;

}

void getIntraPredictorU(pBlockInfo_t pBlockInfo) 
{
	Bool topAvailable, leftAvailable;
	int i;

	//check availability
	topAvailable  = FALSE;
	leftAvailable = FALSE;
	if(pBlockInfo->mbX != 0)
		leftAvailable = TRUE;
	if(pBlockInfo->mbY != 0)
		topAvailable = TRUE;

	if(topAvailable && leftAvailable)
	{
		//hor
		pBlockInfo->chromaPredDir = 1;
		for(i = 0; i<8; i++)
			pBlockInfo->intra8x8ChromaPred_u[i] = pBlockInfo->pCurrLineMem[pBlockInfo->sliceMbNumb-1].intraPRu[i];		
	}
	else if(!topAvailable && leftAvailable)
	{
		//hor
		pBlockInfo->chromaPredDir = 1;
		for(i = 0; i<8; i++)
			pBlockInfo->intra8x8ChromaPred_u[i] = pBlockInfo->pCurrLineMem[pBlockInfo->sliceMbNumb-1].intraPRu[i];		
	}
	else if(topAvailable && !leftAvailable)
	{
		//ver
		pBlockInfo->chromaPredDir = 2;
		for(i = 0; i<8; i++)
			pBlockInfo->intra8x8ChromaPred_u[i] = pBlockInfo->pPrevLineMem[pBlockInfo->sliceMbNumb].intraPBu[i];
	}
	else//first MB
	{
		pBlockInfo->chromaPredDir = 0;
		for(i = 0; i<8; i++)
			pBlockInfo->intra8x8ChromaPred_u[i] = 128;
	}	
}

void getIntraPredictorV(pBlockInfo_t pBlockInfo) 
{
	Bool topAvailable, leftAvailable;
	int i;

	//check availability
	topAvailable  = FALSE;
	leftAvailable = FALSE;
	if(pBlockInfo->mbX != 0)
		leftAvailable = TRUE;
	if(pBlockInfo->mbY != 0)
		topAvailable = TRUE;

	if(topAvailable && leftAvailable)
	{
		//hor
		pBlockInfo->chromaPredDir = 1;
		for(i = 0; i<8; i++)
			pBlockInfo->intra8x8ChromaPred_v[i] = pBlockInfo->pCurrLineMem[pBlockInfo->sliceMbNumb-1].intraPRv[i];		
	}
	else if(!topAvailable && leftAvailable)
	{
		//hor
		pBlockInfo->chromaPredDir = 1;
		for(i = 0; i<8; i++)
			pBlockInfo->intra8x8ChromaPred_v[i] = pBlockInfo->pCurrLineMem[pBlockInfo->sliceMbNumb-1].intraPRv[i];		
	}
	else if(topAvailable && !leftAvailable)
	{
		//ver
		pBlockInfo->chromaPredDir = 2;
		for(i = 0; i<8; i++)
			pBlockInfo->intra8x8ChromaPred_v[i] = pBlockInfo->pPrevLineMem[pBlockInfo->sliceMbNumb].intraPBv[i];
	}
	else//first MB
	{
		pBlockInfo->chromaPredDir = 0;
		for(i = 0; i<8; i++)
			pBlockInfo->intra8x8ChromaPred_v[i] = 128;
	}	
}

void intraPred(pBlockInfo_t pBlockInfo) 
{
	UInt8  row;
	UInt8 col;
	
	//luma
	if(pBlockInfo->blockIdx < 4)
	{
		for (row=0; row<8; row++) 
		{
			for (col=0; col<8; col++) 
			{
				pBlockInfo->block8x8Buff.dctInp[pBlockInfo->blockIdx][row][col] -= pBlockInfo->intra16x16DCPred;
			} 
		} 
	}

	//chroma
	if(pBlockInfo->blockIdx == 4)
	{
		for (row=0; row<8; row++) 
		{
			for (col=0; col<8; col++) 
			{
				if(pBlockInfo->chromaPredDir == 0)
					pBlockInfo->block8x8Buff.dctInp[pBlockInfo->blockIdx][row][col] -= 128;
				else if (pBlockInfo->chromaPredDir == 1)
					pBlockInfo->block8x8Buff.dctInp[pBlockInfo->blockIdx][row][col] -= pBlockInfo->intra8x8ChromaPred_u[row];//???
				else
					pBlockInfo->block8x8Buff.dctInp[pBlockInfo->blockIdx][row][col] -= pBlockInfo->intra8x8ChromaPred_u[col];//???
			} 
		} 
	}

	//chroma
	if(pBlockInfo->blockIdx == 5)
	{
		for (row=0; row<8; row++) 
		{
			for (col=0; col<8; col++) 
			{
				if(pBlockInfo->chromaPredDir == 0)
					pBlockInfo->block8x8Buff.dctInp[pBlockInfo->blockIdx][row][col] -= 128;
				else if (pBlockInfo->chromaPredDir == 1)
					pBlockInfo->block8x8Buff.dctInp[pBlockInfo->blockIdx][row][col] -= pBlockInfo->intra8x8ChromaPred_v[row];//???
				else
					pBlockInfo->block8x8Buff.dctInp[pBlockInfo->blockIdx][row][col] -= pBlockInfo->intra8x8ChromaPred_v[col];//???			
			} 
		} 
	}
}

void iIntraPred(pBlockInfo_t pBlockInfo) 
{
	UInt8  row;
	UInt8 col;

	//luma
	for (pBlockInfo->blockIdx = 0; pBlockInfo->blockIdx < 4; pBlockInfo->blockIdx++)
	{
		for (row=0; row<8; row++) 
		{
			for (col=0; col<8; col++) 
			{
				pBlockInfo->block8x8Buff.tcReconst[pBlockInfo->blockIdx][row][col] += (Int16)pBlockInfo->intra16x16DCPred;
			}
		}
	}	

	//chroma U
	pBlockInfo->blockIdx = 4;
	{
		for (row=0; row<8; row++) 
		{
			for (col=0; col<8; col++) 
			{
				if(pBlockInfo->chromaPredDir == 0)
					pBlockInfo->block8x8Buff.tcReconst[pBlockInfo->blockIdx][row][col] += 128;
				else if (pBlockInfo->chromaPredDir == 1)
					pBlockInfo->block8x8Buff.tcReconst[pBlockInfo->blockIdx][row][col] += (Int16)pBlockInfo->intra8x8ChromaPred_u[row];
				else
					pBlockInfo->block8x8Buff.tcReconst[pBlockInfo->blockIdx][row][col] += (Int16)pBlockInfo->intra8x8ChromaPred_u[col];
			}
		}
	}	

	//chroma V
	pBlockInfo->blockIdx = 5;
	{
		for (row=0; row<8; row++) 
		{
			for (col=0; col<8; col++) 
			{
				if(pBlockInfo->chromaPredDir == 0)
					pBlockInfo->block8x8Buff.tcReconst[pBlockInfo->blockIdx][row][col] += 128;
				else if (pBlockInfo->chromaPredDir == 1)
					pBlockInfo->block8x8Buff.tcReconst[pBlockInfo->blockIdx][row][col] += (Int16)pBlockInfo->intra8x8ChromaPred_v[row];
				else
					pBlockInfo->block8x8Buff.tcReconst[pBlockInfo->blockIdx][row][col] += (Int16)pBlockInfo->intra8x8ChromaPred_v[col];
			}
		}
	}	
}

void dct4x4_c(Int16* data)//also see alternative bellow
{
    Int16 s[4];

    //
    // horizontal
    //

	//i = 0
	s[0] = data[0] + data[3];
	s[3] = data[0] - data[3];
	s[1] = data[1] + data[2];
	s[2] = data[1] - data[2];
	data[0] = s[0] + s[1];
	data[2] = s[0] - s[1];
	data[1] = (s[3] << 1) + s[2];
	data[3] = s[3] - (s[2] << 1);

	//i = 1
	s[0] = data[4] + data[7];
    s[3] = data[4] - data[7];
    s[1] = data[5] + data[6];
    s[2] = data[5] - data[6];
    data[4] = s[0] + s[1];
    data[6] = s[0] - s[1];
    data[5] = (s[3] << 1) + s[2];
    data[7] = s[3] - (s[2] << 1);

	//i = 2
	s[0] = data[8] + data[11];
    s[3] = data[8] - data[11];
    s[1] = data[9] + data[10];
    s[2] = data[9] - data[10];
    data[8] = s[0] + s[1];
    data[10] = s[0] - s[1];
    data[9] = (s[3] << 1) + s[2];
    data[11] = s[3] - (s[2] << 1);

	//i = 3
	s[0] = data[12] + data[15];
    s[3] = data[12] - data[15];
    s[1] = data[13] + data[14];
    s[2] = data[13] - data[14];
    data[12] = s[0] + s[1];
    data[14] = s[0] - s[1];
    data[13] = (s[3] << 1) + s[2];
    data[15] = s[3] - (s[2] << 1);

    //
    // vertical
    //
  
	//i = 0
	s[0] = data[0] + data[12];
    s[3] = data[0] - data[12];
    s[1] = data[4] + data[8];
    s[2] = data[4] - data[8];
    data[0] = s[0] + s[1];
    data[8] = s[0] - s[1];
    data[4] = (s[3] << 1) + s[2];
    data[12] = s[3] - (s[2] << 1);

	//i = 1
	s[0] = data[1] + data[13];
    s[3] = data[1] - data[13];
    s[1] = data[5] + data[9];
    s[2] = data[5] - data[9];
    data[1] = s[0] + s[1];
    data[9] = s[0] - s[1];
    data[5] = (s[3] << 1) + s[2];
    data[13] = s[3] - (s[2] << 1);

	//i = 2
	s[0] = data[2] + data[14];
    s[3] = data[2] - data[14];
    s[1] = data[6] + data[10];
    s[2] = data[6] - data[10];
    data[2] = s[0] + s[1];
    data[10] = s[0] - s[1];
    data[6] = (s[3] << 1) + s[2];
    data[14] = s[3] - (s[2] << 1);

	//i = 3
	s[0] = data[3] + data[15];
    s[3] = data[3] - data[15];
    s[1] = data[7] + data[11];
    s[2] = data[7] - data[11];
    data[3] = s[0] + s[1];
    data[11] = s[0] - s[1];
    data[7] = (s[3] << 1) + s[2];
    data[15] = s[3] - (s[2] << 1);
}


void dct4x4_c_bc(Int16* data)//also see alternative bellow
{
    Int32 i;
    Int16 s[4];

    //
    // horizontal
    //
    for(i = 0 ; i < 4 ; i ++)
    {
        s[0] = data[i * 4 + 0] + data[i * 4 + 3];
        s[3] = data[i * 4 + 0] - data[i * 4 + 3];
        s[1] = data[i * 4 + 1] + data[i * 4 + 2];
        s[2] = data[i * 4 + 1] - data[i * 4 + 2];

        data[i * 4 + 0] = s[0] + s[1];
        data[i * 4 + 2] = s[0] - s[1];
        data[i * 4 + 1] = (s[3] << 1) + s[2];
        data[i * 4 + 3] = s[3] - (s[2] << 1);
    }

    //
    // vertical
    //
    for(i = 0 ; i < 4 ; i ++)
    {
        s[0] = data[0 * 4 + i] + data[3 * 4 + i];
        s[3] = data[0 * 4 + i] - data[3 * 4 + i];
        s[1] = data[1 * 4 + i] + data[2 * 4 + i];
        s[2] = data[1 * 4 + i] - data[2 * 4 + i];

        data[0 * 4 + i] = s[0] + s[1];
        data[2 * 4 + i] = s[0] - s[1];
        data[1 * 4 + i] = (s[3] << 1) + s[2];
        data[3 * 4 + i] = s[3] - (s[2] << 1);
    }

}

void 
dct4x4dc_c(Int16* data)
{
    Int32 i;
    Int16 s[4];

    for(i = 0 ; i < 4 ; i ++)
    {
        s[0] = data[i * 4 + 0] + data[i * 4 + 3];
        s[3] = data[i * 4 + 0] - data[i * 4 + 3];
        s[1] = data[i * 4 + 1] + data[i * 4 + 2];
        s[2] = data[i * 4 + 1] - data[i * 4 + 2];

        data[i * 4 + 0] = s[0] + s[1];
        data[i * 4 + 2] = s[0] - s[1];
        data[i * 4 + 1] = s[3] + s[2];
        data[i * 4 + 3] = s[3] - s[2];
    }

    for(i = 0 ; i < 4 ; i ++)
    {
        s[0] = data[0 * 4 + i] + data[3 * 4 + i];
        s[3] = data[0 * 4 + i] - data[3 * 4 + i];
        s[1] = data[1 * 4 + i] + data[2 * 4 + i];
        s[2] = data[1 * 4 + i] - data[2 * 4 + i];

        data[0 * 4 + i] = (s[0] + s[1] + 1) >> 1;
        data[2 * 4 + i] = (s[0] - s[1] + 1) >> 1;
        data[1 * 4 + i] = (s[3] + s[2] + 1) >> 1;
        data[3 * 4 + i] = (s[3] - s[2] + 1) >> 1;
    }
}

void 
dct2x2dc_c(Int16* data)
{   
    Int16 s[4];

    s[0] = data[0];
    s[1] = data[1];
    s[2] = data[2];
    s[3] = data[3];

    data[0] = s[0] + s[2] + s[1] + s[3];
    data[1] = s[0] + s[2] - s[1] - s[3];
    data[2] = s[0] - s[2] + s[1] - s[3];
    data[3] = s[0] - s[2] - s[1] + s[3];
}

void
idct4x4_c(Int16* data)
{
    Int32 i;
    Int16 s[4];

    for (i = 0; i < 4; i ++)
    {
        s[0] = data[i * 4 + 0] + data[i * 4 + 2];
        s[1] = data[i * 4 + 0] - data[i * 4 + 2];
        s[2] = (data[i * 4 + 1] >> 1) - data[i * 4 + 3];
        s[3] = data[i * 4 + 1] + (data[i * 4 + 3] >> 1);

        data[i * 4 + 0] = s[0] + s[3];
        data[i * 4 + 3] = s[0] - s[3];
        data[i * 4 + 1] = s[1] + s[2];
        data[i * 4 + 2] = s[1] - s[2];
    }

    for (i = 0; i < 4; i ++)
    {
        s[0] = data[0 * 4 + i] + data[2 * 4 + i];
        s[1] = data[0 * 4 + i] - data[2 * 4 + i];
        s[2] = (data[1 * 4 + i] >> 1) - data[3 * 4 + i];
        s[3] = data[1 * 4 + i] + (data[3 * 4 + i] >> 1);

        data[0 * 4 + i] = (s[0] + s[3] + 32) >> 6;
        data[3 * 4 + i] = (s[0] - s[3] + 32) >> 6;
        data[1 * 4 + i] = (s[1] + s[2] + 32) >> 6;
        data[2 * 4 + i] = (s[1] - s[2] + 32) >> 6;
    }
}

void 
idct4x4dc_c(Int16* data)
{
    Int32 i;
    Int16 s[4];

    for (i = 0; i < 4; i ++)
    {
        s[0] = data[i * 4 + 0] + data[i * 4 + 2];
        s[1] = data[i * 4 + 0] - data[i * 4 + 2];
        s[2] = data[i * 4 + 1] - data[i * 4 + 3];
        s[3] = data[i * 4 + 1] + data[i * 4 + 3];

        data[i * 4 + 0] = s[0] + s[3];
        data[i * 4 + 3] = s[0] - s[3];
        data[i * 4 + 1] = s[1] + s[2];
        data[i * 4 + 2] = s[1] - s[2];
    }

    for (i = 0; i < 4; i ++)
    {
        s[0] = data[0 * 4 + i] + data[2 * 4 + i];
        s[1] = data[0 * 4 + i] - data[2 * 4 + i];
        s[2] = data[1 * 4 + i] - data[3 * 4 + i];
        s[3] = data[1 * 4 + i] + data[3 * 4 + i];

        data[0 * 4 + i] = s[0] + s[3];
        data[3 * 4 + i] = s[0] - s[3];
        data[1 * 4 + i] = s[1] + s[2];
        data[2 * 4 + i] = s[1] - s[2];
    }
}

void 
idct2x2dc_c(Int16* data)
{
    Int16 s[4];

    s[0] = data[0];
    s[1] = data[1];
    s[2] = data[2];
    s[3] = data[3];

    data[0] = s[0] + s[2] + s[1] + s[3];
    data[1] = s[0] + s[2] - s[1] - s[3];
    data[2] = s[0] - s[2] + s[1] - s[3];
    data[3] = s[0] - s[2] - s[1] + s[3];
}

///////////////////////////////////////////////////////////
// Quant & IQuant
void
quant4x4_c(Int16* data, const Int32 Qp, Int32 is_intra)
{
    Int32 qbits;
    Int32 mf_index;
    Int32 i;
    Int32 f;

	qbits    = 15 + Qp / 6;
	mf_index = Qp % 6;
	f = (1 << qbits) / (is_intra ? 3 : 6);

    for(i = 0 ; i < 16 ; i ++)
    {
		//printf("%d\n", data[i]);
        if (data[i] > 0)
            data[i] = (Int16)((data[i] * quant[mf_index][i] + f) >> qbits);
        else
            data[i] = (Int16)(-((-(data[i] * quant[mf_index][i]) + f) >> qbits));
    }
}

///////////////////////////////////////////////////////////
// Quant & IQuant
void quant4x4_c_bc(Int16* data, const Int32 Qp, Int32 is_intra)
{
    const Int32 qbits    = 15 + Qp / 6;
    const Int32 mf_index = Qp % 6;
    Int32 i;
    const Int32 f = (1 << qbits) / (is_intra ? 3 : 6);

    for(i = 0 ; i < 16 ; i ++)
    {
        if (data[i] > 0)
            data[i] = (Int16)((data[i] * quant[mf_index][i] + f) >> qbits);
        else
            data[i] = (Int16)(-((-(data[i] * quant[mf_index][i]) + f) >> qbits));
    }
}

void
quant4x4dc_c(Int16* data, const Int32 Qp)
{
    const Int32 qbits    = 15 + Qp / 6;
    const Int32 mf_index = Qp % 6;
    const Int32 mf00 = quant[mf_index][0];
    const Int32 f2 = (2 << qbits) / 3;	// Only 16x16 intra mode
    Int32 i;

    for(i = 0 ; i < 16 ; i ++)
    {
        if (data[i] > 0)
            data[i] = (Int16)((data[i] * mf00 + f2) >> (qbits + 1));
        else
            data[i] = (Int16)(-((-(data[i] * mf00) + f2) >> (qbits + 1)));
    }
}

void
quant2x2dc_c(Int16* data, const Int32 Qp, Int32 is_intra)
{
    const Int32 qbits    = 15 + Qp / 6;
    const Int32 mf_index = Qp % 6;
    const Int32 mf00 = quant[mf_index][0];
    const Int32 f2 = (2 << qbits) / (is_intra ? 3 : 6);
    Int32 i;

    for(i = 0 ; i < 4 ; i ++)
    {
        if (data[i] > 0)
            data[i] = (Int16)((data[i] * mf00 + f2) >> (qbits + 1));
        else
            data[i] = (Int16)(-((-(data[i] * mf00) + f2) >> (qbits + 1)));
    }
}

//#############################################################################

void iquant4x4_c_old(Int16* data, const Int32 Qp)
{
    const Int32 qbits = Qp / 6;
    const Int32 index_mf = Qp % 6;
    Int32 i;

    for(i = 0 ; i < 16 ; i ++)
    {
        data[i] = (data[i] * dequant[index_mf][i]) << qbits;
    }    
}

void iquant4x4_c(Int16* data, const Int32 i_qscale)
{
	const int i_qbits = i_qscale/6 - 4;
	const Int32 mf_index = i_qscale % 6;
	Int32 i;
	//static int cntr =1;

	for(i = 0 ; i < 16 ; i ++)
    {     
		if( i_qbits >= 0 )
		{        
			data[i] = (data[i] * dequant[mf_index][i] * 16) << i_qbits;        
		}
		else
		{
			const int f = 1 << (-i_qbits-1);               
			data[i] = (data[i] * dequant[mf_index][i] * 16 + f) >> (-i_qbits);        
		}
	}	
}


void iquant4x4dc_c(Int16* data, const Int32 i_qscale)
{
	const int i_qbits = i_qscale/6 - 6;
	const Int32 mf_index = i_qscale % 6;
	Int32 i;

	if( i_qbits >= 0 )
    {
        for(i = 0 ; i < 16 ; i ++)
        {            
			data[i] = (data[i] * dequant[mf_index][0] * 16) << i_qbits;
        }
    }
    else
    {
        const int f = 1 << (-i_qbits-1);
        for(i = 0 ; i < 16 ; i ++)
        {           
			data[i] = (data[i] * dequant[mf_index][0] * 16 + f) >> (-i_qbits);
        }
    }
}

void iquant2x2dc_c(Int16* data, const Int32 i_qscale)
{
    const Int32 i_qbits = i_qscale / 6 - 5;
	const Int32 mf_index = i_qscale % 6;
	Int32 i;

    if (i_qbits >= 0 )
    {
		for(i = 0 ; i < 4 ; i ++)
        {
			data[i] = (data[i] * dequant[mf_index][0] * 16) << i_qbits;
		}  
    }
    else
    {
		// chroma DC is truncated, not rounded        
		for(i = 0 ; i < 4 ; i ++)
        {
			data[i] = (data[i] * dequant[mf_index][0] * 16) >> (-i_qbits);
		}  
    }
}


//########################### debug #############################

void print_yuv_in_hw_order(pBlockInfo_t pBlockInfo)
{
	int i;

	for(i = 0; i<16; i++)
		FPRINTF_MC(pBlockInfo->pEncFrame->dbg_files.residual,"%d\n", pBlockInfo->block8x8Buff.dct4x4inp[5][i]);	    
	for(i = 0; i<16; i++)
		FPRINTF_MC(pBlockInfo->pEncFrame->dbg_files.residual,"%d\n", pBlockInfo->block8x8Buff.dct4x4inp[7][i]);	
	for(i = 0; i<16; i++)
		FPRINTF_MC(pBlockInfo->pEncFrame->dbg_files.residual,"%d\n", pBlockInfo->block8x8Buff.dct4x4inp[13][i]);	
	for(i = 0; i<16; i++)
		FPRINTF_MC(pBlockInfo->pEncFrame->dbg_files.residual,"%d\n", pBlockInfo->block8x8Buff.dct4x4inp[15][i]);	
	for(i = 0; i<16; i++)
		FPRINTF_MC(pBlockInfo->pEncFrame->dbg_files.residual,"%d\n", pBlockInfo->block8x8Buff.dct4x4inp[4][i]);	
	for(i = 0; i<16; i++)
		FPRINTF_MC(pBlockInfo->pEncFrame->dbg_files.residual,"%d\n", pBlockInfo->block8x8Buff.dct4x4inp[6][i]);	
	for(i = 0; i<16; i++)
		FPRINTF_MC(pBlockInfo->pEncFrame->dbg_files.residual,"%d\n", pBlockInfo->block8x8Buff.dct4x4inp[12][i]);	
	for(i = 0; i<16; i++)
		FPRINTF_MC(pBlockInfo->pEncFrame->dbg_files.residual,"%d\n", pBlockInfo->block8x8Buff.dct4x4inp[14][i]);	
	for(i = 0; i<16; i++)
		FPRINTF_MC(pBlockInfo->pEncFrame->dbg_files.residual,"%d\n", pBlockInfo->block8x8Buff.dct4x4inp[1][i]);	
	for(i = 0; i<16; i++)
		FPRINTF_MC(pBlockInfo->pEncFrame->dbg_files.residual,"%d\n", pBlockInfo->block8x8Buff.dct4x4inp[3][i]);	
	for(i = 0; i<16; i++)
		FPRINTF_MC(pBlockInfo->pEncFrame->dbg_files.residual,"%d\n", pBlockInfo->block8x8Buff.dct4x4inp[9][i]);	
	for(i = 0; i<16; i++)
		FPRINTF_MC(pBlockInfo->pEncFrame->dbg_files.residual,"%d\n", pBlockInfo->block8x8Buff.dct4x4inp[11][i]);	
	for(i = 0; i<16; i++)
		FPRINTF_MC(pBlockInfo->pEncFrame->dbg_files.residual,"%d\n", pBlockInfo->block8x8Buff.dct4x4inp[0][i]);	
	for(i = 0; i<16; i++)
		FPRINTF_MC(pBlockInfo->pEncFrame->dbg_files.residual,"%d\n", pBlockInfo->block8x8Buff.dct4x4inp[2][i]);	
	for(i = 0; i<16; i++)
		FPRINTF_MC(pBlockInfo->pEncFrame->dbg_files.residual,"%d\n", pBlockInfo->block8x8Buff.dct4x4inp[8][i]);	
	for(i = 0; i<16; i++)
		FPRINTF_MC(pBlockInfo->pEncFrame->dbg_files.residual,"%d\n", pBlockInfo->block8x8Buff.dct4x4inp[10][i]);	

	//chroma
	for(i = 0; i<16; i++)
		FPRINTF_MC(pBlockInfo->pEncFrame->dbg_files.residual,"%d\n", pBlockInfo->block8x8Buff.dct4x4inp[16][i]);	
	for(i = 0; i<16; i++)
		FPRINTF_MC(pBlockInfo->pEncFrame->dbg_files.residual,"%d\n", pBlockInfo->block8x8Buff.dct4x4inp[17][i]);	
	for(i = 0; i<16; i++)
		FPRINTF_MC(pBlockInfo->pEncFrame->dbg_files.residual,"%d\n", pBlockInfo->block8x8Buff.dct4x4inp[18][i]);	
	for(i = 0; i<16; i++)
		FPRINTF_MC(pBlockInfo->pEncFrame->dbg_files.residual,"%d\n", pBlockInfo->block8x8Buff.dct4x4inp[19][i]);	
	for(i = 0; i<16; i++)
		FPRINTF_MC(pBlockInfo->pEncFrame->dbg_files.residual,"%d\n", pBlockInfo->block8x8Buff.dct4x4inp[20][i]);	
	for(i = 0; i<16; i++)
		FPRINTF_MC(pBlockInfo->pEncFrame->dbg_files.residual,"%d\n", pBlockInfo->block8x8Buff.dct4x4inp[21][i]);	
	for(i = 0; i<16; i++)
		FPRINTF_MC(pBlockInfo->pEncFrame->dbg_files.residual,"%d\n", pBlockInfo->block8x8Buff.dct4x4inp[22][i]);	
	for(i = 0; i<16; i++)
		FPRINTF_MC(pBlockInfo->pEncFrame->dbg_files.residual,"%d\n", pBlockInfo->block8x8Buff.dct4x4inp[23][i]);	

}

void get_4x4_blocks_dbg(pBlockInfo_t pBlockInfo)//mb_encoding
{
	Int8 x, y, i, blk4x4cnt;  
	Int8 start_x, start_y;  
	
	//Int16 **dct = dct1;
	 
	//get 8x8 into 4-4x4 blocks
	start_x   = 0;
	start_y   = 0;	
	i         = 0;
	blk4x4cnt = 0;
	for (y=start_y; y < start_y+4; y++ ) 
	{
		for (x=start_x; x < start_x+4; x++ ) 
		{
			pBlockInfo->block8x8Buff.dct4x4inp[(pBlockInfo->blockIdx * 4) + blk4x4cnt][i++] = pBlockInfo->block8x8Buff.tcReconst[pBlockInfo->blockIdx][y][x];
		}
	}

	start_x   = 4;
	start_y   = 0;	
	i         = 0;
	blk4x4cnt = 1;
	for (y=start_y; y < start_y+4; y++ ) 
	{
		for (x=start_x; x < start_x+4; x++ ) 
		{
			pBlockInfo->block8x8Buff.dct4x4inp[(pBlockInfo->blockIdx * 4) + blk4x4cnt][i++] = pBlockInfo->block8x8Buff.tcReconst[pBlockInfo->blockIdx][y][x];
		}
	}

	start_x   = 0;
	start_y   = 4;	
	i         = 0;
	blk4x4cnt = 2;
	for (y=start_y; y < start_y+4; y++ ) 
	{
		for (x=start_x; x < start_x+4; x++ ) 
		{
			pBlockInfo->block8x8Buff.dct4x4inp[(pBlockInfo->blockIdx * 4) + blk4x4cnt][i++] = pBlockInfo->block8x8Buff.tcReconst[pBlockInfo->blockIdx][y][x];
		}
	}
	
	start_x   = 4;
	start_y   = 4;	
	i         = 0;
	blk4x4cnt = 3;
	for (y=start_y; y < start_y+4; y++ ) 
	{
		for (x=start_x; x < start_x+4; x++ ) 
		{
			pBlockInfo->block8x8Buff.dct4x4inp[(pBlockInfo->blockIdx * 4) + blk4x4cnt][i++] = pBlockInfo->block8x8Buff.tcReconst[pBlockInfo->blockIdx][y][x];
		}
	}
	printf("");
}

void print_reconstructed(pBlockInfo_t pBlockInfo)
{
	int i;

	//luma
	for(i = 0; i<16; i++)
		FPRINTF_MC(pBlockInfo->pEncFrame->dbg_files.reconstructed,"%d\n", pBlockInfo->block8x8Buff.dct4x4inp[5][i]);			
	for(i = 0; i<16; i++)
		FPRINTF_MC(pBlockInfo->pEncFrame->dbg_files.reconstructed,"%d\n", pBlockInfo->block8x8Buff.dct4x4inp[7][i]);	
	for(i = 0; i<16; i++)
		FPRINTF_MC(pBlockInfo->pEncFrame->dbg_files.reconstructed,"%d\n", pBlockInfo->block8x8Buff.dct4x4inp[13][i]);	
	for(i = 0; i<16; i++)
		FPRINTF_MC(pBlockInfo->pEncFrame->dbg_files.reconstructed,"%d\n", pBlockInfo->block8x8Buff.dct4x4inp[15][i]);	
	for(i = 0; i<16; i++)
		FPRINTF_MC(pBlockInfo->pEncFrame->dbg_files.reconstructed,"%d\n", pBlockInfo->block8x8Buff.dct4x4inp[4][i]);	
	for(i = 0; i<16; i++)
		FPRINTF_MC(pBlockInfo->pEncFrame->dbg_files.reconstructed,"%d\n", pBlockInfo->block8x8Buff.dct4x4inp[6][i]);	
	for(i = 0; i<16; i++)
		FPRINTF_MC(pBlockInfo->pEncFrame->dbg_files.reconstructed,"%d\n", pBlockInfo->block8x8Buff.dct4x4inp[12][i]);	
	for(i = 0; i<16; i++)
		FPRINTF_MC(pBlockInfo->pEncFrame->dbg_files.reconstructed,"%d\n", pBlockInfo->block8x8Buff.dct4x4inp[14][i]);	
	for(i = 0; i<16; i++)
		FPRINTF_MC(pBlockInfo->pEncFrame->dbg_files.reconstructed,"%d\n", pBlockInfo->block8x8Buff.dct4x4inp[1][i]);	
	for(i = 0; i<16; i++)
		FPRINTF_MC(pBlockInfo->pEncFrame->dbg_files.reconstructed,"%d\n", pBlockInfo->block8x8Buff.dct4x4inp[3][i]);	
	for(i = 0; i<16; i++)
		FPRINTF_MC(pBlockInfo->pEncFrame->dbg_files.reconstructed,"%d\n", pBlockInfo->block8x8Buff.dct4x4inp[9][i]);	
	for(i = 0; i<16; i++)
		FPRINTF_MC(pBlockInfo->pEncFrame->dbg_files.reconstructed,"%d\n", pBlockInfo->block8x8Buff.dct4x4inp[11][i]);	
	for(i = 0; i<16; i++)
		FPRINTF_MC(pBlockInfo->pEncFrame->dbg_files.reconstructed,"%d\n", pBlockInfo->block8x8Buff.dct4x4inp[0][i]);	
	for(i = 0; i<16; i++)
		FPRINTF_MC(pBlockInfo->pEncFrame->dbg_files.reconstructed,"%d\n", pBlockInfo->block8x8Buff.dct4x4inp[2][i]);	
	for(i = 0; i<16; i++)
		FPRINTF_MC(pBlockInfo->pEncFrame->dbg_files.reconstructed,"%d\n", pBlockInfo->block8x8Buff.dct4x4inp[8][i]);	
	for(i = 0; i<16; i++)
		FPRINTF_MC(pBlockInfo->pEncFrame->dbg_files.reconstructed,"%d\n", pBlockInfo->block8x8Buff.dct4x4inp[10][i]);	

	//chroma
	for(i = 0; i<16; i++)
		FPRINTF_MC(pBlockInfo->pEncFrame->dbg_files.reconstructed,"%d\n", pBlockInfo->block8x8Buff.dct4x4inp[16][i]);	
	for(i = 0; i<16; i++)
		FPRINTF_MC(pBlockInfo->pEncFrame->dbg_files.reconstructed,"%d\n", pBlockInfo->block8x8Buff.dct4x4inp[17][i]);	
	for(i = 0; i<16; i++)
		FPRINTF_MC(pBlockInfo->pEncFrame->dbg_files.reconstructed,"%d\n", pBlockInfo->block8x8Buff.dct4x4inp[18][i]);	
	for(i = 0; i<16; i++)
		FPRINTF_MC(pBlockInfo->pEncFrame->dbg_files.reconstructed,"%d\n", pBlockInfo->block8x8Buff.dct4x4inp[19][i]);	
	for(i = 0; i<16; i++)
		FPRINTF_MC(pBlockInfo->pEncFrame->dbg_files.reconstructed,"%d\n", pBlockInfo->block8x8Buff.dct4x4inp[20][i]);	
	for(i = 0; i<16; i++)
		FPRINTF_MC(pBlockInfo->pEncFrame->dbg_files.reconstructed,"%d\n", pBlockInfo->block8x8Buff.dct4x4inp[21][i]);	
	for(i = 0; i<16; i++)
		FPRINTF_MC(pBlockInfo->pEncFrame->dbg_files.reconstructed,"%d\n", pBlockInfo->block8x8Buff.dct4x4inp[22][i]);	
	for(i = 0; i<16; i++)
		FPRINTF_MC(pBlockInfo->pEncFrame->dbg_files.reconstructed,"%d\n", pBlockInfo->block8x8Buff.dct4x4inp[23][i]);	
}
