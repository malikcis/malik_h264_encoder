/*****************************************************************************  
 *  
 *  H264 AVC CODEC  
 *  
 *  Copyright(C) 2020 llcc <malik.cisse@abateck.com>  
 *  
 *  This program is free software ; you can redistribute it and/or modify  
 *  it under the terms of the GNU General Public License as published by  
 *  the Free Software Foundation ; either version 2 of the License, or  
 *  (at your option) any later version.  
 *  
 *  This program is distributed in the hope that it will be useful,  
 *  but WITHOUT ANY WARRANTY ; without even the implied warranty of  
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the  
 *  GNU General Public License for more details.  
 *  
 *  You should have received a copy of the GNU General Public License  
 *  along with this program ; if not, write to the Free Software  
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA  
 *  
 ****************************************************************************/ 
#include "mb_buffer.h"


/* to mask the n least significant bits of an integer */

static unsigned int buff_mask[33] =
{
  0x00000000, 0x00000001, 0x00000003, 0x00000007,
  0x0000000f, 0x0000001f, 0x0000003f, 0x0000007f,
  0x000000ff, 0x000001ff, 0x000003ff, 0x000007ff,
  0x00000fff, 0x00001fff, 0x00003fff, 0x00007fff,
  0x0000ffff, 0x0001ffff, 0x0003ffff, 0x0007ffff,
  0x000fffff, 0x001fffff, 0x003fffff, 0x007fffff,
  0x00ffffff, 0x01ffffff, 0x03ffffff, 0x07ffffff,
  0x0fffffff, 0x1fffffff, 0x3fffffff, 0x7fffffff,
  0xffffffff
};


/* static data for pointers and counters */

/* byte pointer to the output bitstream */
pUInt8 buff_byteptr;
/* counter of how many bytes got written to the bitstream */
static Int32 buff_bytecnt; 

/* a one byte temporary buffer */
static UInt8 buff_outbfr;
/* counter of how many unused bits left in the byte buffer */
static Int32 buff_outcnt;

void buffer_Init(void *buffer)
{
	buff_byteptr = (UInt8 *)buffer;
	buff_bytecnt = 0;
	buff_outbfr = 0;
	buff_outcnt = 8;
}

void buffer_PutBits(UInt32 n, UInt32 val)
{
	Int32 diff;

	while ((diff = n - buff_outcnt) >= 0) { /* input is longer than what is left in the buffer */
		buff_outbfr |= (UInt8)(val >> diff);
		/* note that the first byte of the integer is the least significant byte */
		n = diff;
		val &= buff_mask[n];

		*(buff_byteptr ++) = buff_outbfr;
		buff_bytecnt++;
		buff_outbfr = 0;
		buff_outcnt = 8;
	}

	if (n > 0) { /* input is short enough to fit in what is left in the buffer */
		buff_outbfr |= (UInt8)(val << (-diff));
		buff_outcnt -= n;
	}
}


UInt32 buffer_Close()
{
	while (buff_outcnt != 8) buffer_PutBits(1, 0);
	return buff_bytecnt;
}
