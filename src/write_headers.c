/*****************************************************************************  
 *  
 *  H264 AVC CODEC  
 *  
 *  Copyright(C) 2020 llcc <malik.cisse@abateck.com>  
 *  
 *  This program is free software ; you can redistribute it and/or modify  
 *  it under the terms of the GNU General Public License as published by  
 *  the Free Software Foundation ; either version 2 of the License, or  
 *  (at your option) any later version.  
 *  
 *  This program is distributed in the hope that it will be useful,  
 *  but WITHOUT ANY WARRANTY ; without even the implied warranty of  
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the  
 *  GNU General Public License for more details.  
 *  
 *  You should have received a copy of the GNU General Public License  
 *  along with this program ; if not, write to the Free Software  
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA  
 *  
 ****************************************************************************/ 
#include "write_headers.h"
#include "bitstream.h"

#define ENABLE_DEBUG 1
#define USE_SLICE_QP_DELTA 1
#define MORE_RBSP_DATA 0
#define USE_CROPPING 0

#if ENABLE_DEBUG
#include "debug.h"
#endif

void writeSequenceHeader(pEncFrame_t pEncFrame)
{	
	int eg_len, eg_data;

	pEncFrame->log2_max_pic_order_cnt_lsb_minus4 = 12;
	pEncFrame->log2_max_frame_num_minus4         = 12;
	pEncFrame->pic_order_cnt_type                = 0;

	nal_unit_write(3, NAL_SEQ_SET);

	//normally sce will start here
	//seq_parameter_set_rbsp starts here
    
    //profile_idc
	if(pEncFrame->pInputData->useHighProfile)
		bitstream_PutBits(8, PROFILE_HIGH);
	else
		bitstream_PutBits(8, PROFILE_MAIN);

	//constraint_set0_flag
	bitstream_PutBits(1, 0);

	//constraint_set1_flag
	bitstream_PutBits(1, 0);

	//constraint_set2_flag
	bitstream_PutBits(1, 0);

	//constraint_set3_flag
	bitstream_PutBits(1, 0);

	//reserved_zero_4bits
	bitstream_PutBits(4, 0);
	
	//level_idc
	bitstream_PutBits(8, pEncFrame->pInputData->profile_and_level*10);

	//seq_parameter_set_id ue(v)
	eg_write_ue(0, &eg_len, &eg_data);
	bitstream_PutBits(eg_len, eg_data);

	if(pEncFrame->pInputData->useHighProfile)
	{
		//chroma_format_idc ue(v)
		//bitstream_PutBits(3, 2);//1: 4:2:0 => code: 010
		eg_write_ue(1, &eg_len, &eg_data);
		bitstream_PutBits(eg_len, eg_data);

		//bit_depth_luma_minus8 ue(v)
		//bitstream_PutBits(1, 1);
		eg_write_ue(0, &eg_len, &eg_data);
		bitstream_PutBits(eg_len, eg_data);

		//bit_depth_chroma_minus8 ue(v)
		eg_write_ue(0, &eg_len, &eg_data);
		bitstream_PutBits(eg_len, eg_data);

		//qpprime_y_zero_transform_bypass_flag
		bitstream_PutBits(1, 0);

		//seq_scaling_matrix_present_flag
		bitstream_PutBits(1, 0);
	}

	//log2_max_frame_num_minus4 
	eg_write_ue(pEncFrame->log2_max_frame_num_minus4, &eg_len, &eg_data);
	bitstream_PutBits(eg_len, eg_data);

	//pic_order_cnt_type
	eg_write_ue(pEncFrame->pic_order_cnt_type, &eg_len, &eg_data);
	bitstream_PutBits(eg_len, eg_data);

	if(pEncFrame->pic_order_cnt_type == 0)
	{
		//log2_max_pic_order_cnt_lsb_minus4
		eg_write_ue(pEncFrame->log2_max_pic_order_cnt_lsb_minus4, &eg_len, &eg_data);
		bitstream_PutBits(eg_len, eg_data);
	}

	//num_ref_frames
	eg_write_ue(1, &eg_len, &eg_data);
	bitstream_PutBits(eg_len, eg_data);

	//gaps_in_frame_num_value_allowed_flag 0 u(1)
	bitstream_PutBits(1, 0);

	//pic_width_in_mbs_minus1 0 ue(v)
	eg_write_ue((pEncFrame->pInputData->frame_width)/16 - 1, &eg_len, &eg_data);
	bitstream_PutBits(eg_len, eg_data);	

	//pic_height_in_map_units_minus1 0
	eg_write_ue((pEncFrame->pInputData->frame_height)/16 - 1, &eg_len, &eg_data);
	bitstream_PutBits(eg_len, eg_data);	

	//frame_mbs_only_flag 0 u(1)
	bitstream_PutBits(1, 1);
	
	//direct_8x8_inference_flag 0 u(1)
	bitstream_PutBits(1, 0);

#if USE_CROPPING
	//frame_cropping_flag 0 u(1)
	bitstream_PutBits(1, 1);

	eg_write_ue(0, &eg_len, &eg_data);
	bitstream_PutBits(eg_len, eg_data);

	eg_write_ue(0, &eg_len, &eg_data);
	bitstream_PutBits(eg_len, eg_data);

	eg_write_ue(0, &eg_len, &eg_data);
	bitstream_PutBits(eg_len, eg_data);

	eg_write_ue(4, &eg_len, &eg_data);
	bitstream_PutBits(eg_len, eg_data);
#else
	//frame_cropping_flag 0 u(1)
	bitstream_PutBits(1, 0);
#endif
	//vui_parameters_present_flag
	bitstream_PutBits(1, 0);
   
    //Write stop bit
    bitstream_PutBits(1, 1);//rbsp_stop_one_bit. See P.72
}//seq_header

void writePictureHeader(pEncFrame_t pEncFrame)
{
	int pic_init_qp_minus26;
	int eg_len, eg_data;
	int pic_init_qs_minus26;

	nal_unit_write(3, NAL_PIC_SET);

	//pic_parameter_set_id 1 ue(v)
	eg_write_ue(0, &eg_len, &eg_data);
	bitstream_PutBits(eg_len, eg_data);	 

	//seq_parameter_set_id 1 ue(v)
	eg_write_ue(0, &eg_len, &eg_data);
	bitstream_PutBits(eg_len, eg_data);	

	//entropy_coding_mode_flag 1 u(1)
	bitstream_PutBits(1, 0);//CAVLC

	//pic_order_present_flag 1 u(1)
	bitstream_PutBits(1, 0);

	//num_slice_groups_minus1 1 ue(v)
	eg_write_ue(0, &eg_len, &eg_data);
	bitstream_PutBits(eg_len, eg_data);	

	//num_ref_idx_l0_active_minus1 1 ue(v)
	eg_write_ue(0, &eg_len, &eg_data);
	bitstream_PutBits(eg_len, eg_data);	

	//num_ref_idx_l1_active_minus1
	eg_write_ue(0, &eg_len, &eg_data);
	bitstream_PutBits(eg_len, eg_data);	

	//weighted_pred_flag 1 u(1)
	bitstream_PutBits(1, 0);

	//weighted_bipred_idc 1 u(2)
	bitstream_PutBits(2, 0);

	//pic_init_qp_minus26 /* relative to 26 */ 1 se(v)
#if USE_SLICE_QP_DELTA
	pic_init_qp_minus26 = 0;
#else
	pic_init_qp_minus26 = (int)pEncFrame->PQIndex - 26;//you can save up to 11 bits here if you mimic JM10.0. also see slice_qp_delta
#endif
	eg_write_se(pic_init_qp_minus26, &eg_len, &eg_data);	
	bitstream_PutBits(eg_len, eg_data);

	//pic_init_qs_minus26 /* relative to 26 */ 1 se(v)
#if USE_SLICE_QP_DELTA
	pic_init_qs_minus26 = 0;
#else
	pic_init_qs_minus26 = (int)pEncFrame->PQIndex - 26;//you can save up to 11 bits here if you mimic JM10.0. also see slice_qp_delta
#endif
	eg_write_se(pic_init_qs_minus26, &eg_len, &eg_data);
	bitstream_PutBits(eg_len, eg_data);

	//chroma_qp_index_offset 1 se(v)
	eg_write_se(0, &eg_len, &eg_data);
	bitstream_PutBits(eg_len, eg_data);

	//deblocking_filter_control_present_flag 1 u(1)
	if(pEncFrame->pInputData->disableFilter)
		bitstream_PutBits(1, 1);
	else
		bitstream_PutBits(1, 0);//default values for inloop filter are taken

	//constrained_intra_pred_flag 1 u(1)
	bitstream_PutBits(1, 0);

	//redundant_pic_cnt_present_flag 1 
	bitstream_PutBits(1, 0);

#if MORE_RBSP_DATA
	//transform_8x8_mode_flag 1 u(1)
	bitstream_PutBits(1, 0);

	//pic_scaling_matrix_present_flag 1 u(1)
	bitstream_PutBits(1, 0);

	//second_chroma_qp_index_offset
	eg_write_se(0, &eg_len, &eg_data);
	bitstream_PutBits(eg_len, eg_data);
#endif

	//Write stop bit
    bitstream_PutBits(1, 1);//rbsp_stop_one_bit. See P.72
}

void writeSliceHeader(pEncFrame_t pEncFrame)
{	
	int eg_len, eg_data;
	int slice_qp_delta;

	pEncFrame->pic_order_cnt_lsb = pEncFrame->poc % (1 << (pEncFrame->log2_max_pic_order_cnt_lsb_minus4 + 4));

	if(pEncFrame->frameType == I)//IDR picture
		nal_unit_write(3, NAL_SLICE_IDR);
	else
		nal_unit_write(3, NAL_SLICE_NOPART);

	set_scep(1);//activate start code emulation prevention

    //first_mb_in_slice
    eg_write_ue(0, &eg_len, &eg_data);
	bitstream_PutBits(eg_len, eg_data);

#if 1
	//slice_type
	if (pEncFrame->frameType == I)//IDR picture
		eg_write_ue(SLICE_I, &eg_len, &eg_data);
	else//P picture
		eg_write_ue(SLICE_P, &eg_len, &eg_data);	
	bitstream_PutBits(eg_len, eg_data);
#else//JM. why and when is this needed
	//slice_type
	/*slice_type values in the range 5..9 specify, in addition to the coding type of the current slice, that all other slices of the
	current coded picture shall have a value of slice_type equal to the current value of slice_type or equal to the current value
	of slice_type � 5.
	When nal_unit_type is equal to 5 (IDR picture), slice_type shall be equal to 2, 4, 7, or 9.
	When num_ref_frames is equal to 0, slice_type shall be equal to 2, 4, 7, or 9.*/
	int same_slicetype_for_whole_frame = 5;
	if (pEncFrame->frameType == I)//IDR picture
		eg_write_ue(SLICE_I + same_slicetype_for_whole_frame, &eg_len, &eg_data);
	else//P picture
		eg_write_ue(SLICE_P + same_slicetype_for_whole_frame, &eg_len, &eg_data);	
	bitstream_PutBits(eg_len, eg_data);
#endif

	//pic_parameter_set_id
	eg_write_ue(0, &eg_len, &eg_data);
	bitstream_PutBits(eg_len, eg_data); 

	//frame_num	
	bitstream_PutBits(pEncFrame->log2_max_frame_num_minus4 + 4, pEncFrame->h264_frame_num);//because of log2_max_frame_num_minus4 == 0

	if (pEncFrame->frameType == I)//IDR picture
	{
		//idr_pic_id
		pEncFrame->idr_pic_id = (pEncFrame->idr_pic_id + 1) %2;
		eg_write_ue(pEncFrame->idr_pic_id, &eg_len, &eg_data);
		bitstream_PutBits(eg_len, eg_data); 
	}

	if(pEncFrame->pic_order_cnt_type == 0)
	{
		//pic_order_cnt_lsb
		bitstream_PutBits(pEncFrame->log2_max_pic_order_cnt_lsb_minus4 + 4, pEncFrame->pic_order_cnt_lsb);	
	}
	

	//dec_ref_pic_marking( )
	if (pEncFrame->frameType == I)//IDR picture
	{
		//no_output_of_prior_pics_flag 2 | 5 u(1)
		bitstream_PutBits(1, 0);

		//long_term_reference_flag 2 | 5 u(1)
		bitstream_PutBits(1, 0);
	}
	else//P picture
	{	
		bitstream_PutBits(1, 0);//num_ref_idx_active_override_flag
	
		bitstream_PutBits(1, 0);//ref_pic_list_reordering_flag_l0

		//adaptive_ref_pic_marking_mode_flag
		bitstream_PutBits(1, 0);
	}

	//slice_qp_delta: SliceQPY = 26 + pic_init_qp_minus26 + slice_qp_delta
#if USE_SLICE_QP_DELTA
	slice_qp_delta = (int)pEncFrame->PQIndex - 26 - 0;
#else
	slice_qp_delta = (int)pEncFrame->PQIndex - (int)pEncFrame->PQIndexInit;	
#endif

	eg_write_se(slice_qp_delta, &eg_len, &eg_data);
	bitstream_PutBits(eg_len, eg_data); 

	if(pEncFrame->pInputData->disableFilter)
	{
		//deblocking_filter_control_present_flag 1 ==> must set disable_deblocking_filter_idc
		int disable_deblocking_filter_idc = 1;
		eg_write_ue(disable_deblocking_filter_idc, &eg_len, &eg_data);
		bitstream_PutBits(eg_len, eg_data); 
	}
}

void nal_unit_write(int nal_ref_idc, int nal_unit_type)
{
	//NAL  Start Code
    bitstream_PutBits(32, NAL_SC);//write nal start code

	//forbidden_zero_bit
	bitstream_PutBits(1, 0);

	//nal_ref_idc
	bitstream_PutBits(2, nal_ref_idc);////nal_ref_idc: 0=not used for ref; 1=top used; 2=bottom used; 3=both fields (or frame) used
    
	//nal_unit_type
	bitstream_PutBits(5, nal_unit_type);
}
