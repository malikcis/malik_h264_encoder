/*****************************************************************************  
 *  
 *  H264 AVC CODEC  
 *  
 *  Copyright(C) 2020 llcc <malik.cisse@abateck.com>  
 *  
 *  This program is free software ; you can redistribute it and/or modify  
 *  it under the terms of the GNU General Public License as published by  
 *  the Free Software Foundation ; either version 2 of the License, or  
 *  (at your option) any later version.  
 *  
 *  This program is distributed in the hope that it will be useful,  
 *  but WITHOUT ANY WARRANTY ; without even the implied warranty of  
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the  
 *  GNU General Public License for more details.  
 *  
 *  You should have received a copy of the GNU General Public License  
 *  along with this program ; if not, write to the Free Software  
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA  
 *  
 ****************************************************************************/ 
#include "bitstream.h"

/* to mask the n least significant bits of an integer */

static unsigned int mask[33] =
{
  0x00000000, 0x00000001, 0x00000003, 0x00000007,
  0x0000000f, 0x0000001f, 0x0000003f, 0x0000007f,
  0x000000ff, 0x000001ff, 0x000003ff, 0x000007ff,
  0x00000fff, 0x00001fff, 0x00003fff, 0x00007fff,
  0x0000ffff, 0x0001ffff, 0x0003ffff, 0x0007ffff,
  0x000fffff, 0x001fffff, 0x003fffff, 0x007fffff,
  0x00ffffff, 0x01ffffff, 0x03ffffff, 0x07ffffff,
  0x0fffffff, 0x1fffffff, 0x3fffffff, 0x7fffffff,
  0xffffffff
};


/* static data for pointers and counters */

/* byte pointer to the output bitstream */
pUInt8 byteptr;
/* counter of how many bytes got written to the bitstream */
static Int32 bytecnt; 

/* a one byte temporary buffer */
static UInt8 outbfr;

/* a one byte temporary buffer */
static char outbfr1;

/* a one byte temporary buffer */
static char outbfr2;

static Bool activate_SCEP;//activate start code emulation prevention

static Bool byte_stuffing;

/* counter of how many unused bits left in the byte buffer */
static Int32 outcnt;

void bitstream_Init(void *buffer)
{
	byteptr       = (UInt8 *)buffer;
	bytecnt       = 0;
	outbfr        = 0;
	outcnt        = 8;
	outbfr1       = 0xFF;
	outbfr2       = 0xFF;
}

void set_scep(Bool scep)
{
	activate_SCEP = scep;
}

void bitstream_PutBits(UInt32 n, UInt32 val)
{
	Int32 diff;

	while ( (diff = n - outcnt) >= 0 ) 
	{ /* input is longer than what is left in the buffer */
		outbfr |= (UInt8)(val >> diff);
		/* note that the first byte of the integer is the least significant byte */
		n = diff;
		val &= mask[n];

		if(activate_SCEP)//activate start code emulation prevention
		{
			start_code_emul_prevention();//start code emulation prevention
		}
		*(byteptr ++) = outbfr;
		bytecnt++;
		outbfr = 0;
		outcnt = 8;
	}

	if (n > 0) { /* input is short enough to fit in what is left in the buffer */
		outbfr |= (UInt8)(val << (-diff));
		outcnt -= n;
	}
}

void start_code_emul_prevention_h264()
{
	if((outbfr2 == 0) && 
	   (outbfr1 == 0) && 
	   ((outbfr == 0) || (outbfr == 1) || (outbfr == 2) || (outbfr == 3))
	  )
	{
		*(byteptr ++) = 0x03;//E.3 step 4. P.437
		outbfr2 = outbfr1 = 0xFF;//reset buffers
		bytecnt++;
	}
	outbfr2 = outbfr1;
	outbfr1 = outbfr;
}

void start_code_emul_prevention()
{
	if((outbfr2 == 0) && 
	   (outbfr1 == 0) && 
	   ((outbfr & 0xFC) == 0))
	{
		*(byteptr ++) = 0x03;//E.3 step 4. P.437
		outbfr2 = outbfr1 = 0xFF;//reset buffers
		bytecnt++;
	}
	outbfr2 = outbfr1;
	outbfr1 = outbfr;
}

UInt32 bitstream_Close()
{
	if(outcnt != 8)
		byte_stuffing = TRUE;
	else
		byte_stuffing = FALSE;

	while (outcnt != 8) bitstream_PutBits(1, 0);//byte alignement
	return bytecnt;
}

UInt32 bitstream_Close_32bits(UInt32 seq_hdr_len, UInt32 epl_hdr_len)
{
	UInt32 total_bytes;	
	int i;

	total_bytes = seq_hdr_len + epl_hdr_len + bytecnt;	
	if((total_bytes % 4 == 0) && (byte_stuffing == FALSE))
	{
		bitstream_PutBits(8, 0); 
		total_bytes += 1;
	}
	while(total_bytes % 4 != 0)
	{
		bitstream_PutBits(8, 0); //32 bit alignement like in HW
		total_bytes += 1;
	}
	//to be finished 
	for(i = 0; i<4; i++)
	{
		bitstream_PutBits(8, 0); //always add 4 0 at the end of picture
		total_bytes += 1;
	}
	//to be implemented:
	//in case of SCE add 4 more bytes 
	return total_bytes;
}

UInt32 bitstream_Close_16bits(void)
{
	if(bytecnt % 2 != 0)
		bitstream_PutBits(8, 0); 

	return bytecnt;
}

UInt32 bitstream_Stuffing(UInt32 seq_len)
{	
	UInt32 total_bytes;
	
	total_bytes = seq_len;
	while(total_bytes % 2048 != 0)
	{
		bitstream_PutBits(8, 0); //2048 bit alignement like in HW
		total_bytes += 1;
	}
	return 0;
}

UInt32 bitstream_GetLength()
{
	return bytecnt;
}

// exp-golomb
void eg_write_ue(int code_num, int *len, int *data)
{
    int tmp = code_num + 1;
    int m, info;
  
    if (code_num != 0)
    {
        m = 0;
       
        while (tmp)
        {
    	    tmp >>= 1;
    	    m ++;
        }

        m --;
        
        info = code_num + 1 - (1 << m);
        //BitstreamPutBits(bs, 1, m + 1);
        //BitstreamPutBits(bs, info, m);
		*len = 2*m + 1;
		*data = (1<<m) + info;
    }
    else
    {
		*len = 1;
		*data = 1;
        //BitstreamPutBits(bs, 1, 1);
    }
}

void eg_write_se(int code_num, int *len, int *data)
{
    int code_num_se;

    code_num_se = (code_num > 0) ? (code_num << 1) - 1 : (-code_num) << 1;

    eg_write_ue(code_num_se, len, data);    
}
