This small test demonstrates how to encode a test clip into H.264 Baseline profile.
Just run h264_encoder.exe
This will launch the encoder using configuration in options.cfg. result will be written to test_1280x720.264. This compressed video can be played with VLC.
